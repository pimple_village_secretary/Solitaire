import Poker from "../../poker/Poker";

const {ccclass, property} = cc._decorator;

@ccclass
export default class SelectPoker extends cc.Component {

    @property(cc.Node)
    private content: cc.Node = null;

    @property(cc.Prefab)
    private pokerPrefab: cc.Prefab = null;

    private curPokers: Array<Poker> = [];

    private selectedPoker: Map<number, Poker> = new Map<number, Poker>();

    offsetRight: number = 0;

    init(cards: Array<number>): void {
        this.curPokers = [];
        this.selectedPoker.clear();
        this.initListener();
        this.initCards(cards);
    }

    private initCards(cards: Array<number>) {
        this.content.removeAllChildren();
        cards.forEach(cardId => {
            if (cardId == null) return;
            let node: cc.Node = cc.instantiate(this.pokerPrefab);
            let poker = node.getComponent(Poker);
            poker.init(cardId);
            this.content.addChild(node);
            this.curPokers.push(poker);
            // console.log(`position: ${node.getBoundingBox()}, height: ${node.height}, width: ${node.width}`);
        });
    }

    private initListener() {
        this.node.off(cc.Node.EventType.TOUCH_START);
        this.node.off(cc.Node.EventType.TOUCH_MOVE);
        this.node.off(cc.Node.EventType.TOUCH_END);
        this.node.off(cc.Node.EventType.TOUCH_CANCEL);
        this.node.on(cc.Node.EventType.TOUCH_START, this.eventTouchStart, this);
        this.node.on(cc.Node.EventType.TOUCH_MOVE, this.eventTouchMove, this);
        this.node.on(cc.Node.EventType.TOUCH_END, this.eventTouchEnd, this);
        this.node.on(cc.Node.EventType.TOUCH_CANCEL, this.eventTouchCancel, this);
        console.log("SelectPoker initListener");
    }

    private eventTouchStart(event: cc.Event.EventTouch) {
        this.loopSearch(event.getLocation(), this.curPokers);
    }

    private eventTouchMove(event: cc.Event.EventTouch) {
        this.loopSearch(event.getLocation(), this.curPokers);
    }

    private eventTouchEnd(event: cc.Event.EventTouch) {
        this.selectedPoker.forEach((value, key) => {
            value.hideTouchMask();
            value.clickSelected();
        });
        console.log(`length: ${this.selectedPoker.size}`);
        this.selectedPoker.clear();
        this.node.emit("touchPokerEnd", this.currentlySelectedCard());
    }

    private eventTouchCancel(event: cc.Event.EventTouch) {
        this.selectedPoker.forEach((value, key) => {
            value.hideTouchMask();
        });
        this.selectedPoker.clear();
    }

    loopSearch(touch: cc.Vec2, pokers: Array<Poker>) {
        console.log(`old touch: ${touch}`);
        for (let index = 0; index < pokers.length; index++) {
            const poker = pokers[index];
            if (!this.isInMatrix(touch, poker.node)) continue;
            if (index + 1 < pokers.length) {
                const nextPoker = pokers[index + 1];
                if (this.isInMatrix(touch, nextPoker.node)) continue;
            }
            if (this.selectedPoker.has(poker.cardId)) continue;
            this.selectedPoker.set(poker.cardId, poker);
            poker.showTouchMask();
            console.log(`push: ${poker.cardId}`);
        }
    }

    isInMatrix(touch: cc.Vec2, targetNode: cc.Node): boolean {
        const curPosition = this.content.convertToNodeSpaceAR(touch);
        // console.log(`touch: ${curPosition}`);
        const rect = targetNode.getBoundingBox();
        // console.log(`position: ${rect}, height: ${rect.height}, width: ${rect.width}`);
        return rect.contains(curPosition);
    }

    currentlySelectedCard(): Array<number> {
        let result: Array<number> = [];
        for (let index = 0; index < this.curPokers.length; index++) {
            if (!this.curPokers[index].isSelected) continue;
            result.push(this.curPokers[index].cardId);
        }
        return result;
    }
}
