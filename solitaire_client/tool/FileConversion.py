import argparse
from itertools import cycle
from tkinter import NONE
import pandas as pd
import json
import sys
import os

def parse_argument(argv):
	""" 参数传递函数 """
	parser = argparse.ArgumentParser()
	parser.add_argument('--in_path',type=str, default="",help='excel文件路径')
	parser.add_argument('--out_path_json',default="",help='json输出文件路径')
	parser.add_argument('--out_path_typescript',default="",help='typescript输出文件路径')
	return parser.parse_args(argv)

def getTypeNameAndJsonName(file):
    result = []
    data = pd.read_excel(file)
    result.append(data.columns[0])
    result.append(data.columns[1])
    return result

# 创建TypeScript文件
def createTypeScriptFile(file_name, data, outPath):
    attributes = dict()
    for attr in data.columns: 
        attributes_name = data.loc[0][attr]
        attributes_type = data.loc[3][attr]
        attributes[attributes_name] = attributes_type
    class_str = createTypeScriptClass(file_name, attributes)
    file_out = open(outPath + file_name + '.ts', "w", encoding='utf-8')
    file_out.write(class_str)
    file_out.close()

# 创建TypeScript类的文本
def createTypeScriptClass(file_name, attributes):
    result = "export default class " + file_name + " {\n"
    for key, value in attributes.items():
        result += "\t" + key + ": " + value + ";\n"
    return result + "}"

# 创建Json文件
def createJsonFile(file_name, data, outPath):
    json_object = dict()
    for column in data.columns: 
        attributes_name = data.loc[0][column]
        attributes_value = getJsonValue(data, column)
        json_object[attributes_name] = attributes_value
    file_out = open(outPath + file_name + '.json', "w", encoding='utf-8')
    json_data = json.dump(json_object, file_out, ensure_ascii=False)
    file_out.close()

# 获取某列的所有值
def getJsonValue(data, column):
    values = []
    for index in range(4, data.shape[0]): values.append(data.loc[index][column])
    return values

def cycleConversion(in_path, out_path_json, out_path_typescript):
    files = os.listdir(in_path)
    for file in files: 
        data = pd.read_excel(in_path + "\\" + file, header=1)
        fileName = getTypeNameAndJsonName(in_path + "\\" + file)
        createTypeScriptFile(fileName[1] + "JsonInfo", data, out_path_typescript + "\\")
        createJsonFile(fileName[0], data, out_path_json + "\\")
        
if __name__ == '__main__':
    args = sys.argv[1:]
    print(args)
    in_path = args[0]
    out_path_json = args[1]
    out_path_typescript = args[2]
    cycleConversion(in_path, out_path_json, out_path_typescript)

