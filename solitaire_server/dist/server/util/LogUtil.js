"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
var fs = __importStar(require("fs"));
var LogUtil = /** @class */ (function () {
    function LogUtil() {
    }
    LogUtil.createServerLog = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            var serverFile = _this.filePath + _this.serverFileName;
            fs.open(serverFile, "w", function (err, fd) {
                if (err)
                    throw new Error(err.message);
                _this.fdMap.set(_this.serverFileName, fd);
                resolve();
            });
        });
    };
    LogUtil.writeServerLog = function (title, logStr) {
        var fd = this.fdMap.get(this.serverFileName);
        if (fd == undefined)
            throw new Error("server's fd is not exist!");
        var str = "[".concat(title, "][").concat(Date(), "]: ").concat(logStr, "\n");
        fs.writeFileSync(fd, str);
    };
    LogUtil.closeServerLog = function () {
        var fd = this.fdMap.get(this.serverFileName);
        if (fd == undefined)
            throw new Error("server's fd is not exist!");
        fs.closeSync(fd);
        this.fdMap.delete(this.serverFileName);
    };
    // static writeRoomLog(roomId: number, logStr: string) {
    //     const fd = this.fdMap.get(roomId.toString());
    //     if (fd == undefined) throw new Error(`can't get room log file of fd`);
    //     fs.writeFileSync(fd, logStr + "\n");
    // }
    // static createRoomLog(roomId: number): Promise<void> { 
    //     return new Promise<void>((resolve, reject) => {
    //         const timeNowStr = Date.now().toString();
    //         const fileName = this.filePath + `${roomId}-${timeNowStr}`;
    //         fs.open(fileName, "w", (err, fd) => {
    //             if (err) throw new Error(err.message);
    //             this.fdMap.set(roomId.toString(), fd);
    //             resolve();
    //         });
    //     });
    // }
    // static closeRoomLog(roomId: number): void { 
    //     const fd = this.fdMap.get(roomId.toString());
    //     if (!fd) throw new Error(`room ${roomId}'s fd is not exist!`);
    //     fs.closeSync(fd);
    //     this.fdMap.delete(roomId.toString());
    // }
    LogUtil.closeAllLogs = function () {
        this.fdMap.forEach(function (value, key) {
            if (value == undefined)
                throw new Error("server's fd is not exist!");
            fs.closeSync(value);
        });
        this.fdMap.clear();
    };
    LogUtil.filePath = "./log/";
    LogUtil.serverFileName = "server_log";
    LogUtil.fdMap = new Map();
    return LogUtil;
}());
exports.default = LogUtil;
