"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var socket_io_1 = require("socket.io");
var GameManagerVo_1 = __importDefault(require("./server/data/GameManagerVo"));
var GameStaticDataManager_1 = __importDefault(require("./server/data/GameStaticDataManager"));
var ClientToServerEventHandler_1 = __importDefault(require("./server/handler/ClientToServerEventHandler"));
var NetManager_1 = __importDefault(require("./server/net/NetManager"));
var LogUtil_1 = __importDefault(require("./server/util/LogUtil"));
var runServer = function () { return __awaiter(void 0, void 0, void 0, function () {
    var server, server_port;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                server = new socket_io_1.Server();
                NetManager_1.default.io = require("socket.io")(server, { cors: true });
                server_port = 3000;
                console.log("-----------------------------------------");
                console.log("initializing the server...");
                return [4 /*yield*/, GameStaticDataManager_1.default.initAll()];
            case 1:
                _a.sent();
                return [4 /*yield*/, LogUtil_1.default.createServerLog()];
            case 2:
                _a.sent();
                GameManagerVo_1.default.init();
                NetManager_1.default.io.on("connection", function (socket) {
                    var count = NetManager_1.default.io.engine.clientsCount;
                    console.log("\nhave a client to connect. socket id: ".concat(socket.id, ", cur client num: ").concat(count));
                    NetManager_1.default.addEventListener(socket, "disconnect", ClientToServerEventHandler_1.default.disconnect, false);
                    NetManager_1.default.addEventListener(socket, "login", ClientToServerEventHandler_1.default.login, true);
                    NetManager_1.default.addEventListener(socket, "findFreeRoom", ClientToServerEventHandler_1.default.findFreeRoomHandler, true);
                    NetManager_1.default.addEventListener(socket, "quitGameRoom", ClientToServerEventHandler_1.default.quitGameRoomHandler, true);
                    NetManager_1.default.addEventListener(socket, "switchGameRoom", ClientToServerEventHandler_1.default.switchGameRoomHandler, true);
                    NetManager_1.default.addEventListener(socket, "prepareGame", ClientToServerEventHandler_1.default.prepareGameHandler, true);
                    NetManager_1.default.addEventListener(socket, "cancelPrepareGame", ClientToServerEventHandler_1.default.cancelPrepareGameHandler, true);
                    NetManager_1.default.addEventListener(socket, "callLandlord", ClientToServerEventHandler_1.default.callLandlord, true);
                    NetManager_1.default.addEventListener(socket, "playCards", ClientToServerEventHandler_1.default.playCards, true);
                    // NetManager.addEventListener(socket, "startPlay", ClientToServerEventHandler.startPlay, true);
                });
                // 绑定端口
                NetManager_1.default.io.listen(server_port);
                console.log("server started, waiting for connection...");
                console.log("-----------------------------------------");
                return [2 /*return*/];
        }
    });
}); };
runServer();
