import ObjUtil from "../util/ObjUtil";
import GameManagerVo from "../data/GameManagerVo";
import SendData from "./SendData";

export default class NetManager {

    static io: any = null;

    static defaultListenEvent = "message";

    /**
     * 向当前socket发送数据
     * 
     * 备注: 向客户端被动的返回数据, 其客户端监听message事件，通过cmdIndex参数来区分时不同的发送
     * @param userId 用户id
     * @returns 
     */
    static sendDataToClient(socket: any, cmdIndex: number, message: any): void {
        const sendData = new SendData(message, cmdIndex, NetManager.defaultListenEvent);
        const massage = JSON.stringify(sendData);
        socket.emit(NetManager.defaultListenEvent, massage);
    }

    /**
     * 向除了当前socket之外的房间所有人发送数据
     * 
     * 备注：向客户端主动的发送数据，其客户端监听message事件，通过method参数来区分时不同的发送
     * @param userId 用户id
     * @returns 
     */
    static sendDataToRoomOthersClient(socket: any, roomId: number, message: any, method: string): void {
        if (!GameManagerVo.roomManager.findRoomOfId(roomId)) {
            console.error("room is not find!!!");
            return;
        }
        const sendData = new SendData(message, 0, method);
        const massage = JSON.stringify(sendData);
        socket.to(roomId).emit(NetManager.defaultListenEvent, massage);
    }

    /**
     * 向房间所有人发送数据
     * 
     * 备注：向客户端主动的发送数据，其客户端监听message事件，通过method参数来区分时不同的发送
     * @param userId 用户id
     * @returns 
     */
    static sendDataToRoomAllClient(roomId: number, message: any, method: string): void {
        if (!GameManagerVo.roomManager.findRoomOfId(roomId)) {
            console.error("room is not find!!!");
            return;
        }
        const sendData = new SendData(message, 0, method);
        const massage = JSON.stringify(sendData);
        this.io.in(roomId).emit(NetManager.defaultListenEvent, massage);
    }

    /**
     * 监听事件, 并制定对应的处理函数
     * @param socket socket套接字
     * @param listenEvent 监听事件
     * @param handler 触发时的处理事件函数
     * @param isHaveReturnValue 是否有返回值
     */
    static addEventListener(socket: any, listenEvent: string, handler: Function, isHaveReturnValue: boolean): void {
        socket.on(listenEvent, (resp: any) => {
            if (isHaveReturnValue) {
                const sendData = JSON.parse(resp);
                const message = sendData.message;
                const cmdIndex = sendData.cmdIndex;
                ObjUtil.execFunc(handler, socket, cmdIndex, message);
                return;
            }
            ObjUtil.execFunc(handler, socket);
        });
    }
}