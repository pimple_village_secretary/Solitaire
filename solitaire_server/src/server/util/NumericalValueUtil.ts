export default class NumericalValueUtil {

    /**
     * 得到一个两数之间的随机整数
     * 
     * 返回了一个在指定值之间的随机整数。这个值不小于 min （如果 min 不是整数，则不小于 min 的向上取整数），且小于（等于）max。
     * @param min 
     * @param max 
     * @returns 
     */
    static getRandomInt(min: number, max: number): number {
        min = Math.ceil(min);
        max = Math.floor(max);
        const result = Math.floor(Math.random() * (max - min + 1)) + min;
        return result;
    }

    /**
     * 从数组中寻找一定间隔值的元素(循环取余)
     * @param nums 数组
     * @param initIndex 初始下标
     * @param intervalValue 间隔值
     * @returns 
     */
    static findValueOfArray(nums: Array<number>, initIndex: number, intervalValue: number): number {
        const index = (initIndex + intervalValue) % nums.length;
        return nums[index];     
    }

    /**
     * 获取元素对应再数组中的下标
     * @param nums 数组集合
     * @param elements 元素
     * @returns 元素对应再数组中的下标
     */
    static findArrayElementIndex(nums: Array<number | null>, elements:Array<number>): Array<number> | null {
        if (elements.length > nums.length) return null;
        let slots: Array<number> = [];
        for (let index1 = 0, index2 = 0; index1 < nums.length; index1++) {
            if (elements[index2] != nums[index1]) continue;
            slots.push(index1);
            index2 += 1;
        }
        return slots;
    }
    

}