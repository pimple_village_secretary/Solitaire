import NumericalValueUtil from "../../util/NumericalValueUtil";
import UserVo from "./UserVo";

export default abstract class RefereeVo {

    bookmakerCardsNum: number = 0;

    handCardsNum: number = 0;

    bookmakerUserId: number | null = null;                        // 庄家Id

    bookmakerCards: Array<number> = [];                           // 庄家牌

    handCards: { [key: number]: Array<number | null> } = {};      // 用户手牌

    magnification: { [key: number]: number } = {};                // 倍率(分别存储每个人的倍率)

    initBet: number = 1;                    // 起始赌注

    numberOfRounds: number = 0;             // 回合数

    turnId: number | null = null;           // 谁的回合

    private _countdownTime: number;         // 倒计时(秒)

    private _countdownStartTime: number = 0;    // 倒计时起始时间(时间戳)

    get countdownTime(): number { return this._countdownTime };

    get countdownStartTime(): number { return this._countdownStartTime };

    initJsonInfo(resp: any) {
        this.bookmakerCardsNum = resp.bookmakerCardsNum;
        this.handCardsNum = resp.handCardsNum;
        this.bookmakerUserId = resp.bookmakerUserId;
        this.bookmakerCards = resp.bookmakerCards;
        this.initBet = resp.initBet;
        this.numberOfRounds = resp.numberOfRounds;
        this.turnId = resp.turnId;
        this._countdownTime = resp.countdownTime;
        this._countdownStartTime = resp.countdownStartTime;
        this.handCards = resp.handCards;
        this.magnification = resp.magnification;
    }

    /**
     * 剩余时间
     */
    get remainingTime(): number {
        return this.countdownStartTime + (this.countdownTime * 1000) - Date.now()
    }

    /**
     * 根据用户Id找到对应的手牌
     * @param roleId 用户Id
     * @returns 
     */
    findHandCards(roleId: number) {
        return this.handCards[roleId];
    }

    /**
     * 返回当前用户还有多少张手牌
     * @param roleId 用户Id
     */
    curHandCardNumber(roleId: number) {
        let result = 0;
        this.handCards[roleId].forEach(element => {
            if (element == null) return;
            result += 1;
        });
        return result;
    }

    /**
     * 是否符合出牌规则
     */
    abstract isComplyRules(roleId: number, cards: Array<number>): boolean;

    /**
     * 比较两幅牌组大小
     * @param cards1 牌组1
     * @param cards2 牌组2
     * @returns cards1大于cards2则返回true， 反之返回false
     */
    abstract compareCards(cards1: Array<number>, cards2: Array<number>): boolean;

}