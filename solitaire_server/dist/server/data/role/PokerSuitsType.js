"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.POKER_SUITS = void 0;
var POKER_SUITS;
(function (POKER_SUITS) {
    POKER_SUITS[POKER_SUITS["diamond"] = 0] = "diamond";
    POKER_SUITS[POKER_SUITS["club"] = 1] = "club";
    POKER_SUITS[POKER_SUITS["heart"] = 2] = "heart";
    POKER_SUITS[POKER_SUITS["spade"] = 3] = "spade";
    POKER_SUITS[POKER_SUITS["none"] = -1] = "none";
})(POKER_SUITS = exports.POKER_SUITS || (exports.POKER_SUITS = {}));
