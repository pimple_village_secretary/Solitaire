import GameRoleDataManager from "../../data/GameRoleDataManager";
import FightingLandlordCardsVo from "../../data/roledata/FightingLandlordCardsVo";
import RoomVo from "../../data/roledata/RoomVo";
import NetManager from "../../net/NetManager";
import SendData from "../../net/SendData";
import FightingLandlordRoomLayer from "../room/fightingLandlordRoom/FightingLandlordRoomLayer";
import GameContext from "./GameContext";

const {ccclass, property} = cc._decorator;

@ccclass
export default class MainUI extends cc.Component {

    @property(cc.Prefab)
    private roomLayerPrefab: cc.Prefab = null;

    async clickFastJoinGame() {
        const sendData = new SendData(null, "findFreeRoom");
        const resp = await NetManager.socket.sendDataSync(sendData);
        
        let roomVo: RoomVo = new RoomVo();
        roomVo.initJsonInfo(resp.roomVo);
        const node = cc.instantiate(this.roomLayerPrefab);
        const roomLayer = node.getComponent(FightingLandlordRoomLayer);
        roomLayer.init(roomVo, GameRoleDataManager.userVo);
        this.node.addChild(node);
        GameContext.fightingLandlordRoomLayer = roomLayer;
        node.on("close", () => {
            GameContext.fightingLandlordRoomLayer = null;
            node.destroy();
        });
    }

}
