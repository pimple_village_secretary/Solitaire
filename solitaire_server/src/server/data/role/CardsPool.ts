import NumericalValueUtil from "../../util/NumericalValueUtil";

export default class CardsPool {

    protected static readonly cardsNumber: number = 4 * 13 + 2;   // 4种花色 * 13张点数 + 大王 + 小王 54张

    private cardsPool: Set<number> = new Set<number>(); // 卡牌池
    
    init(): void {
        this.cardsPool.clear();
        for (let i = 0; i < CardsPool.cardsNumber;) {
            const cardId = NumericalValueUtil.getRandomInt(0, CardsPool.cardsNumber - 1);
            if (this.cardsPool.has(cardId)) continue;
            this.cardsPool.add(cardId);
            i += 1;
        }
    }

    /**
     * 从卡牌池里面获取一定数量的卡牌
     * @param cardsNum 要取的卡牌数量
     * @returns 
     */
    getCardsOfCardsPool(cardsNum: number): Array<number> {
        let cards: Array<number> = [];
        let count = 0;
        for (let key of this.cardsPool.keys()) {
            const cardId = key;
            this.cardsPool.delete(key);
            cards.push(cardId);
            count += 1;
            if (count >= cardsNum) return cards;
        }
        return cards;
    }

    /**
     * 将卡牌返回给卡牌池
     * @param cards 返回给卡牌池的卡牌组
     */
    returnCardsToPool(cards: Array<number>): void {
        cards.forEach(element => {
            this.cardsPool.add(element);
        });
    }

}