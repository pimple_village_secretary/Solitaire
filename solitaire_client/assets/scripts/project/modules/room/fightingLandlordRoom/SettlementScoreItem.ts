const {ccclass, property} = cc._decorator;

@ccclass
export default class SettlementScoreItem extends cc.Component {

    @property(cc.Label)
    userName: cc.Label = null;

    @property(cc.Label)
    money: cc.Label = null;

    @property(cc.Label)
    winOrLose: cc.Label = null;

    init(roleId: number, money: number, isWin: boolean): void {
        this.userName.string = `${roleId}`;
        this.money.string = `${money}`;
        if (isWin) {
            this.winOrLose.string = `胜利`;
            this.winOrLose.node.color = cc.color().fromHEX("#EFA91A");
        } else {
            this.winOrLose.string = `失败`;
            this.winOrLose.node.color = cc.color().fromHEX("#CF3333");
        }
    }



}
