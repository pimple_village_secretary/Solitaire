import RoomVo from "./roledata/RoomVo";
import UserVo from "./roledata/UserVo";

export default class GameRoleDataManager {

    static userVo: UserVo = new UserVo();

    // static roomVo: RoomVo = new RoomVo();

    static init() {
        GameRoleDataManager.userVo.roleId = Math.ceil(1000 + (1000 * Math.random()));
        GameRoleDataManager.userVo.roleName = `用户${GameRoleDataManager.userVo.roleId}`;
        GameRoleDataManager.userVo.money = GameRoleDataManager.userVo.roleId * 22;
        GameRoleDataManager.userVo.roleAvatar = "null";
    }

}