export default class GameScoringVo {

    scoring: { [key: number]: number } = {};

    initJsonInfo(resp: any) {
        this.scoring = resp.scoring;
    }

}