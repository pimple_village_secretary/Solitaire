import BaseSocket from "./BaseSocket";
import { SocketSessionState, SocketEvent } from "./NetEnum";
import ServerConfig from "./ServerConfig";

export default class NetManager {

    static socket: BaseSocket;

    private static sessionState: SocketSessionState = SocketSessionState.closed;

    private static serverConfig: ServerConfig = new ServerConfig();
    
    static createSocket(serverConfig: ServerConfig): void {
        this.serverConfig = serverConfig;
        NetManager.socket = new BaseSocket();
    }

    static connect(): void {
        if (!NetManager.socket) return;
        NetManager.sessionState = SocketSessionState.connecting;
        // 接收一个事件, 判断连接是否成功

        NetManager.socket.connect(this.serverConfig);
    }

}