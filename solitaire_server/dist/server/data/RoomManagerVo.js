"use strict";
var __values = (this && this.__values) || function(o) {
    var s = typeof Symbol === "function" && Symbol.iterator, m = s && o[s], i = 0;
    if (m) return m.call(o);
    if (o && typeof o.length === "number") return {
        next: function () {
            if (o && i >= o.length) o = void 0;
            return { value: o && o[i++], done: !o };
        }
    };
    throw new TypeError(s ? "Object is not iterable." : "Symbol.iterator is not defined.");
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var RoomStatusType_1 = require("./role/RoomStatusType");
var RoomVo_1 = __importDefault(require("./role/RoomVo"));
var RoomManagerVo = /** @class */ (function () {
    function RoomManagerVo() {
        this.roomVosNoFull = new Map();
        this.roomVosNoFullCount = 0;
        this.roomVosFull = new Map();
        this.roomIdCounter = 0;
    }
    Object.defineProperty(RoomManagerVo.prototype, "isHaveFreeRoom", {
        get: function () { return this.roomVosNoFullCount > 0; },
        enumerable: false,
        configurable: true
    });
    ;
    RoomManagerVo.prototype.init = function (maximumNumberRooms, roomType) {
        this.roomCanHaveMaximumPeople = maximumNumberRooms;
        this.roomType = roomType;
    };
    /**
     * 创建一个房间
     * @returns 返回一个RoomVo类型的数据
     */
    RoomManagerVo.prototype.createRoom = function () {
        var vo = new RoomVo_1.default();
        vo.init(this.roomIdCounter, this.roomCanHaveMaximumPeople, this.roomType);
        this.roomVosNoFull.set(vo.roomId, vo);
        this.roomIdCounter += 1;
        this.roomVosNoFullCount += 1;
        return vo;
    };
    /**
     * 删除房间
     * @param roomId
     */
    RoomManagerVo.prototype.deleteRoom = function (roomId) {
        if (this.findRoomOfId(roomId) != null) {
            this.roomVosNoFull.delete(roomId);
            this.roomVosNoFullCount -= 1;
            return;
        }
        this.roomVosFull.delete(roomId);
    };
    /**
     * 找到对应房间
     * @param roomId 房间号
     * @returns
     */
    RoomManagerVo.prototype.findRoomOfId = function (roomId) {
        var vo = this.roomVosNoFull.get(roomId);
        if (vo)
            return vo;
        vo = this.roomVosFull.get(roomId);
        if (vo)
            return vo;
        return null;
    };
    /**
     * 用户加入房间
     * @param roomId 房间号
     * @param userVo 用户Vo
     * @returns
     */
    RoomManagerVo.prototype.addUserToRoom = function (roomId, userVo) {
        var vo = this.roomVosNoFull.get(roomId);
        if (!vo || vo.status == RoomStatusType_1.ROOM_STATUS.inGame)
            return null;
        vo.addToUser(userVo);
        if (!vo.isRoomFull())
            return vo;
        this.roomVosFull.set(roomId, vo);
        this.roomVosNoFull.delete(roomId);
        this.roomVosNoFullCount -= 1;
        return vo;
    };
    /**
     * 用户退出房间 (如果用户准备了, 则自动移除准备队列)
     * @param roomId
     * @param userId
     */
    RoomManagerVo.prototype.removeUserToRoom = function (roomId, userId) {
        var vo = this.roomVosNoFull.get(roomId);
        if (vo) {
            vo.removeUser(userId);
            return vo;
        }
        vo = this.roomVosFull.get(roomId);
        if (!vo)
            return null;
        this.roomVosNoFull.set(roomId, vo);
        this.roomVosFull.delete(roomId);
        vo.removeUser(userId);
        vo.removeAllToPrepareQueue(); // 应该移除准备队列中的所有用户
        this.roomVosNoFullCount += 1;
        return vo;
    };
    /**
     * 返回一个空闲的房间号
     * @param curRoomId 当前房间号
     * @returns
     */
    RoomManagerVo.prototype.findFreeRoom = function (curRoomId) {
        var e_1, _a;
        if (curRoomId === void 0) { curRoomId = 0; }
        if (!this.isHaveFreeRoom)
            return null;
        if (curRoomId == null) {
            try {
                for (var _b = __values(this.roomVosNoFull.values()), _c = _b.next(); !_c.done; _c = _b.next()) {
                    var value = _c.value;
                    if (!value.isRoomFull())
                        return value;
                }
            }
            catch (e_1_1) { e_1 = { error: e_1_1 }; }
            finally {
                try {
                    if (_c && !_c.done && (_a = _b.return)) _a.call(_b);
                }
                finally { if (e_1) throw e_1.error; }
            }
            return null;
        }
        for (var i = curRoomId; i < this.roomIdCounter - 1; i++) {
            var result = this.roomVosNoFull.get(i + 1);
            if (result && !result.isRoomFull())
                return result;
        }
        for (var i = 0; i <= curRoomId; i++) {
            var result = this.roomVosNoFull.get(i);
            if (result && !result.isRoomFull())
                return result;
        }
        return null;
    };
    /**
     * 设置房间为初始状态（不移除房间的人）
     * @param roomId 房间号
     * @returns
     */
    RoomManagerVo.prototype.setInitializationState = function (roomId) {
        var vo = this.findRoomOfId(roomId);
        if (!vo)
            return;
        vo.changeRoomStatus(RoomStatusType_1.ROOM_STATUS.waiting);
        vo.removeReferee();
        vo.removeAllToPrepareQueue();
    };
    return RoomManagerVo;
}());
exports.default = RoomManagerVo;
