"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var UserManagerVo = /** @class */ (function () {
    function UserManagerVo() {
        /**
         * 套接字id索引用户Id
         */
        this.socketIndexUser = new Map();
        /**
         * 用户id索引套接字和用户vo
         */
        this.userIndexSocketUsers = new Map();
    }
    UserManagerVo.prototype.init = function () {
    };
    /**
     * 添加套接字和用户
     * @param socket
     * @param user
     * @returns void
     */
    UserManagerVo.prototype.addToUser = function (socket, user) {
        if (this.socketIndexUser.has(socket.id) || this.userIndexSocketUsers.has(user.roleId))
            return;
        this.socketIndexUser.set(socket.id, user.roleId);
        this.userIndexSocketUsers.set(user.roleId, { socket: socket, user: user });
    };
    /**
     * 根据socket Id删除用户和对应的套接字
     * @param socketId 要被删除的socket id
     * @returns void
     */
    UserManagerVo.prototype.removeUser = function (socketId) {
        if (!this.socketIndexUser.has(socketId))
            return;
        var roleId = this.socketIndexUser.get(socketId);
        if (!roleId)
            return;
        // 关闭删除套接字和用户
        this.socketIndexUser.delete(socketId);
        this.userIndexSocketUsers.delete(roleId);
    };
    /**
     * 使用socket id找到对应的用户
     * @param id socket id
     * @returns
     */
    UserManagerVo.prototype.findUserUsingSocketId = function (id) {
        var userId = this.socketIndexUser.get(id);
        if (!userId)
            return null;
        var result = this.userIndexSocketUsers.get(userId);
        if (!result)
            return null;
        return result.user;
    };
    /**
     * 使用user id找到对应用户的socket
     * @param id 用户id
     * @returns
     */
    UserManagerVo.prototype.findSocketUsingUserId = function (id) {
        var result = this.userIndexSocketUsers.get(id);
        if (!result)
            return null;
        return result.socket;
    };
    /**
     * 扣钱
     * @param roleId 用户ID
     * @param num 对应扣除的数量
     * @returns 返回现有的数量
     */
    UserManagerVo.prototype.minusMoney = function (roleId, num) {
        var _a;
        var userVo = (_a = this.userIndexSocketUsers.get(roleId)) === null || _a === void 0 ? void 0 : _a.user;
        if (!userVo)
            return null;
        userVo.money -= num;
        return userVo.money;
    };
    /**
     * 加钱
     * @param roleId 用户ID
     * @param num 对应增加的数量
     * @returns 返回现有的数量
     */
    UserManagerVo.prototype.addMoney = function (roleId, num) {
        var _a;
        var userVo = (_a = this.userIndexSocketUsers.get(roleId)) === null || _a === void 0 ? void 0 : _a.user;
        if (!userVo)
            return null;
        userVo.money += num;
        return userVo.money;
    };
    /**
     * 判断是否有足够的钱
     * @param roleId 用户ID
     * @param num 游戏货币数量
     */
    UserManagerVo.prototype.isEnoughMoney = function (roleId, num) {
        var _a;
        var userVo = (_a = this.userIndexSocketUsers.get(roleId)) === null || _a === void 0 ? void 0 : _a.user;
        if (!userVo)
            return null;
        if (userVo.money >= num)
            return true;
        return false;
    };
    /**
     * 获取用户的钱
     * @param roleId 用户ID
     * @returns
     */
    UserManagerVo.prototype.getUserMony = function (roleId) {
        var _a;
        var userVo = (_a = this.userIndexSocketUsers.get(roleId)) === null || _a === void 0 ? void 0 : _a.user;
        if (!userVo)
            return null;
        return userVo.money;
    };
    UserManagerVo.prototype.gameScoring = function (scoringVo) {
        var _this = this;
        Object.keys(scoringVo.scoring).forEach(function (key) {
            _this.addMoney(Number(key), scoringVo.scoring[Number(key)]);
        });
    };
    return UserManagerVo;
}());
exports.default = UserManagerVo;
