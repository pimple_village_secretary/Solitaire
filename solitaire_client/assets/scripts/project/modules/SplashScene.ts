import GameStaticDataManager from "../data/GameStaticDataManager";
import NetManager from "../net/NetManager";
import ServerConfig from "../net/ServerConfig";
import LoadResInfo from "../util/LoadResInfo";

const {ccclass, property} = cc._decorator;

@ccclass
export default class SplashScene extends cc.Component {

    @property(cc.Label)
    label: cc.Label = null;

    private loadResInfo = new LoadResInfo();

    start () {
        this.label.string = "这是加载场景";

        let serverConfig = new ServerConfig();
        serverConfig.initLoactionTest();
        NetManager.createSocket(serverConfig);
        NetManager.connect();

        this.loadResInfo.init(() => {
            this.scheduleOnce(() => {
                cc.director.loadScene("GameScene", () => {
                    console.log("加载完成！");
                });
            }, 1);
        });
        GameStaticDataManager.init(this.loadResInfo);
    }

    // 连接服务器
    connectionServer(): void {
        
    }

}
