import UserVo from "../../../data/roledata/UserVo";

const {ccclass, property} = cc._decorator;

@ccclass
export default class Role extends cc.Component {

    @property(cc.Sprite)
    roleBody: cc.Sprite = null;

    @property(cc.Label)
    roleName: cc.Label = null;

    @property(cc.Label)
    moneyNum: cc.Label = null;

    init(userVo: UserVo, bookmakerUserId: number | null) {
        this.roleName.string = `${userVo.roleName}`;
        this.moneyNum.string = `$${userVo.money}`;
        let imageName: string;
        if (bookmakerUserId === userVo.roleId) imageName = "landlord";
        else imageName = "farmer";
        cc.loader.loadRes(`texture/room/${imageName}`, cc.SpriteFrame, (err, spriteFrame: cc.SpriteFrame) => {
            this.roleBody.spriteFrame = spriteFrame;
        });
    }

}
