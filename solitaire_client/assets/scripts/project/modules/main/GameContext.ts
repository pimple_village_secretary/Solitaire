import GameRoleDataManager from "../../data/GameRoleDataManager";
import { ROOM_STATUS } from "../../data/roledata/RoomStatusType";
import RoomVo from "../../data/roledata/RoomVo";
import NetManager from "../../net/NetManager";
import FightingLandlordRoomLayer from "../room/fightingLandlordRoom/FightingLandlordRoomLayer";

export default class GameContext {

    static fightingLandlordRoomLayer: FightingLandlordRoomLayer = null;

    static roomVo: RoomVo = null;

    static initListener(): void {

        NetManager.socket.offEventListener("roomStatus");
        NetManager.socket.addEventListener("roomStatus", (resp) => {
            if (!GameContext.fightingLandlordRoomLayer) return;
            const roomVo = new RoomVo();
            roomVo.initJsonInfo(resp.roomVo);
            this.fightingLandlordRoomLayer.roomVo = roomVo;
            if (this.fightingLandlordRoomLayer.isInCheckoutInterface) return;
            if (this.fightingLandlordRoomLayer.roomVo.status == ROOM_STATUS.GameOver) {
                this.fightingLandlordRoomLayer.isInCheckoutInterface = true;
                this.fightingLandlordRoomLayer.changeRoomToWaitStatus();
                this.fightingLandlordRoomLayer.refresh();
                this.fightingLandlordRoomLayer.mountSettlementScoreNode(resp);
                this.fightingLandlordRoomLayer.roomVo.setInitializationState();
            } else GameContext.fightingLandlordRoomLayer.refresh();
        }, false);

        // NetManager.socket.offEventListener("callLandlord");
        // NetManager.socket.addEventListener("callLandlord", (resp) => {
        //     if (!GameContext.fightingLandlordRoomLayer) return;

        // }, false);

    }

}