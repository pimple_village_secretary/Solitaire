export enum FACE_CARDS {
    diamond = 0,    // 方块
    club = 1,       // 梅花
    heart = 2,      // 红心
    spade = 3,      // 黑桃
    attendant = 4,      // 侍从
    queen = 5,          // 王后
    king = 6,           // 国王
    smallJoker = 7,     // 小王
    bigJoker = 8,       // 大王
}