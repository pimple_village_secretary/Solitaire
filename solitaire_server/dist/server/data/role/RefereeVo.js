"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var NumericalValueUtil_1 = __importDefault(require("../../util/NumericalValueUtil"));
var CardsPool_1 = __importDefault(require("./CardsPool"));
var RefereeVo = /** @class */ (function () {
    function RefereeVo() {
        this.bookmakerCardsNum = 0;
        this.handCardsNum = 0;
        this.bookmakerUserId = null; // 庄家Id
        this.bookmakerCards = []; // 庄家牌
        this.handCards = {}; // 用户手牌
        this.magnification = {}; // 倍率(分别存储每个人的倍率)
        this.initBet = 1; // 起始赌注
        this.cardsPool = null; // 卡牌池
        this.numberOfRounds = 0; // 回合数
        this.turnId = null; // 谁的回合
        this.countdownTime = 25; // 倒计时
        this.countdownStartTime = 0; // 起始时间(倒计时起始时间)
        this.slots = []; // 槽位
    }
    /**
     * 初始化一套牌组
     *
     * 这个函数初始化的同时会自动洗好一套牌组
     */
    RefereeVo.prototype.init = function (users, initBet, bookmakerCardsNum, handCardsNum) {
        var _this = this;
        if (initBet === void 0) { initBet = 1; }
        this.initBet = initBet;
        this.bookmakerCardsNum = bookmakerCardsNum !== null && bookmakerCardsNum !== void 0 ? bookmakerCardsNum : this.bookmakerCardsNum;
        this.handCardsNum = handCardsNum !== null && handCardsNum !== void 0 ? handCardsNum : this.bookmakerCardsNum;
        this.cardsPool = new CardsPool_1.default();
        this.cardsPool.init();
        users.forEach(function (element) {
            if (!element)
                return;
            _this.handCards[element.roleId] = new Array();
            _this.magnification[element.roleId] = 1; // 初始化倍率
            _this.slots.push(element.roleId); // 初始化槽位
        });
    };
    /**
     * 洗牌
     */
    RefereeVo.prototype.shuffle = function () {
        if (!this.cardsPool)
            return;
        this.cardsPool.init();
    };
    /**
     * 发牌
     */
    RefereeVo.prototype.deal = function (sortingAlgorithm) {
        var _this = this;
        if (!this.cardsPool)
            throw new Error("cards pool is not init!");
        if (this.slots.length == 0)
            throw new Error("users is null.");
        this.slots.forEach(function (roleId) {
            _this.draw(roleId, _this.handCardsNum);
            _this.handCards[roleId].sort(sortingAlgorithm);
        });
        var cards = this.cardsPool.getCardsOfCardsPool(this.bookmakerCardsNum);
        if (cards.length < this.bookmakerCardsNum)
            throw new Error("cards pool is not have ".concat(this.bookmakerCardsNum, " cards"));
        this.bookmakerCards = cards;
    };
    /**
     * 出牌
     * @param roleId 出牌的用户Id
     * @param cards 牌的槽位
     */
    RefereeVo.prototype.play = function (roleId, cards) {
        var _this = this;
        var nums = this.handCards[roleId];
        var slots = NumericalValueUtil_1.default.findArrayElementIndex(nums, cards);
        if (!slots)
            return;
        slots.forEach(function (element) {
            _this.handCards[roleId][element] = null;
        });
    };
    /**
     * 摸牌
     * @param roleId 摸牌的用户Id
     */
    RefereeVo.prototype.draw = function (roleId, num) {
        var _this = this;
        if (!this.cardsPool)
            throw new Error("cards pool is not init!");
        var cards = this.cardsPool.getCardsOfCardsPool(num);
        if (cards.length < num)
            throw new Error("cards pool is not have ".concat(num, " cards"));
        cards.forEach(function (element) {
            _this.handCards[roleId].push(element);
        });
    };
    /**
     * 设置庄家ID
     * @param roleId 用户ID
     */
    RefereeVo.prototype.setBookmakerUserId = function (roleId) { this.bookmakerUserId = roleId; };
    /**
     * 回合数自增
     */
    RefereeVo.prototype.autoIncrementNumberOfRounds = function () { this.numberOfRounds += 1; };
    ;
    /**
     * 增加倍率
     * @param magnification 倍率
     */
    RefereeVo.prototype.increaseMagnification = function (roleId, magnification) {
        this.magnification[roleId] = magnification; // magnification;
    };
    /**
     * 设置倒计时时间
     *
     * 默认为25秒
     * @param time 单位为秒
     */
    RefereeVo.prototype.setCountdownTime = function (time) { this.countdownTime = time; };
    ;
    /**
     * 获取倒计时时间(秒)
     */
    RefereeVo.prototype.getCountdownTime = function () { return this.countdownTime; };
    ;
    /**
     * 设置倒计时时间起始时间
     * @param time 时间戳
     */
    RefereeVo.prototype.setCountdownStartTime = function (time) { this.countdownStartTime = time; };
    ;
    /**
     * 获取倒计时时间(时间戳)
     */
    RefereeVo.prototype.getCountdownStartTime = function () { return this.countdownStartTime; };
    ;
    /**
     * 谁的回合
     */
    RefereeVo.prototype.getTurnId = function () { return this.turnId; };
    ;
    /**
     * 获取对应用户的手牌
     * @param roleId
     */
    RefereeVo.prototype.getUserHandCards = function (roleId) {
        return this.handCards[roleId];
    };
    return RefereeVo;
}());
exports.default = RefereeVo;
