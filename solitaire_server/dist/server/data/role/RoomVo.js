"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var RoomStatusType_1 = require("./RoomStatusType");
var RoomVo = /** @class */ (function () {
    function RoomVo() {
        this.users = [];
        this.numberOfUsers = 0;
        this.status = RoomStatusType_1.ROOM_STATUS.waiting;
        this.prepareQueue = []; // 准备队列
        this.referee = null; // 裁判
    }
    RoomVo.prototype.init = function (roomId, maximumNumberRooms, roomType) {
        this.roomId = roomId;
        this.roomCanHaveMaximumPeople = maximumNumberRooms;
        this.roomType = roomType;
        for (var i = 0; i < this.roomCanHaveMaximumPeople; i++) {
            this.users.push(null);
        }
    };
    /**
     * 房间是否已满
     * @returns
     */
    RoomVo.prototype.isRoomFull = function () { return this.roomCanHaveMaximumPeople <= this.numberOfUsers; };
    ;
    /**
     * 房间是否为空
     * @returns
     */
    RoomVo.prototype.isRoomEmpty = function () { return this.numberOfUsers <= 0; };
    ;
    /**
     * 是否都已经准备了
     * @returns
     */
    RoomVo.prototype.isAllPrepare = function () { return this.prepareQueue.length === this.numberOfUsers; };
    ;
    /**
     * 用户加入到房间
     * @param userVo 被添加的用户Vo
     * @returns
     */
    RoomVo.prototype.addToUser = function (userVo) {
        if (this.isRoomFull())
            return false;
        if (this.findUser(userVo.roleId))
            return false;
        var index = this.findFirstEmptySlot();
        if (index === null)
            return false;
        this.users[index] = userVo;
        this.numberOfUsers += 1;
        return true;
    };
    /**
     * 用户离开房间
     * @param userId 被删除的用户ID
     * @returns
     */
    RoomVo.prototype.removeUser = function (userId) {
        var index = this.findUserIndex(userId);
        if (this.isRoomEmpty())
            return false;
        if (index === null)
            return false;
        this.users[index] = null;
        this.numberOfUsers -= 1;
        return true;
    };
    /**
     * 更改当前房间状态
     * @param status 房间状态
     */
    RoomVo.prototype.changeRoomStatus = function (status) {
        this.status = status;
    };
    /**
     * 寻找第一个空槽位
     * @param userVo 用户Vo
     */
    RoomVo.prototype.findFirstEmptySlot = function () {
        for (var index = 0; index < this.users.length; index++) {
            var user = this.users[index];
            if (!user)
                return index;
        }
        return null;
    };
    /**
     * 根据ID在当前房间内找到对应的下标
     * @param userId 用户ID
     * @returns
     */
    RoomVo.prototype.findUserIndex = function (userId) {
        for (var index = 0; index < this.users.length; index++) {
            var user = this.users[index];
            if (!user)
                continue;
            if (user.roleId === userId)
                return index;
        }
        return null;
    };
    /**
     * 根据ID在当前房间内找到对应的用户
     * @param userId 用户ID
     * @returns
     */
    RoomVo.prototype.findUser = function (userId) {
        var index = this.findUserIndex(userId);
        if (index === null)
            return null;
        return this.users[index];
    };
    /**
     * 判断用户是否准备
     * @param userId 用户Id
     * @returns
     */
    RoomVo.prototype.findUserPrepare = function (userId) {
        for (var index = 0; index < this.prepareQueue.length; index++) {
            if (this.prepareQueue[index] === userId)
                return index;
        }
        return null;
    };
    /**
     * 添加用户Id到准备队列中
     * @param userId 用户Id
     */
    RoomVo.prototype.addUserIdToPrepareQueue = function (userId) {
        if (this.findUserPrepare(userId) != null)
            return;
        this.prepareQueue.push(userId);
    };
    /**
     * 移除用户Id到准备队列中
     * @param userId 用户Id
     */
    RoomVo.prototype.removeUserIdToPrepareQueue = function (userId) {
        var index = this.findUserPrepare(userId);
        if (index === null)
            return;
        this.prepareQueue.splice(index, 1);
    };
    /**
     * 移除准备队列中的所有用户
     */
    RoomVo.prototype.removeAllToPrepareQueue = function () { this.prepareQueue = []; };
    ;
    /**
     * 为房间挂载一个裁判
     *
     * 这个函数会自动初始化裁判的同时洗好一套牌组
     * @param referee 裁判(referee)
     * @returns
     */
    RoomVo.prototype.initReferee = function (referee, initBet) {
        if (initBet === void 0) { initBet = 1; }
        if (!this.isAllPrepare())
            return null;
        this.referee = new referee();
        this.referee.init(this.users, initBet);
        return this.referee;
    };
    /**
     * 获取裁判
     * @returns 返回裁判
     */
    RoomVo.prototype.getReferee = function () { return this.referee; };
    /**
     * 移除裁判
     */
    RoomVo.prototype.removeReferee = function () { this.referee = null; };
    return RoomVo;
}());
exports.default = RoomVo;
