export const enum SocketSessionState {
    connecting = "connecting",
    connected = "connected",
    closed = "closed",
}

export const enum SocketEvent {
    connected = "connected",
    closed = "closed"
}

// export const enum SocketType {
//     hall = "hall"
// }
