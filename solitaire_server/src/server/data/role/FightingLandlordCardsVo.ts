import NumericalValueUtil from "../../util/NumericalValueUtil";
import GameManagerVo from "../GameManagerVo";
import GameStaticDataManager from "../GameStaticDataManager";
import PokerJsonInfo from "../static/PokerJsonInfo";
import UserManagerVo from "../UserManagerVo";
import GameScoringVo from "./GameScoringVo";
import { PLAY_CARDS_TYPE } from "./PlayCardsType";
import { POKER_POINTS } from "./PokerPointsType";
import RefereeVo from "./RefereeVo";
import { ROOM_STATUS } from "./RoomStatusType";
import UserVo from "./UserVo";

export default class FightingLandlordCardsVo extends RefereeVo {

    lastPlayedCardsRoleId: number | null = null;                                // 最近一次打出的牌组的人

    protected cardsPlayedByUsers: { [key: number]: Array<number> | null } = {};         // 记录用户打出的牌组

    protected callLandlordByUsers: { [key: number]: boolean | null } = {};              // 记录用户是否叫地主

    protected landlordCandidateQueue: Array<number> = [];                               // 判断到谁地主用户队列

    constructor() { super(); }

    init(users: Array<UserVo | null>, initBet: number = 1): void {
        const bookmakerCardsNum = 3;
        const handCardsNum = 16;
        super.init(users, initBet, bookmakerCardsNum, handCardsNum);
        this.turnId = this.randomFirstCandidate();
        users.forEach(element => {
            if (!element) return;
            this.cardsPlayedByUsers[element.roleId] = null;
            this.callLandlordByUsers[element.roleId] = null;
        });
    }

    sortingAlgorithm(a: number | null, b: number | null): number {
        if (a === b && !a) return 0;
        if (a == null) return 1;
        if (b == null) return -1;
        const jsonInfo1 = GameStaticDataManager.pokerJsonInfoMap.get(a);
        const jsonInfo2 = GameStaticDataManager.pokerJsonInfoMap.get(b);
        if (jsonInfo1!.pokerPoint == jsonInfo2!.pokerPoint) return a - b;
        return jsonInfo1!.landlordPointWeights - jsonInfo2!.landlordPointWeights;
    }

    deal(): void { super.deal(this.sortingAlgorithm); };

    play(roleId: number, cards: Array<number>): void {
        super.play(roleId, cards);
        this.cardsPlayedByUsers[roleId] = cards;
        if (cards.length == 0) return;
        this.lastPlayedCardsRoleId = roleId;
    }

    setBookmakerUserId(roleId: number | null): void {
        if (roleId == null) return;
        super.setBookmakerUserId(roleId);
        this.bookmakerCards.forEach(element => {
            this.handCards[roleId].push(element);
        });
        this.handCards[roleId].sort(this.sortingAlgorithm);
    }

    /**
     * 初始化记牌器
     */
    private initCardsCounter(): Map<POKER_POINTS, number> {
        let cardsCounter: Map<POKER_POINTS, number> = new Map<POKER_POINTS, number>();
        GameStaticDataManager.pokerJsonInfoMap.forEach((value, key) => {
            if (value.pokerPoint == POKER_POINTS.none || 
                value.pokerPoint == POKER_POINTS.point_a || 
                value.pokerPoint == POKER_POINTS.point_2) 
                return;
            cardsCounter.set(value.pokerPoint as POKER_POINTS, 0);
        });
        cardsCounter.set(POKER_POINTS.point_a, 0);
        cardsCounter.set(POKER_POINTS.point_2, 0);
        cardsCounter.set(POKER_POINTS.none, 0);
        return cardsCounter;
    }

    /**
     * 设置记牌器
     */
    private setCardsCounter(cardsCounter: Map<POKER_POINTS, number>, cards: Array<number>) {
        const jsonInfos = GameStaticDataManager.getPokerJsonInfoMap(cards);
        jsonInfos.forEach(element => {
            const value = cardsCounter.get(element.pokerPoint as POKER_POINTS);
            cardsCounter.set(element.pokerPoint as POKER_POINTS, value! + 1);
        });
    }

    isComplyRules(roleId: number, cards: Array<number>): boolean {
        const curPlayType = this.determinePlayCardsType(cards);
        if (this.isMustPlayCards(roleId)) {
            if (curPlayType == PLAY_CARDS_TYPE.none) return false;
            return true;
        }
        if (curPlayType != PLAY_CARDS_TYPE.none) {
            const lastPlayedCards = this.cardsPlayedByUsers[this.lastPlayedCardsRoleId!];
            if (lastPlayedCards == null) return true;
            return this.compareCards(cards, lastPlayedCards);
        }
        return true;
    };

    isGameOver(): number | null {
        for (let index = 0; index < this.slots.length; index++) {
            const roleId = this.slots[index];
            if (this.findMinCards(roleId) == null) return roleId;
        }
        return null;
    }

    increaseMagnificationOfCards(cards: Array<number>): void {
        const curPlayType = this.determinePlayCardsType(cards);
        let multiple = 1;
        if (curPlayType == PLAY_CARDS_TYPE.rocket) multiple = multiple * 3;
        if (curPlayType == PLAY_CARDS_TYPE.bomb) multiple = multiple * 2;
        Object.keys(this.magnification).forEach(key => {
            const value = this.magnification[Number(key)];
            this.magnification[Number(key)] = value * multiple;
        });
    };

    compareCards(cards1: Array<number>, cards2: Array<number>): boolean { 
        const curPlayType = this.determinePlayCardsType(cards1);
        const lastPlayedType = this.determinePlayCardsType(cards2);
        if (curPlayType == PLAY_CARDS_TYPE.rocket) return true;
        if (lastPlayedType == PLAY_CARDS_TYPE.rocket) return false;
        if (curPlayType == PLAY_CARDS_TYPE.bomb) {
            if (lastPlayedType != PLAY_CARDS_TYPE.bomb) return true;
            if (lastPlayedType == PLAY_CARDS_TYPE.bomb) {
                if (this.comparePoint(cards1[0], cards2[0]) > 0) return true;
                return false;
            }
        }
        if (curPlayType != lastPlayedType) return false;
        if (cards2.length != cards1.length) return false;
        if (curPlayType == PLAY_CARDS_TYPE.single ||
            curPlayType == PLAY_CARDS_TYPE.pair ||
            curPlayType == PLAY_CARDS_TYPE.threeOfKind) {
            if (this.comparePoint(cards1[0], cards2[0]) > 0) return true;
            return false;
        }
        let repeatingNum = 0;
        if (curPlayType == PLAY_CARDS_TYPE.singleStraight) repeatingNum = 1;
        if (curPlayType == PLAY_CARDS_TYPE.doubleStraight) repeatingNum = 2;
        if (curPlayType == PLAY_CARDS_TYPE.threeWithSingle || 
            curPlayType == PLAY_CARDS_TYPE.threeWithPair ||
            curPlayType == PLAY_CARDS_TYPE.triStraight ||
            curPlayType == PLAY_CARDS_TYPE.planeWithWings) {
            repeatingNum = 3;
        }
        if (curPlayType == PLAY_CARDS_TYPE.fourWitTwo) repeatingNum = 4;
        const curMinCard = this.getMinCardsPointConsecutive(cards1, repeatingNum)!;
        const lastMinCard = this.getMinCardsPointConsecutive(cards2, repeatingNum)!;
        if (this.comparePoint(curMinCard, lastMinCard) > 0) return true;
        return false;
    }

    /**
     * 随机返回第一个地主候选人的ID
     */
    private randomFirstCandidate(): number {
        const users = Object.keys(this.handCards);
        const num = NumericalValueUtil.getRandomInt(0, users.length - 1);
        const roleId = users[num];
        return Number(roleId);
    }

    /**
     * 是否叫地主, 抢地主
     * @param roleId 用户Id
     * @param isCall 是否叫地主
     */
    callLandlord(roleId: number, isCall: boolean): void {
        if (roleId != this.turnId) return;
        this.callLandlordByUsers[roleId] = isCall;
        if (!isCall) this.landlordCandidateQueue.push(0);
        else this.landlordCandidateQueue.push(roleId);
    }

    /**
     * 确定庄家
     * 
     * 按照用户的人数来确定一轮， 一轮结束将进行一次遍历，确定当前叫地主和抢地主的人数。
     * 如果第一轮结束后有超过一个人叫或抢地主，则不管， 如果只有一人叫，则确定那人为地主
     * 第二轮开始，在第一个抢地主还是不抢地主，将直接确定庄家
     */
    dealerBookmaker(): number | null {
        let result: number | null = null;
        if (this.landlordCandidateQueue.length < this.slots.length) return result;
        let curLandlordCandidateNum: number = 0;
        for (let i = this.landlordCandidateQueue.length - 1; i >= 0; i--) {
            if (this.landlordCandidateQueue[i] != 0) curLandlordCandidateNum += 1;
        }
        if (curLandlordCandidateNum == 0) {
            result = this.randomFirstCandidate();
            return result;
        }
        if (this.landlordCandidateQueue.length == 3 && curLandlordCandidateNum > 1) return result;
        for (let i = this.landlordCandidateQueue.length - 1; i >= 0; i--) {
            if (this.landlordCandidateQueue[i] != 0) {
                result = this.landlordCandidateQueue[i];
                break;
            }
        }
        return result;
    }

    refreshWhoTurned(status: ROOM_STATUS): void {
        let roleId: number | null = null;
        if (status === ROOM_STATUS.inGame) roleId = this.refreshWhoPlay();
        else if (status === ROOM_STATUS.setBookmaker) roleId = this.refreshWhoCallLandlord();
        this.turnId = roleId;
    }

    /**
     * 轮到谁出牌
     */
    private refreshWhoPlay(): number | null {
        if (this.bookmakerUserId === null) return null;
        const index = this.slots.indexOf(this.bookmakerUserId);
        const intervalValue = this.numberOfRounds;
        return NumericalValueUtil.findValueOfArray(this.slots, index, intervalValue);
    }

    /**
     * 轮到谁叫地主
     */
    private refreshWhoCallLandlord(): number | null {
        if (this.turnId === null) return null;
        const index = this.slots.indexOf(this.turnId);
        const intervalValue = 1;
        return NumericalValueUtil.findValueOfArray(this.slots, index, intervalValue);
    }

    calculateReward(winner: number): GameScoringVo {
        if (winner === this.bookmakerUserId) return this.calculateBookmakerWin();
        return this.calculateIdlerWin();
    }

    /**
     * 计算用户为庄家时赢了
     */
    private calculateBookmakerWin(): GameScoringVo {
        let scoringVo = new GameScoringVo();
        let bookmakerWinMony = 0;
        Object.keys(this.magnification).forEach(key => {
            if (Number(key) == this.bookmakerUserId) return;
            const num = this.calculateIdlerLose(Number(key));
            scoringVo.scoring[Number(key)] = num * -1;
            bookmakerWinMony += num;
        });
        scoringVo.scoring[this.bookmakerUserId!] = bookmakerWinMony;
        return scoringVo;
    }

    /**
     * 计算用户为闲家时赢了
     */
    private calculateIdlerWin(): GameScoringVo {
        let scoringVo = new GameScoringVo();
        const bookmakerLoseMony = this.calculateBookmakerLose(this.bookmakerUserId!);
        scoringVo.scoring[this.bookmakerUserId!] = bookmakerLoseMony * -1;
        Object.keys(this.magnification).forEach(key => {
            if (Number(key) == this.bookmakerUserId) return;
            scoringVo.scoring[Number(key)] = bookmakerLoseMony / (this.slots.length - 1);
        });
        return scoringVo;
    }

    /**
     * 计算用户为庄家时输了
     */
    private calculateBookmakerLose(roleId: number): number {
        let money = 0;
        Object.keys(this.magnification).forEach(key => {
            if (Number(key) == roleId) return;
            const magnification = this.magnification[Number(key)];
            money += this.initBet * magnification;
        });
        if (GameManagerVo.userManager.isEnoughMoney(roleId, money)) return money;
        else return GameManagerVo.userManager.getUserMony(roleId)!;
    }

    /**
     * 计算用户为闲家时输了
     */
    private calculateIdlerLose(roleId: number): number {
        const magnification = this.magnification[roleId];
        const money = magnification * this.initBet;
        if (GameManagerVo.userManager.isEnoughMoney(roleId, money)) return money;
        else return GameManagerVo.userManager.getUserMony(roleId)!;
    }

    /**
     * 有多少用户没有出牌(当前一圈儿)
     */
    private numberOfUsersNotPlayCards(): number {
        const keys = Object.keys(this.cardsPlayedByUsers);
        let result = 0;
        for (const key of keys) {
            const cards = this.cardsPlayedByUsers[Number(key)];
            if (cards == null || cards.length == 0) result += 1;
        }
        return result;
    }

    /**
     * 找到用户当前手里最小的那张牌
     * @param roleId 用户Id
     */
    findMinCards(roleId: number): number | null {
        for (let cardId of this.handCards[roleId]) {
            if (cardId != null) return cardId;
        }
        return null;
    }
    
    /**
     * 是否轮到自己必须出牌
     * @param roleId 用户id
     * @returns 
     */
    isMustPlayCards(roleId: number): boolean {
        const num = this.numberOfUsersNotPlayCards();
        const haveThreeUserNotPlayCards = (num == 3);
        const IsMePlayedLastCard = (this.turnId == this.lastPlayedCardsRoleId);
        const isMyTurn = (this.turnId == roleId);
        if (haveThreeUserNotPlayCards || (num == 2 && IsMePlayedLastCard && isMyTurn)) return true;
        return false;
    }

    /**
     * 比较点数大小
     * @param cardId1 
     * @param cardId2 
     * @returns 
     */
    private comparePoint(cardId1: number, cardId2: number): number {
        const jsonInfo1 = GameStaticDataManager.pokerJsonInfoMap.get(cardId1);
        const jsonInfo2 = GameStaticDataManager.pokerJsonInfoMap.get(cardId2);
        if (jsonInfo1!.landlordPointWeights == jsonInfo2!.landlordPointWeights) return 0;
        if (jsonInfo1!.landlordPointWeights > jsonInfo2!.landlordPointWeights) return 1;
        return -1;
    }

    /**
     * 确定牌组类型
     * @param cards 卡牌组
     * @returns 确定牌组类型
     */
    determinePlayCardsType(cards: Array<number>): PLAY_CARDS_TYPE {
        let cardsCounter = this.initCardsCounter();
        this.setCardsCounter(cardsCounter, cards);
        const notRepeatingCards = this.getPointOfNotRepeating(cards);
        if (cards.length == 0) return PLAY_CARDS_TYPE.none;
        if (cards.length == 1) return PLAY_CARDS_TYPE.single;
        if (cards.length == 2 && notRepeatingCards.length == 1) {
            const jsonInfo = GameStaticDataManager.pokerJsonInfoMap.get(cards[0])!; 
            if (jsonInfo.pokerPoint == POKER_POINTS.none) 
                return PLAY_CARDS_TYPE.rocket;
            return PLAY_CARDS_TYPE.pair;
        }
        if (cards.length == 3 && notRepeatingCards.length == 1) {
            return PLAY_CARDS_TYPE.threeOfKind;
        }
        if (cards.length == 4) {
            if (notRepeatingCards.length == 1) return PLAY_CARDS_TYPE.bomb;
            if (notRepeatingCards.length == 2) {
                if (cardsCounter.get(notRepeatingCards[0] as POKER_POINTS) == 3) return PLAY_CARDS_TYPE.threeWithSingle;
                if (cardsCounter.get(notRepeatingCards[1] as POKER_POINTS) == 3) return PLAY_CARDS_TYPE.threeWithSingle;
            }
        }
        if (cards.length == 5 && notRepeatingCards.length == 2) {
            if (cardsCounter.get(notRepeatingCards[0] as POKER_POINTS) == 3) return PLAY_CARDS_TYPE.threeWithPair;
            if (cardsCounter.get(notRepeatingCards[1] as POKER_POINTS) == 3) return PLAY_CARDS_TYPE.threeWithPair;
        }
        if (cards.length >= 5) {
            let consecutiveLength = this.getCardsPointConsecutiveLengthOfNum(cardsCounter, 1);
            if (consecutiveLength >= 5 && cards.length % consecutiveLength == 0) {
                if (cardsCounter.get(POKER_POINTS.point_2)) return PLAY_CARDS_TYPE.none;
                return PLAY_CARDS_TYPE.singleStraight;
            }
            consecutiveLength = this.getCardsPointConsecutiveLengthOfNum(cardsCounter, 2);
            if (consecutiveLength >= 3 && cards.length % consecutiveLength == 0) {
                if (cardsCounter.get(POKER_POINTS.point_2)) return PLAY_CARDS_TYPE.none;
                return PLAY_CARDS_TYPE.doubleStraight;
            }
            consecutiveLength = this.getCardsPointConsecutiveLengthOfNum(cardsCounter, 3);
            if (consecutiveLength >= 2) {
                if (cards.length % consecutiveLength == 0) {
                    if (cardsCounter.get(POKER_POINTS.point_2)) return PLAY_CARDS_TYPE.none;
                    return PLAY_CARDS_TYPE.triStraight;
                }
                // isSameQuantity 判断 牌组A带B这种规则时， 用来判断是否满足A的不一样的点数数量要等于B的不一样的点数数量
                const isSameQuantity = notRepeatingCards.length == (consecutiveLength * 2);
                // isNumberLessThanTwo 判断 牌组A带B这种规则时， 用来限制B的相同点数的数量等于1，或等于2
                const remainingNum = (cards.length - consecutiveLength * 3) / consecutiveLength;
                const isNumberLessThanTwo = remainingNum == 2 || remainingNum == 1;
                if (isSameQuantity && isNumberLessThanTwo) return PLAY_CARDS_TYPE.planeWithWings;
            }
            consecutiveLength = this.getCardsPointConsecutiveLengthOfNum(cardsCounter, 4);
            if (consecutiveLength >= 1) {
                const isSameQuantity = notRepeatingCards.length == (consecutiveLength * 2);
                const remainingNum = (cards.length - consecutiveLength * 3) / consecutiveLength;
                const isNumberLessThanTwo = remainingNum == 2 || remainingNum == 1;
                if (isSameQuantity && isNumberLessThanTwo) return PLAY_CARDS_TYPE.planeWithWings;
            }
        }
        return PLAY_CARDS_TYPE.none;
    }

    /**
     * 获取点数不重复的卡牌组
     * @param cards 卡牌组
     * @returns 点数不重复的卡牌组
     */
    private getPointOfNotRepeating(cards: Array<number>): Array<string> {
        let result: Array<string> = []
        let tempBuffer: Set<string> = new Set();
        for (let card of cards) {
            const point = GameStaticDataManager.pokerJsonInfoMap.get(card)!.pokerPoint;
            if (tempBuffer.has(point)) continue;
            tempBuffer.add(point);
            result.push(point)
        }
        return result;
    }

    /**
     * 获取顺子的长度
     * @param cardsCounter 卡牌计数器
     * @param num 顺子中对应的每个点数应有num张
     * @returns 
     */
    private getCardsPointConsecutiveLengthOfNum(cardsCounter: Map<string, number>, num: number): number {
        let length = 0;
        let isBegin = false;
        let isEnd = false;
        cardsCounter.forEach((value, key) => {
            if (value == num) isBegin = true;
            if (isBegin && value == 0) isEnd = true;
            if (isBegin && !isEnd) if (value == num) length += 1;
        });
        return length;
    }

    /**
     * 获取顺子的最小的点数
     * @param cards 卡牌组
     * @param num 顺子中对应的每个点数应有num张
     * @returns 
     */
    private getMinCardsPointConsecutive(cards: Array<number>, num: number) {
        let minCard: number | null = null;
        let jsonInfos: Array<PokerJsonInfo>= [];
        cards.forEach(element => {
            const jsonInfo = GameStaticDataManager.pokerJsonInfoMap.get(element);
            if (!jsonInfo) return;
            jsonInfos.push(jsonInfo);
        });
        jsonInfos.sort((a, b) => { return a.landlordPointWeights - b.landlordPointWeights });
        let cardCounter = new Map<string, number>();
        for (const jsonInfo of jsonInfos) {
            let value = 0;
            if (cardCounter.has(jsonInfo.pokerPoint)) value = cardCounter.get(jsonInfo.pokerPoint)!;
            if (value + 1 >= num) {
                minCard = jsonInfo.id;
                break;
            };
            cardCounter.set(jsonInfo.pokerPoint, value + 1);
        }
        return minCard;
    }

}