import mysql from "mysql";
import dbConfig from "./DataBaseConfig";

export default class DataBaseManager {

    private static pool = mysql.createPool(dbConfig);

    static query(sql: string): Promise<any> | null {
        return new Promise<any>((resolve, reject) => {
            this.pool.getConnection((err, connection) => {
                try {
                    if (err) throw new Error(err.message);
                    connection.query(sql, (error, results) => {
                        if (error) throw new Error(error.message);
                        else resolve(JSON.stringify(results));
                        connection.release(); // 释放该链接，把该链接放回池里供其他人使用;
                    });
                } catch (e) {
                    console.error(e);
                    connection.release(); // 释放该链接，把该链接放回池里供其他人使用;
                    return null;
                }
            });
        });
    }

    static getStaticData(tableName: string): Promise<any> | null {
		const results = this.query(`SELECT * FROM ${tableName}`);
        if (results) return results;
        console.log(`load ${tableName} failed`);
        return null;
    }

}
