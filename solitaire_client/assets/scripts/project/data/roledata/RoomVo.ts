import GameRoleDataManager from "../GameRoleDataManager";
import FightingLandlordCardsVo from "./FightingLandlordCardsVo";
import RefereeVo from "./RefereeVo";
import { ROOM_STATUS } from "./RoomStatusType";
import { ROOM_TYPE } from "./RoomType";
import UserVo from "./UserVo";

export default class RoomVo {
    
    roomId!: number;

    roomCanHaveMaximumPeople!: number;

    users: Array<UserVo | null> = [];   // 初始化将会用一下

    private numberOfUsers: number = 0;

    status: ROOM_STATUS = ROOM_STATUS.waiting;

    prepareQueue: Array<number> = [];           // 准备队列

    private referee: RefereeVo = null;           // 裁判

    private roomType!: ROOM_TYPE;

    initJsonInfo(resp: any): void {
        this.roomId = resp.roomId;
        this.roomCanHaveMaximumPeople = resp.roomCanHaveMaximumPeople;
        this.initUsers(resp.users);
        this.numberOfUsers = resp.numberOfUsers;
        this.status = resp.status;
        this.prepareQueue = resp.prepareQueue;
        this.roomType = resp.roomType;
        if (!resp.referee) {
            this.referee = null
            return;
        }
        if (this.roomType == ROOM_TYPE.FightingLandlord) {
            this.referee = new FightingLandlordCardsVo();
        } else if (this.roomType == ROOM_TYPE.TexasHoldEmPoker) {
            // ...
        }
        this.referee.initJsonInfo(resp.referee);
    }

    /**
     * 初始化并改编排序用户顺序
     * 
     * 将自己的用户的Vo改变到第一个
     */
    initUsers(users: any): void {
        let temp_users = [];
        let isFind = false;
        const roleId = GameRoleDataManager.userVo.roleId;
        for (const user of users) {
            let userVo: UserVo | null = null;
            if (user) {
                userVo = new UserVo();
                userVo.initJsonInfo(user);
                if (user.roleId === roleId) isFind = true;
            }
            if (!isFind) temp_users.push(userVo);
            else this.users.push(userVo);
        }
        temp_users.forEach(element => {
            this.users.push(element);
        });
    }

    /**
     * 房间是否已满
     * @returns 
     */
    isRoomFull(): boolean { return this.roomCanHaveMaximumPeople <= this.numberOfUsers; };

    /**
     * 房间是否为空
     * @returns 
     */
    isRoomEmpty(): boolean { return this.numberOfUsers <= 0; };

    /**
     * 判断用户是否准备
     * @param userId 用户Id
     * @returns 
     */
    isUserPrepare(): boolean {
        for (let value of this.prepareQueue) {
            if (value === GameRoleDataManager.userVo.roleId) return true;
        }
        return false;
    }

    /**
     * 获取裁判
     * @returns 返回裁判 
     */
    getReferee<T>(): T | null { return this.referee as unknown as (T | null); };


    /**
     * 改变房间状态
     * @param status 
     */
    changeRoomStatus(status: ROOM_STATUS): void {
        this.status = status;
    }

    /**
     * 设置房间到初始状态，但不清理用户
     */
    setInitializationState(): void {
        this.changeRoomStatus(ROOM_STATUS.waiting);
        this.prepareQueue = [];
        this.referee = null;
    }
    

}