import FightingLandlordCardsVo from "../../../data/roledata/FightingLandlordCardsVo";
import { ROOM_STATUS } from "../../../data/roledata/RoomStatusType";
import UserVo from "../../../data/roledata/UserVo";
import Poker from "../../poker/Poker";
import Role from "./Role";

const {ccclass, property} = cc._decorator;

@ccclass
export default class OtherSlot extends cc.Component {

    @property(Role)
    private role: Role = null;

    @property(cc.Node)
    private cardsNode: cc.Node = null;

    @property(cc.Label)
    private cardsNumber: cc.Label = null;

    @property(cc.Node)
    private alarmClock: cc.Node = null;

    @property(cc.Label)
    private countdown: cc.Label = null;

    @property(cc.Node)
    private showCardsContent: cc.Node = null;

    @property(cc.Label)
    private userStatusLabel: cc.Label = null;

    @property(cc.Prefab)
    private pokerPrefab: cc.Prefab = null;

    private userVo: UserVo = null;
    
    private referee: FightingLandlordCardsVo = null;

    private roomStatus: ROOM_STATUS = null;

    init(userVo: UserVo, referee: FightingLandlordCardsVo, status: ROOM_STATUS) {
        this.userVo = userVo;
        this.referee = referee;
        this.roomStatus = status;
        this.role.init(userVo, referee?.bookmakerUserId);
        if (this.roomStatus === ROOM_STATUS.waiting) return;
        this.refreshCardStatus();
        this.refreshCountdown();
        this.refreshPokerStatus();
    }

    /**
     * 改变槽到等待状态的样式
     */
    changeSlotToWaitStatus(): void {
        this.cardsNode.active = false;
        this.alarmClock.active = false;
        this.showCardsContent.active = false;
        this.userStatusLabel.node.active = false;
    }

    refreshCardStatus() {
        const cardsNum = this.referee.curHandCardNumber(this.userVo.roleId);
        this.cardsNumber.string = `${cardsNum}`;
        if (cardsNum <= 0) this.cardsNode.active = false;
        else this.cardsNode.active = true;
    }

    refreshCountdown() {
        this.unschedule(this.showCountdown);
        this.alarmClock.active = false;
        if (this.referee.turnId != this.userVo.roleId) return;
        this.alarmClock.active = true;
        this.schedule(this.showCountdown, 1);
        this.showCountdown();
    }

    private showCountdown() {
        const countdownTime = this.referee.remainingTime;
        if (countdownTime >= 0) {
            this.countdown.string = `${Math.round(countdownTime / 1000)}`;
            return;
        }
        this.unschedule(this.showCountdown);
    }

    refreshPokerStatus() {
        this.userStatusLabel.node.active = false;
        this.showCardsContent.active = false;
        if (this.roomStatus == ROOM_STATUS.setBookmaker) {
            const isCallLandlord = this.referee.callLandlordByUsers[this.userVo.roleId];
            if (isCallLandlord == null || this.referee.turnId == this.userVo.roleId) return;
            this.userStatusLabel.node.active = true;
            if (isCallLandlord == true) this.userStatusLabel.string = "叫地主";
            else this.userStatusLabel.string = "不叫地主";
        }
        else if (this.roomStatus == ROOM_STATUS.inGame || this.roomStatus == ROOM_STATUS.GameOver) {
            this.userStatusLabel.string = "不要";
            const cards = this.referee.cardsPlayedByUsers[this.userVo.roleId];
            if (cards == null || this.referee.turnId == this.userVo.roleId) return;
            this.refreshPlayCards(cards);
            if (cards.length == 0) this.userStatusLabel.node.active = true;
            else this.showCardsContent.active = true;
        };
    }

    refreshPlayCards(cards: Array<number>): void {
        this.showCardsContent.removeAllChildren();
        cards.forEach(cardId => {
            let node: cc.Node = cc.instantiate(this.pokerPrefab);
            let poker = node.getComponent(Poker);
            poker.init(cardId);
            this.showCardsContent.addChild(node);
        });
    }


}
