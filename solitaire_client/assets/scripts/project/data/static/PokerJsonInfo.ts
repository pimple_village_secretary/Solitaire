export default class PokerJsonInfo {
	id: number;
	name: string;
	pokerPoint: string;
	pokerSuit: number;
	faceCard: number;
	landlordPointWeights: number;
}