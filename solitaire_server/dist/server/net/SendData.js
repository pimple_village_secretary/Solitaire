"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var SendData = /** @class */ (function () {
    function SendData(message, cmdIndex, method) {
        this.message = message;
        this.cmdIndex = cmdIndex;
        this.method = method;
    }
    return SendData;
}());
exports.default = SendData;
