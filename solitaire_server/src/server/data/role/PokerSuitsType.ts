export enum POKER_SUITS {
    diamond = 0,    // 方块
    club = 1,       // 梅花
    heart = 2,      // 红心
    spade = 3,      // 黑桃
    none = -1,      // 无花色
}