import * as fs from "fs";

export default class LogUtil {

    private static filePath = "./log/";

    private static serverFileName = "server_log";

    private static fdMap: Map<string, number> = new Map();

    static createServerLog(): Promise<void> {
        return new Promise<void>((resolve, reject) => {
            const serverFile = this.filePath + this.serverFileName;
            fs.open(serverFile, "w", (err, fd) => {
                if (err) throw new Error(err.message);
                this.fdMap.set(this.serverFileName, fd);
                resolve();
            });
        });
    }

    static writeServerLog(title: string, logStr: string) {
        const fd = this.fdMap.get(this.serverFileName);
        if (fd == undefined) throw new Error(`server's fd is not exist!`);
        const str = `[${title}][${Date()}]: ${logStr}\n`;
        fs.writeFileSync(fd, str);
    }

    static closeServerLog(): void {
        const fd = this.fdMap.get(this.serverFileName);
        if (fd == undefined) throw new Error(`server's fd is not exist!`);
        fs.closeSync(fd);
        this.fdMap.delete(this.serverFileName);
    }

    // static writeRoomLog(roomId: number, logStr: string) {
    //     const fd = this.fdMap.get(roomId.toString());
    //     if (fd == undefined) throw new Error(`can't get room log file of fd`);
    //     fs.writeFileSync(fd, logStr + "\n");
    // }

    // static createRoomLog(roomId: number): Promise<void> { 
    //     return new Promise<void>((resolve, reject) => {
    //         const timeNowStr = Date.now().toString();
    //         const fileName = this.filePath + `${roomId}-${timeNowStr}`;
    //         fs.open(fileName, "w", (err, fd) => {
    //             if (err) throw new Error(err.message);
    //             this.fdMap.set(roomId.toString(), fd);
    //             resolve();
    //         });
    //     });
    // }

    // static closeRoomLog(roomId: number): void { 
    //     const fd = this.fdMap.get(roomId.toString());
    //     if (!fd) throw new Error(`room ${roomId}'s fd is not exist!`);
    //     fs.closeSync(fd);
    //     this.fdMap.delete(roomId.toString());
    // }

    static closeAllLogs(): void {
        this.fdMap.forEach((value, key) => {
            if (value == undefined) throw new Error(`server's fd is not exist!`);
            fs.closeSync(value);
        });
        this.fdMap.clear();
    }

}