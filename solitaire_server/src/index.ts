import { Server } from "socket.io";
import GameManagerVo from "./server/data/GameManagerVo";
import GameStaticDataManager from "./server/data/GameStaticDataManager";
import FightingLandlordCardsVo from "./server/data/role/FightingLandlordCardsVo";
import DataBaseManager from "./server/dataBase/DataBaseManager";
import ClientToServerEventHandler from "./server/handler/ClientToServerEventHandler";
import NetManager from "./server/net/NetManager";
import LogUtil from "./server/util/LogUtil";
import TimeUtil from "./server/util/TimeUtil";

const runServer = async () => {
    
    const server = new Server<ClientToServerEvents, ServerToClientEvents, InterServerEvents, SocketData>();
    NetManager.io = require("socket.io")(server, {cors: true});
    
    const server_port = 3000;
    
    console.log(`-----------------------------------------`);
    console.log("initializing the server...");
    
    await GameStaticDataManager.initAll();
    await LogUtil.createServerLog();
    GameManagerVo.init();
    
    
    NetManager.io.on("connection", (socket: any) => {
    
        let count = NetManager.io.engine.clientsCount;
        console.log(`\nhave a client to connect. socket id: ${socket.id}, cur client num: ${count}`);
    
        NetManager.addEventListener(socket, "disconnect", ClientToServerEventHandler.disconnect, false);
    
        NetManager.addEventListener(socket, "login", ClientToServerEventHandler.login, true);
    
        NetManager.addEventListener(socket, "findFreeRoom", ClientToServerEventHandler.findFreeRoomHandler, true);
    
        NetManager.addEventListener(socket, "quitGameRoom", ClientToServerEventHandler.quitGameRoomHandler, true);
    
        NetManager.addEventListener(socket, "switchGameRoom", ClientToServerEventHandler.switchGameRoomHandler, true);
        
        NetManager.addEventListener(socket, "prepareGame", ClientToServerEventHandler.prepareGameHandler, true);
    
        NetManager.addEventListener(socket, "cancelPrepareGame", ClientToServerEventHandler.cancelPrepareGameHandler, true);
    
        NetManager.addEventListener(socket, "callLandlord", ClientToServerEventHandler.callLandlord, true);
    
        NetManager.addEventListener(socket, "playCards", ClientToServerEventHandler.playCards, true);
    
        // NetManager.addEventListener(socket, "startPlay", ClientToServerEventHandler.startPlay, true);
    
    });
    
    // 绑定端口
    NetManager.io.listen(server_port);
    console.log("server started, waiting for connection...");
    console.log(`-----------------------------------------`);
}

runServer();

