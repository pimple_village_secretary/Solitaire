"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var RoomType_1 = require("./role/RoomType");
var RoomManagerVo_1 = __importDefault(require("./RoomManagerVo"));
var UserManagerVo_1 = __importDefault(require("./UserManagerVo"));
var GameManagerVo = /** @class */ (function () {
    function GameManagerVo() {
    }
    GameManagerVo.init = function () {
        GameManagerVo.userManager.init();
        GameManagerVo.roomManager.init(3, RoomType_1.ROOM_TYPE.FightingLandlord);
    };
    GameManagerVo.userManager = new UserManagerVo_1.default();
    GameManagerVo.roomManager = new RoomManagerVo_1.default();
    return GameManagerVo;
}());
exports.default = GameManagerVo;
