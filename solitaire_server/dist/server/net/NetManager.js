"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var ObjUtil_1 = __importDefault(require("../util/ObjUtil"));
var GameManagerVo_1 = __importDefault(require("../data/GameManagerVo"));
var SendData_1 = __importDefault(require("./SendData"));
var NetManager = /** @class */ (function () {
    function NetManager() {
    }
    /**
     * 向当前socket发送数据
     *
     * 备注: 向客户端被动的返回数据, 其客户端监听message事件，通过cmdIndex参数来区分时不同的发送
     * @param userId 用户id
     * @returns
     */
    NetManager.sendDataToClient = function (socket, cmdIndex, message) {
        var sendData = new SendData_1.default(message, cmdIndex, NetManager.defaultListenEvent);
        var massage = JSON.stringify(sendData);
        socket.emit(NetManager.defaultListenEvent, massage);
    };
    /**
     * 向除了当前socket之外的房间所有人发送数据
     *
     * 备注：向客户端主动的发送数据，其客户端监听message事件，通过method参数来区分时不同的发送
     * @param userId 用户id
     * @returns
     */
    NetManager.sendDataToRoomOthersClient = function (socket, roomId, message, method) {
        if (!GameManagerVo_1.default.roomManager.findRoomOfId(roomId)) {
            console.error("room is not find!!!");
            return;
        }
        var sendData = new SendData_1.default(message, 0, method);
        var massage = JSON.stringify(sendData);
        socket.to(roomId).emit(NetManager.defaultListenEvent, massage);
    };
    /**
     * 向房间所有人发送数据
     *
     * 备注：向客户端主动的发送数据，其客户端监听message事件，通过method参数来区分时不同的发送
     * @param userId 用户id
     * @returns
     */
    NetManager.sendDataToRoomAllClient = function (roomId, message, method) {
        if (!GameManagerVo_1.default.roomManager.findRoomOfId(roomId)) {
            console.error("room is not find!!!");
            return;
        }
        var sendData = new SendData_1.default(message, 0, method);
        var massage = JSON.stringify(sendData);
        this.io.in(roomId).emit(NetManager.defaultListenEvent, massage);
    };
    /**
     * 监听事件, 并制定对应的处理函数
     * @param socket socket套接字
     * @param listenEvent 监听事件
     * @param handler 触发时的处理事件函数
     * @param isHaveReturnValue 是否有返回值
     */
    NetManager.addEventListener = function (socket, listenEvent, handler, isHaveReturnValue) {
        socket.on(listenEvent, function (resp) {
            if (isHaveReturnValue) {
                var sendData = JSON.parse(resp);
                var message = sendData.message;
                var cmdIndex = sendData.cmdIndex;
                ObjUtil_1.default.execFunc(handler, socket, cmdIndex, message);
                return;
            }
            ObjUtil_1.default.execFunc(handler, socket);
        });
    };
    NetManager.io = null;
    NetManager.defaultListenEvent = "message";
    return NetManager;
}());
exports.default = NetManager;
