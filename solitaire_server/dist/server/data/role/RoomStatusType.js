"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ROOM_STATUS = void 0;
var ROOM_STATUS;
(function (ROOM_STATUS) {
    ROOM_STATUS[ROOM_STATUS["waiting"] = 0] = "waiting";
    ROOM_STATUS[ROOM_STATUS["setBookmaker"] = 1] = "setBookmaker";
    ROOM_STATUS[ROOM_STATUS["inGame"] = 2] = "inGame";
    ROOM_STATUS[ROOM_STATUS["GameOver"] = 3] = "GameOver";
})(ROOM_STATUS = exports.ROOM_STATUS || (exports.ROOM_STATUS = {}));
