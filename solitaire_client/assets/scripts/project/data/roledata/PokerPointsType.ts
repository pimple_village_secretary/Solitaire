export enum POKER_POINTS {
    point_a = "A",
    point_2 = "2",
    point_3 = "3",
    point_4 = "4",
    point_5 = "5",
    point_6 = "6",
    point_7 = "7",
    point_8 = "8",
    point_9 = "9",
    point_10 = "10",
    point_j = "J",
    point_q = "Q",
    point_k = "K",
    none = "-1"
}
