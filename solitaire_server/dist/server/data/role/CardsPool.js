"use strict";
var __values = (this && this.__values) || function(o) {
    var s = typeof Symbol === "function" && Symbol.iterator, m = s && o[s], i = 0;
    if (m) return m.call(o);
    if (o && typeof o.length === "number") return {
        next: function () {
            if (o && i >= o.length) o = void 0;
            return { value: o && o[i++], done: !o };
        }
    };
    throw new TypeError(s ? "Object is not iterable." : "Symbol.iterator is not defined.");
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var NumericalValueUtil_1 = __importDefault(require("../../util/NumericalValueUtil"));
var CardsPool = /** @class */ (function () {
    function CardsPool() {
        this.cardsPool = new Set(); // 卡牌池
    }
    CardsPool.prototype.init = function () {
        this.cardsPool.clear();
        for (var i = 0; i < CardsPool.cardsNumber;) {
            var cardId = NumericalValueUtil_1.default.getRandomInt(0, CardsPool.cardsNumber - 1);
            if (this.cardsPool.has(cardId))
                continue;
            this.cardsPool.add(cardId);
            i += 1;
        }
    };
    /**
     * 从卡牌池里面获取一定数量的卡牌
     * @param cardsNum 要取的卡牌数量
     * @returns
     */
    CardsPool.prototype.getCardsOfCardsPool = function (cardsNum) {
        var e_1, _a;
        var cards = [];
        var count = 0;
        try {
            for (var _b = __values(this.cardsPool.keys()), _c = _b.next(); !_c.done; _c = _b.next()) {
                var key = _c.value;
                var cardId = key;
                this.cardsPool.delete(key);
                cards.push(cardId);
                count += 1;
                if (count >= cardsNum)
                    return cards;
            }
        }
        catch (e_1_1) { e_1 = { error: e_1_1 }; }
        finally {
            try {
                if (_c && !_c.done && (_a = _b.return)) _a.call(_b);
            }
            finally { if (e_1) throw e_1.error; }
        }
        return cards;
    };
    /**
     * 将卡牌返回给卡牌池
     * @param cards 返回给卡牌池的卡牌组
     */
    CardsPool.prototype.returnCardsToPool = function (cards) {
        var _this = this;
        cards.forEach(function (element) {
            _this.cardsPool.add(element);
        });
    };
    CardsPool.cardsNumber = 4 * 13 + 2; // 4种花色 * 13张点数 + 大王 + 小王 54张
    return CardsPool;
}());
exports.default = CardsPool;
