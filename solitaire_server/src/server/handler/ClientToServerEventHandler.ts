import RoomManagerVo from "../data/RoomManagerVo";
import UserManagerVo from "../data/UserManagerVo";
import SendData from "../net/SendData";
import GameManagerVo from "../data/GameManagerVo";
import NetManager from "../net/NetManager";
import UserVo from "../data/role/UserVo";
import FightingLandlordCardsVo from "../data/role/FightingLandlordCardsVo";
import { ROOM_STATUS } from "../data/role/RoomStatusType";
import RoomVo from "../data/role/RoomVo";
import TimeUtil from "../util/TimeUtil";
import ClientToServerEventInternal from "./ClientToServerEventInternal";
import LogUtil from "../util/LogUtil";

export default class ClientToServerEventHandler {

    ///////////////////////////////////////////////////////////////////////
    // 创建格式
    // 
    // static 函数名称(socket: any, cmdIndex: number, 客户端发送的数据) {
    //      ...   
    // }
    //
    // 注意: socket 和 cmdIndex 参数必备, 即使不需要用到
    ////////////////////////////////////////////////////////////////////////
    
    static login(socket: any, cmdIndex: number, userVo: UserVo) {
        LogUtil.writeServerLog(`server`, `user logged in. socket id: ${socket.id}`)
        if (!userVo) return;
        GameManagerVo.userManager.addToUser(socket, userVo);
    }

    static disconnect(socket: any, cmdIndex: number) {
        LogUtil.writeServerLog(`server`, `user disconnected. socket id: ${socket.id}`);
        GameManagerVo.userManager.removeUser(socket.id);
    }

    static async findFreeRoomHandler(socket: any, cmdIndex: number, roomId: number): Promise<void> {
        LogUtil.writeServerLog(`server`, `socket id ${socket.id} perform action: findGameRoom`);
        try {
            let roomVo = GameManagerVo.roomManager.findFreeRoom(roomId);
            if (!roomVo) {
                roomVo = GameManagerVo.roomManager.createRoom();
            }
            const userVo = GameManagerVo.userManager.findUserUsingSocketId(socket.id);
            if (!userVo) throw new Error(`user is does not exist!`);
            roomVo = GameManagerVo.roomManager.addUserToRoom(roomVo.roomId, userVo);
            if (!roomVo) throw new Error("addUserToRoom error: roomVo null");
            LogUtil.writeServerLog(`room ${roomVo.roomId}`, `${userVo.roleId} join room`);
            socket.join(roomVo.roomId);
            NetManager.sendDataToClient(socket, cmdIndex, {roomVo});
            NetManager.sendDataToRoomOthersClient(socket, roomVo.roomId, {roomVo}, "roomStatus");
        } catch (err: any) {
            LogUtil.writeServerLog(`error`, err);
            NetManager.sendDataToClient(socket, cmdIndex, {err});
            LogUtil.closeAllLogs();
        }
    }

    static quitGameRoomHandler(socket: any, cmdIndex: number, roomId: number): void {
        LogUtil.writeServerLog(`server`, `socket id ${socket.id} perform action: quitGameRoom`);
        try {
            const userVo = GameManagerVo.userManager.findUserUsingSocketId(socket.id);
            if (!userVo) throw new Error(`user is does not exist!`);
            const roomVo = GameManagerVo.roomManager.removeUserToRoom(roomId, userVo.roleId);
            if (!roomVo) throw new Error("removeUserToRoom error: roomVo null");
            LogUtil.writeServerLog(`room ${roomVo.roomId}`, `${userVo.roleId} quit room`);
            socket.leave(roomId);
            NetManager.sendDataToRoomOthersClient(socket, roomVo.roomId, {roomVo}, "roomStatus");
        } catch (err: any) {
            LogUtil.writeServerLog(`error`, err);
            NetManager.sendDataToClient(socket, cmdIndex, {err});
            LogUtil.closeAllLogs();
        }
    }

    static switchGameRoomHandler(socket: any, cmdIndex: number, roomId: number): void {
        LogUtil.writeServerLog(`server`, `socket id ${socket.id} perform action: switchGameRoomHandler`);
        ClientToServerEventHandler.quitGameRoomHandler(socket, cmdIndex, roomId);
        ClientToServerEventHandler.findFreeRoomHandler(socket, cmdIndex, roomId);
    }

    static prepareGameHandler(socket: any, cmdIndex: number, roomId: number): void {
        LogUtil.writeServerLog(`server`, `socket id ${socket.id} perform action: prepareGame`);
        try {
            let roomVo = GameManagerVo.roomManager.findRoomOfId(roomId);
            if (!roomVo) throw new Error(`room is not exist.`);
            const userVo = GameManagerVo.userManager.findUserUsingSocketId(socket.id);
            if (!userVo) throw new Error(`user is does not exist!`);
            roomVo.addUserIdToPrepareQueue(userVo.roleId);
            LogUtil.writeServerLog(`room ${roomVo.roomId}`, `have a user prepare, id: ${userVo.roleId}`);
            if (roomVo.isAllPrepare()) {
                roomVo.changeRoomStatus(ROOM_STATUS.setBookmaker);
                let referee = roomVo.initReferee(FightingLandlordCardsVo);
                if (!referee) return;
                referee.deal();
                referee.setCountdownStartTime(Date.now());
                // TODO: 测试
                referee.setCountdownTime(10);
                // 添加计时器
                const waitTime = (referee.getCountdownTime() + 0.5) * 1000;
                TimeUtil.delayedExecution(`${roomId}`, () => {
                    ClientToServerEventInternal.updateCalledLandlordStatusToFalse(referee!, roomId);
                }, waitTime);
                LogUtil.writeServerLog(`room ${roomVo.roomId}`, `all prepare, and room add referee`);
                LogUtil.writeServerLog(`room ${roomVo.roomId}`, `first turn user is ${referee.getTurnId()}`);
            }
            NetManager.sendDataToClient(socket, cmdIndex, {roomVo});
            NetManager.sendDataToRoomOthersClient(socket, roomId, {roomVo}, "roomStatus");
        } catch (err: any) {
            LogUtil.writeServerLog(`error`, err);
            NetManager.sendDataToClient(socket, cmdIndex, err);
            TimeUtil.removeAllDelayedExecution();
            LogUtil.closeAllLogs();
        }
    }

    static cancelPrepareGameHandler(socket: any, cmdIndex: number, roomId: number): void {
        LogUtil.writeServerLog(`server`, `socket id ${socket.id} perform action: cancelPrepareGame`);
        try {
            let roomVo = GameManagerVo.roomManager.findRoomOfId(roomId);
            if (!roomVo) throw new Error(`room is not exist.`);
            const userVo = GameManagerVo.userManager.findUserUsingSocketId(socket.id);
            if (!userVo) throw new Error(`user is does not exist!`);
            roomVo.removeUserIdToPrepareQueue(userVo.roleId);
            LogUtil.writeServerLog(`room ${roomVo.roomId}`, `have a user no prepare, id: ${userVo.roleId}`);
            NetManager.sendDataToClient(socket, cmdIndex, {roomVo});
            NetManager.sendDataToRoomOthersClient(socket, roomId, {roomVo}, "roomStatus");
        } catch (err: any) {
            LogUtil.writeServerLog(`error`, err);
            NetManager.sendDataToClient(socket, cmdIndex, {err});
            LogUtil.closeAllLogs();
        }
    }

    static callLandlord(socket: any, cmdIndex: number, resp: any): void {
        LogUtil.writeServerLog(`server`, `socket id ${socket.id} perform action: callLandlord`);
        try {
            const roomId: number = resp.roomId;
            const isCall: boolean = resp.isCall;
            TimeUtil.stopDelayedExecution(`${roomId}`);
            const userVo = GameManagerVo.userManager.findUserUsingSocketId(socket.id);
            if (!userVo) throw new Error(`user is does not exist!`);
            const roomVo = ClientToServerEventInternal.refreshCallLandlordRoomData(userVo.roleId, roomId, isCall);
            NetManager.sendDataToClient(socket, cmdIndex, {roomVo});
            NetManager.sendDataToRoomOthersClient(socket, roomId, {roomVo}, "roomStatus");
            TimeUtil.runDelayedExecution(`${roomId}`);
        } catch(err: any) {
            LogUtil.writeServerLog(`error`, err);
            NetManager.sendDataToClient(socket, cmdIndex, {err});
            TimeUtil.removeAllDelayedExecution();
            LogUtil.closeAllLogs();
        }
    }

    static playCards(socket: any, cmdIndex: number, resp: any): void {
        LogUtil.writeServerLog(`server`, `socket id ${socket.id} perform action: playCards`);
        try {
            const roomId: number = resp.roomId;
            const cards: Array<number> = resp.cards;
            TimeUtil.stopDelayedExecution(`${roomId}`);
            const userVo = GameManagerVo.userManager.findUserUsingSocketId(socket.id);
            if (!userVo) throw new Error(`user is does not exist!`);
            const sendData = ClientToServerEventInternal.refreshPlayCardsRoomData(userVo.roleId, roomId, cards);
            NetManager.sendDataToClient(socket, cmdIndex, sendData);
            NetManager.sendDataToRoomOthersClient(socket, roomId, sendData, "roomStatus");
            const roomVo = GameManagerVo.roomManager.findRoomOfId(roomId);
            if (roomVo == null) throw new Error(`room is not exist.`);
            if (roomVo.status == ROOM_STATUS.GameOver) GameManagerVo.roomManager.setInitializationState(roomId);
            else TimeUtil.runDelayedExecution(`${roomId}`);
        } catch (err: any) {
            LogUtil.writeServerLog(`error`, err);
            NetManager.sendDataToClient(socket, cmdIndex, err);
            TimeUtil.removeAllDelayedExecution();
            LogUtil.closeAllLogs();
        }
    }

}

