
export default class SendData implements SocketData {

    message: any;
    
    method: string;
    
    cmdIndex: number

    constructor(message: any, cmdIndex: number, method: string) {
        this.message = message;
        this.cmdIndex = cmdIndex;
        this.method = method;
    }

}