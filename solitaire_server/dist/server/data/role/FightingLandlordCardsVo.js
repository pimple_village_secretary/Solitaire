"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        if (typeof b !== "function" && b !== null)
            throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __values = (this && this.__values) || function(o) {
    var s = typeof Symbol === "function" && Symbol.iterator, m = s && o[s], i = 0;
    if (m) return m.call(o);
    if (o && typeof o.length === "number") return {
        next: function () {
            if (o && i >= o.length) o = void 0;
            return { value: o && o[i++], done: !o };
        }
    };
    throw new TypeError(s ? "Object is not iterable." : "Symbol.iterator is not defined.");
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var NumericalValueUtil_1 = __importDefault(require("../../util/NumericalValueUtil"));
var GameManagerVo_1 = __importDefault(require("../GameManagerVo"));
var GameStaticDataManager_1 = __importDefault(require("../GameStaticDataManager"));
var GameScoringVo_1 = __importDefault(require("./GameScoringVo"));
var PlayCardsType_1 = require("./PlayCardsType");
var PokerPointsType_1 = require("./PokerPointsType");
var RefereeVo_1 = __importDefault(require("./RefereeVo"));
var RoomStatusType_1 = require("./RoomStatusType");
var FightingLandlordCardsVo = /** @class */ (function (_super) {
    __extends(FightingLandlordCardsVo, _super);
    function FightingLandlordCardsVo() {
        var _this = _super.call(this) || this;
        _this.lastPlayedCardsRoleId = null; // 最近一次打出的牌组的人
        _this.cardsPlayedByUsers = {}; // 记录用户打出的牌组
        _this.callLandlordByUsers = {}; // 记录用户是否叫地主
        _this.landlordCandidateQueue = []; // 判断到谁地主用户队列
        return _this;
    }
    FightingLandlordCardsVo.prototype.init = function (users, initBet) {
        var _this = this;
        if (initBet === void 0) { initBet = 1; }
        var bookmakerCardsNum = 3;
        var handCardsNum = 16;
        _super.prototype.init.call(this, users, initBet, bookmakerCardsNum, handCardsNum);
        this.turnId = this.randomFirstCandidate();
        users.forEach(function (element) {
            if (!element)
                return;
            _this.cardsPlayedByUsers[element.roleId] = null;
            _this.callLandlordByUsers[element.roleId] = null;
        });
    };
    FightingLandlordCardsVo.prototype.sortingAlgorithm = function (a, b) {
        if (a === b && !a)
            return 0;
        if (a == null)
            return 1;
        if (b == null)
            return -1;
        var jsonInfo1 = GameStaticDataManager_1.default.pokerJsonInfoMap.get(a);
        var jsonInfo2 = GameStaticDataManager_1.default.pokerJsonInfoMap.get(b);
        if (jsonInfo1.pokerPoint == jsonInfo2.pokerPoint)
            return a - b;
        return jsonInfo1.landlordPointWeights - jsonInfo2.landlordPointWeights;
    };
    FightingLandlordCardsVo.prototype.deal = function () { _super.prototype.deal.call(this, this.sortingAlgorithm); };
    ;
    FightingLandlordCardsVo.prototype.play = function (roleId, cards) {
        _super.prototype.play.call(this, roleId, cards);
        this.cardsPlayedByUsers[roleId] = cards;
        if (cards.length == 0)
            return;
        this.lastPlayedCardsRoleId = roleId;
    };
    FightingLandlordCardsVo.prototype.setBookmakerUserId = function (roleId) {
        var _this = this;
        if (roleId == null)
            return;
        _super.prototype.setBookmakerUserId.call(this, roleId);
        this.bookmakerCards.forEach(function (element) {
            _this.handCards[roleId].push(element);
        });
        this.handCards[roleId].sort(this.sortingAlgorithm);
    };
    /**
     * 初始化记牌器
     */
    FightingLandlordCardsVo.prototype.initCardsCounter = function () {
        var cardsCounter = new Map();
        GameStaticDataManager_1.default.pokerJsonInfoMap.forEach(function (value, key) {
            if (value.pokerPoint == PokerPointsType_1.POKER_POINTS.none ||
                value.pokerPoint == PokerPointsType_1.POKER_POINTS.point_a ||
                value.pokerPoint == PokerPointsType_1.POKER_POINTS.point_2)
                return;
            cardsCounter.set(value.pokerPoint, 0);
        });
        cardsCounter.set(PokerPointsType_1.POKER_POINTS.point_a, 0);
        cardsCounter.set(PokerPointsType_1.POKER_POINTS.point_2, 0);
        cardsCounter.set(PokerPointsType_1.POKER_POINTS.none, 0);
        return cardsCounter;
    };
    /**
     * 设置记牌器
     */
    FightingLandlordCardsVo.prototype.setCardsCounter = function (cardsCounter, cards) {
        var jsonInfos = GameStaticDataManager_1.default.getPokerJsonInfoMap(cards);
        jsonInfos.forEach(function (element) {
            var value = cardsCounter.get(element.pokerPoint);
            cardsCounter.set(element.pokerPoint, value + 1);
        });
    };
    FightingLandlordCardsVo.prototype.isComplyRules = function (roleId, cards) {
        var curPlayType = this.determinePlayCardsType(cards);
        if (this.isMustPlayCards(roleId)) {
            if (curPlayType == PlayCardsType_1.PLAY_CARDS_TYPE.none)
                return false;
            return true;
        }
        if (curPlayType != PlayCardsType_1.PLAY_CARDS_TYPE.none) {
            var lastPlayedCards = this.cardsPlayedByUsers[this.lastPlayedCardsRoleId];
            if (lastPlayedCards == null)
                return true;
            return this.compareCards(cards, lastPlayedCards);
        }
        return true;
    };
    ;
    FightingLandlordCardsVo.prototype.isGameOver = function () {
        for (var index = 0; index < this.slots.length; index++) {
            var roleId = this.slots[index];
            if (this.findMinCards(roleId) == null)
                return roleId;
        }
        return null;
    };
    FightingLandlordCardsVo.prototype.increaseMagnificationOfCards = function (cards) {
        var _this = this;
        var curPlayType = this.determinePlayCardsType(cards);
        var multiple = 1;
        if (curPlayType == PlayCardsType_1.PLAY_CARDS_TYPE.rocket)
            multiple = multiple * 3;
        if (curPlayType == PlayCardsType_1.PLAY_CARDS_TYPE.bomb)
            multiple = multiple * 2;
        Object.keys(this.magnification).forEach(function (key) {
            var value = _this.magnification[Number(key)];
            _this.magnification[Number(key)] = value * multiple;
        });
    };
    ;
    FightingLandlordCardsVo.prototype.compareCards = function (cards1, cards2) {
        var curPlayType = this.determinePlayCardsType(cards1);
        var lastPlayedType = this.determinePlayCardsType(cards2);
        if (curPlayType == PlayCardsType_1.PLAY_CARDS_TYPE.rocket)
            return true;
        if (lastPlayedType == PlayCardsType_1.PLAY_CARDS_TYPE.rocket)
            return false;
        if (curPlayType == PlayCardsType_1.PLAY_CARDS_TYPE.bomb) {
            if (lastPlayedType != PlayCardsType_1.PLAY_CARDS_TYPE.bomb)
                return true;
            if (lastPlayedType == PlayCardsType_1.PLAY_CARDS_TYPE.bomb) {
                if (this.comparePoint(cards1[0], cards2[0]) > 0)
                    return true;
                return false;
            }
        }
        if (curPlayType != lastPlayedType)
            return false;
        if (cards2.length != cards1.length)
            return false;
        if (curPlayType == PlayCardsType_1.PLAY_CARDS_TYPE.single ||
            curPlayType == PlayCardsType_1.PLAY_CARDS_TYPE.pair ||
            curPlayType == PlayCardsType_1.PLAY_CARDS_TYPE.threeOfKind) {
            if (this.comparePoint(cards1[0], cards2[0]) > 0)
                return true;
            return false;
        }
        var repeatingNum = 0;
        if (curPlayType == PlayCardsType_1.PLAY_CARDS_TYPE.singleStraight)
            repeatingNum = 1;
        if (curPlayType == PlayCardsType_1.PLAY_CARDS_TYPE.doubleStraight)
            repeatingNum = 2;
        if (curPlayType == PlayCardsType_1.PLAY_CARDS_TYPE.threeWithSingle ||
            curPlayType == PlayCardsType_1.PLAY_CARDS_TYPE.threeWithPair ||
            curPlayType == PlayCardsType_1.PLAY_CARDS_TYPE.triStraight ||
            curPlayType == PlayCardsType_1.PLAY_CARDS_TYPE.planeWithWings) {
            repeatingNum = 3;
        }
        if (curPlayType == PlayCardsType_1.PLAY_CARDS_TYPE.fourWitTwo)
            repeatingNum = 4;
        var curMinCard = this.getMinCardsPointConsecutive(cards1, repeatingNum);
        var lastMinCard = this.getMinCardsPointConsecutive(cards2, repeatingNum);
        if (this.comparePoint(curMinCard, lastMinCard) > 0)
            return true;
        return false;
    };
    /**
     * 随机返回第一个地主候选人的ID
     */
    FightingLandlordCardsVo.prototype.randomFirstCandidate = function () {
        var users = Object.keys(this.handCards);
        var num = NumericalValueUtil_1.default.getRandomInt(0, users.length - 1);
        var roleId = users[num];
        return Number(roleId);
    };
    /**
     * 是否叫地主, 抢地主
     * @param roleId 用户Id
     * @param isCall 是否叫地主
     */
    FightingLandlordCardsVo.prototype.callLandlord = function (roleId, isCall) {
        if (roleId != this.turnId)
            return;
        this.callLandlordByUsers[roleId] = isCall;
        if (!isCall)
            this.landlordCandidateQueue.push(0);
        else
            this.landlordCandidateQueue.push(roleId);
    };
    /**
     * 确定庄家
     *
     * 按照用户的人数来确定一轮， 一轮结束将进行一次遍历，确定当前叫地主和抢地主的人数。
     * 如果第一轮结束后有超过一个人叫或抢地主，则不管， 如果只有一人叫，则确定那人为地主
     * 第二轮开始，在第一个抢地主还是不抢地主，将直接确定庄家
     */
    FightingLandlordCardsVo.prototype.dealerBookmaker = function () {
        var result = null;
        if (this.landlordCandidateQueue.length < this.slots.length)
            return result;
        var curLandlordCandidateNum = 0;
        for (var i = this.landlordCandidateQueue.length - 1; i >= 0; i--) {
            if (this.landlordCandidateQueue[i] != 0)
                curLandlordCandidateNum += 1;
        }
        if (curLandlordCandidateNum == 0) {
            result = this.randomFirstCandidate();
            return result;
        }
        if (this.landlordCandidateQueue.length == 3 && curLandlordCandidateNum > 1)
            return result;
        for (var i = this.landlordCandidateQueue.length - 1; i >= 0; i--) {
            if (this.landlordCandidateQueue[i] != 0) {
                result = this.landlordCandidateQueue[i];
                break;
            }
        }
        return result;
    };
    FightingLandlordCardsVo.prototype.refreshWhoTurned = function (status) {
        var roleId = null;
        if (status === RoomStatusType_1.ROOM_STATUS.inGame)
            roleId = this.refreshWhoPlay();
        else if (status === RoomStatusType_1.ROOM_STATUS.setBookmaker)
            roleId = this.refreshWhoCallLandlord();
        this.turnId = roleId;
    };
    /**
     * 轮到谁出牌
     */
    FightingLandlordCardsVo.prototype.refreshWhoPlay = function () {
        if (this.bookmakerUserId === null)
            return null;
        var index = this.slots.indexOf(this.bookmakerUserId);
        var intervalValue = this.numberOfRounds;
        return NumericalValueUtil_1.default.findValueOfArray(this.slots, index, intervalValue);
    };
    /**
     * 轮到谁叫地主
     */
    FightingLandlordCardsVo.prototype.refreshWhoCallLandlord = function () {
        if (this.turnId === null)
            return null;
        var index = this.slots.indexOf(this.turnId);
        var intervalValue = 1;
        return NumericalValueUtil_1.default.findValueOfArray(this.slots, index, intervalValue);
    };
    FightingLandlordCardsVo.prototype.calculateReward = function (winner) {
        if (winner === this.bookmakerUserId)
            return this.calculateBookmakerWin();
        return this.calculateIdlerWin();
    };
    /**
     * 计算用户为庄家时赢了
     */
    FightingLandlordCardsVo.prototype.calculateBookmakerWin = function () {
        var _this = this;
        var scoringVo = new GameScoringVo_1.default();
        var bookmakerWinMony = 0;
        Object.keys(this.magnification).forEach(function (key) {
            if (Number(key) == _this.bookmakerUserId)
                return;
            var num = _this.calculateIdlerLose(Number(key));
            scoringVo.scoring[Number(key)] = num * -1;
            bookmakerWinMony += num;
        });
        scoringVo.scoring[this.bookmakerUserId] = bookmakerWinMony;
        return scoringVo;
    };
    /**
     * 计算用户为闲家时赢了
     */
    FightingLandlordCardsVo.prototype.calculateIdlerWin = function () {
        var _this = this;
        var scoringVo = new GameScoringVo_1.default();
        var bookmakerLoseMony = this.calculateBookmakerLose(this.bookmakerUserId);
        scoringVo.scoring[this.bookmakerUserId] = bookmakerLoseMony * -1;
        Object.keys(this.magnification).forEach(function (key) {
            if (Number(key) == _this.bookmakerUserId)
                return;
            scoringVo.scoring[Number(key)] = bookmakerLoseMony / (_this.slots.length - 1);
        });
        return scoringVo;
    };
    /**
     * 计算用户为庄家时输了
     */
    FightingLandlordCardsVo.prototype.calculateBookmakerLose = function (roleId) {
        var _this = this;
        var money = 0;
        Object.keys(this.magnification).forEach(function (key) {
            if (Number(key) == roleId)
                return;
            var magnification = _this.magnification[Number(key)];
            money += _this.initBet * magnification;
        });
        if (GameManagerVo_1.default.userManager.isEnoughMoney(roleId, money))
            return money;
        else
            return GameManagerVo_1.default.userManager.getUserMony(roleId);
    };
    /**
     * 计算用户为闲家时输了
     */
    FightingLandlordCardsVo.prototype.calculateIdlerLose = function (roleId) {
        var magnification = this.magnification[roleId];
        var money = magnification * this.initBet;
        if (GameManagerVo_1.default.userManager.isEnoughMoney(roleId, money))
            return money;
        else
            return GameManagerVo_1.default.userManager.getUserMony(roleId);
    };
    /**
     * 有多少用户没有出牌(当前一圈儿)
     */
    FightingLandlordCardsVo.prototype.numberOfUsersNotPlayCards = function () {
        var e_1, _a;
        var keys = Object.keys(this.cardsPlayedByUsers);
        var result = 0;
        try {
            for (var keys_1 = __values(keys), keys_1_1 = keys_1.next(); !keys_1_1.done; keys_1_1 = keys_1.next()) {
                var key = keys_1_1.value;
                var cards = this.cardsPlayedByUsers[Number(key)];
                if (cards == null || cards.length == 0)
                    result += 1;
            }
        }
        catch (e_1_1) { e_1 = { error: e_1_1 }; }
        finally {
            try {
                if (keys_1_1 && !keys_1_1.done && (_a = keys_1.return)) _a.call(keys_1);
            }
            finally { if (e_1) throw e_1.error; }
        }
        return result;
    };
    /**
     * 找到用户当前手里最小的那张牌
     * @param roleId 用户Id
     */
    FightingLandlordCardsVo.prototype.findMinCards = function (roleId) {
        var e_2, _a;
        try {
            for (var _b = __values(this.handCards[roleId]), _c = _b.next(); !_c.done; _c = _b.next()) {
                var cardId = _c.value;
                if (cardId != null)
                    return cardId;
            }
        }
        catch (e_2_1) { e_2 = { error: e_2_1 }; }
        finally {
            try {
                if (_c && !_c.done && (_a = _b.return)) _a.call(_b);
            }
            finally { if (e_2) throw e_2.error; }
        }
        return null;
    };
    /**
     * 是否轮到自己必须出牌
     * @param roleId 用户id
     * @returns
     */
    FightingLandlordCardsVo.prototype.isMustPlayCards = function (roleId) {
        var num = this.numberOfUsersNotPlayCards();
        var haveThreeUserNotPlayCards = (num == 3);
        var IsMePlayedLastCard = (this.turnId == this.lastPlayedCardsRoleId);
        var isMyTurn = (this.turnId == roleId);
        if (haveThreeUserNotPlayCards || (num == 2 && IsMePlayedLastCard && isMyTurn))
            return true;
        return false;
    };
    /**
     * 比较点数大小
     * @param cardId1
     * @param cardId2
     * @returns
     */
    FightingLandlordCardsVo.prototype.comparePoint = function (cardId1, cardId2) {
        var jsonInfo1 = GameStaticDataManager_1.default.pokerJsonInfoMap.get(cardId1);
        var jsonInfo2 = GameStaticDataManager_1.default.pokerJsonInfoMap.get(cardId2);
        if (jsonInfo1.landlordPointWeights == jsonInfo2.landlordPointWeights)
            return 0;
        if (jsonInfo1.landlordPointWeights > jsonInfo2.landlordPointWeights)
            return 1;
        return -1;
    };
    /**
     * 确定牌组类型
     * @param cards 卡牌组
     * @returns 确定牌组类型
     */
    FightingLandlordCardsVo.prototype.determinePlayCardsType = function (cards) {
        var cardsCounter = this.initCardsCounter();
        this.setCardsCounter(cardsCounter, cards);
        var notRepeatingCards = this.getPointOfNotRepeating(cards);
        if (cards.length == 0)
            return PlayCardsType_1.PLAY_CARDS_TYPE.none;
        if (cards.length == 1)
            return PlayCardsType_1.PLAY_CARDS_TYPE.single;
        if (cards.length == 2 && notRepeatingCards.length == 1) {
            var jsonInfo = GameStaticDataManager_1.default.pokerJsonInfoMap.get(cards[0]);
            if (jsonInfo.pokerPoint == PokerPointsType_1.POKER_POINTS.none)
                return PlayCardsType_1.PLAY_CARDS_TYPE.rocket;
            return PlayCardsType_1.PLAY_CARDS_TYPE.pair;
        }
        if (cards.length == 3 && notRepeatingCards.length == 1) {
            return PlayCardsType_1.PLAY_CARDS_TYPE.threeOfKind;
        }
        if (cards.length == 4) {
            if (notRepeatingCards.length == 1)
                return PlayCardsType_1.PLAY_CARDS_TYPE.bomb;
            if (notRepeatingCards.length == 2) {
                if (cardsCounter.get(notRepeatingCards[0]) == 3)
                    return PlayCardsType_1.PLAY_CARDS_TYPE.threeWithSingle;
                if (cardsCounter.get(notRepeatingCards[1]) == 3)
                    return PlayCardsType_1.PLAY_CARDS_TYPE.threeWithSingle;
            }
        }
        if (cards.length == 5 && notRepeatingCards.length == 2) {
            if (cardsCounter.get(notRepeatingCards[0]) == 3)
                return PlayCardsType_1.PLAY_CARDS_TYPE.threeWithPair;
            if (cardsCounter.get(notRepeatingCards[1]) == 3)
                return PlayCardsType_1.PLAY_CARDS_TYPE.threeWithPair;
        }
        if (cards.length >= 5) {
            var consecutiveLength = this.getCardsPointConsecutiveLengthOfNum(cardsCounter, 1);
            if (consecutiveLength >= 5 && cards.length % consecutiveLength == 0) {
                if (cardsCounter.get(PokerPointsType_1.POKER_POINTS.point_2))
                    return PlayCardsType_1.PLAY_CARDS_TYPE.none;
                return PlayCardsType_1.PLAY_CARDS_TYPE.singleStraight;
            }
            consecutiveLength = this.getCardsPointConsecutiveLengthOfNum(cardsCounter, 2);
            if (consecutiveLength >= 3 && cards.length % consecutiveLength == 0) {
                if (cardsCounter.get(PokerPointsType_1.POKER_POINTS.point_2))
                    return PlayCardsType_1.PLAY_CARDS_TYPE.none;
                return PlayCardsType_1.PLAY_CARDS_TYPE.doubleStraight;
            }
            consecutiveLength = this.getCardsPointConsecutiveLengthOfNum(cardsCounter, 3);
            if (consecutiveLength >= 2) {
                if (cards.length % consecutiveLength == 0) {
                    if (cardsCounter.get(PokerPointsType_1.POKER_POINTS.point_2))
                        return PlayCardsType_1.PLAY_CARDS_TYPE.none;
                    return PlayCardsType_1.PLAY_CARDS_TYPE.triStraight;
                }
                // isSameQuantity 判断 牌组A带B这种规则时， 用来判断是否满足A的不一样的点数数量要等于B的不一样的点数数量
                var isSameQuantity = notRepeatingCards.length == (consecutiveLength * 2);
                // isNumberLessThanTwo 判断 牌组A带B这种规则时， 用来限制B的相同点数的数量等于1，或等于2
                var remainingNum = (cards.length - consecutiveLength * 3) / consecutiveLength;
                var isNumberLessThanTwo = remainingNum == 2 || remainingNum == 1;
                if (isSameQuantity && isNumberLessThanTwo)
                    return PlayCardsType_1.PLAY_CARDS_TYPE.planeWithWings;
            }
            consecutiveLength = this.getCardsPointConsecutiveLengthOfNum(cardsCounter, 4);
            if (consecutiveLength >= 1) {
                var isSameQuantity = notRepeatingCards.length == (consecutiveLength * 2);
                var remainingNum = (cards.length - consecutiveLength * 3) / consecutiveLength;
                var isNumberLessThanTwo = remainingNum == 2 || remainingNum == 1;
                if (isSameQuantity && isNumberLessThanTwo)
                    return PlayCardsType_1.PLAY_CARDS_TYPE.planeWithWings;
            }
        }
        return PlayCardsType_1.PLAY_CARDS_TYPE.none;
    };
    /**
     * 获取点数不重复的卡牌组
     * @param cards 卡牌组
     * @returns 点数不重复的卡牌组
     */
    FightingLandlordCardsVo.prototype.getPointOfNotRepeating = function (cards) {
        var e_3, _a;
        var result = [];
        var tempBuffer = new Set();
        try {
            for (var cards_1 = __values(cards), cards_1_1 = cards_1.next(); !cards_1_1.done; cards_1_1 = cards_1.next()) {
                var card = cards_1_1.value;
                var point = GameStaticDataManager_1.default.pokerJsonInfoMap.get(card).pokerPoint;
                if (tempBuffer.has(point))
                    continue;
                tempBuffer.add(point);
                result.push(point);
            }
        }
        catch (e_3_1) { e_3 = { error: e_3_1 }; }
        finally {
            try {
                if (cards_1_1 && !cards_1_1.done && (_a = cards_1.return)) _a.call(cards_1);
            }
            finally { if (e_3) throw e_3.error; }
        }
        return result;
    };
    /**
     * 获取顺子的长度
     * @param cardsCounter 卡牌计数器
     * @param num 顺子中对应的每个点数应有num张
     * @returns
     */
    FightingLandlordCardsVo.prototype.getCardsPointConsecutiveLengthOfNum = function (cardsCounter, num) {
        var length = 0;
        var isBegin = false;
        var isEnd = false;
        cardsCounter.forEach(function (value, key) {
            if (value == num)
                isBegin = true;
            if (isBegin && value == 0)
                isEnd = true;
            if (isBegin && !isEnd)
                if (value == num)
                    length += 1;
        });
        return length;
    };
    /**
     * 获取顺子的最小的点数
     * @param cards 卡牌组
     * @param num 顺子中对应的每个点数应有num张
     * @returns
     */
    FightingLandlordCardsVo.prototype.getMinCardsPointConsecutive = function (cards, num) {
        var e_4, _a;
        var minCard = null;
        var jsonInfos = [];
        cards.forEach(function (element) {
            var jsonInfo = GameStaticDataManager_1.default.pokerJsonInfoMap.get(element);
            if (!jsonInfo)
                return;
            jsonInfos.push(jsonInfo);
        });
        jsonInfos.sort(function (a, b) { return a.landlordPointWeights - b.landlordPointWeights; });
        var cardCounter = new Map();
        try {
            for (var jsonInfos_1 = __values(jsonInfos), jsonInfos_1_1 = jsonInfos_1.next(); !jsonInfos_1_1.done; jsonInfos_1_1 = jsonInfos_1.next()) {
                var jsonInfo = jsonInfos_1_1.value;
                var value = 0;
                if (cardCounter.has(jsonInfo.pokerPoint))
                    value = cardCounter.get(jsonInfo.pokerPoint);
                if (value + 1 >= num) {
                    minCard = jsonInfo.id;
                    break;
                }
                ;
                cardCounter.set(jsonInfo.pokerPoint, value + 1);
            }
        }
        catch (e_4_1) { e_4 = { error: e_4_1 }; }
        finally {
            try {
                if (jsonInfos_1_1 && !jsonInfos_1_1.done && (_a = jsonInfos_1.return)) _a.call(jsonInfos_1);
            }
            finally { if (e_4) throw e_4.error; }
        }
        return minCard;
    };
    return FightingLandlordCardsVo;
}(RefereeVo_1.default));
exports.default = FightingLandlordCardsVo;
