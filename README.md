# Solitaire

#### 介绍
纸牌游戏-学习Cocos项目

#### 预览效果
![主界面](https://images.gitee.com/uploads/images/2022/0223/140902_a7d39828_5488194.png "0.png")
![等待状态](https://images.gitee.com/uploads/images/2022/0223/140942_0fb16eaf_5488194.png "2.png")
![准备状态_1](https://images.gitee.com/uploads/images/2022/0223/140950_9cd03486_5488194.png "3.png")
![准备状态_2](https://images.gitee.com/uploads/images/2022/0223/140958_074e0636_5488194.png "4.png")
![叫地主_1](https://images.gitee.com/uploads/images/2022/0223/141008_1a8dbb1c_5488194.png "5.png")
![叫地主_2](https://images.gitee.com/uploads/images/2022/0223/141018_25fc620b_5488194.png "6.png")
![开始出牌](https://images.gitee.com/uploads/images/2022/0223/141027_989510c5_5488194.png "7.png")
![结算界面_胜利](https://images.gitee.com/uploads/images/2022/0223/141047_eabfe04b_5488194.png "8.png")
![结算界面_失败](https://images.gitee.com/uploads/images/2022/0223/141105_8b3fa3e2_5488194.png "9.png")