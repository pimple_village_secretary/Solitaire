"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var GameManagerVo_1 = __importDefault(require("../data/GameManagerVo"));
var NetManager_1 = __importDefault(require("../net/NetManager"));
var FightingLandlordCardsVo_1 = __importDefault(require("../data/role/FightingLandlordCardsVo"));
var RoomStatusType_1 = require("../data/role/RoomStatusType");
var TimeUtil_1 = __importDefault(require("../util/TimeUtil"));
var ClientToServerEventInternal_1 = __importDefault(require("./ClientToServerEventInternal"));
var LogUtil_1 = __importDefault(require("../util/LogUtil"));
var ClientToServerEventHandler = /** @class */ (function () {
    function ClientToServerEventHandler() {
    }
    ///////////////////////////////////////////////////////////////////////
    // 创建格式
    // 
    // static 函数名称(socket: any, cmdIndex: number, 客户端发送的数据) {
    //      ...   
    // }
    //
    // 注意: socket 和 cmdIndex 参数必备, 即使不需要用到
    ////////////////////////////////////////////////////////////////////////
    ClientToServerEventHandler.login = function (socket, cmdIndex, userVo) {
        LogUtil_1.default.writeServerLog("server", "user logged in. socket id: ".concat(socket.id));
        if (!userVo)
            return;
        GameManagerVo_1.default.userManager.addToUser(socket, userVo);
    };
    ClientToServerEventHandler.disconnect = function (socket, cmdIndex) {
        LogUtil_1.default.writeServerLog("server", "user disconnected. socket id: ".concat(socket.id));
        GameManagerVo_1.default.userManager.removeUser(socket.id);
    };
    ClientToServerEventHandler.findFreeRoomHandler = function (socket, cmdIndex, roomId) {
        return __awaiter(this, void 0, void 0, function () {
            var roomVo, userVo;
            return __generator(this, function (_a) {
                LogUtil_1.default.writeServerLog("server", "socket id ".concat(socket.id, " perform action: findGameRoom"));
                try {
                    roomVo = GameManagerVo_1.default.roomManager.findFreeRoom(roomId);
                    if (!roomVo) {
                        roomVo = GameManagerVo_1.default.roomManager.createRoom();
                    }
                    userVo = GameManagerVo_1.default.userManager.findUserUsingSocketId(socket.id);
                    if (!userVo)
                        throw new Error("user is does not exist!");
                    roomVo = GameManagerVo_1.default.roomManager.addUserToRoom(roomVo.roomId, userVo);
                    if (!roomVo)
                        throw new Error("addUserToRoom error: roomVo null");
                    LogUtil_1.default.writeServerLog("room ".concat(roomVo.roomId), "".concat(userVo.roleId, " join room"));
                    socket.join(roomVo.roomId);
                    NetManager_1.default.sendDataToClient(socket, cmdIndex, { roomVo: roomVo });
                    NetManager_1.default.sendDataToRoomOthersClient(socket, roomVo.roomId, { roomVo: roomVo }, "roomStatus");
                }
                catch (err) {
                    LogUtil_1.default.writeServerLog("error", err);
                    NetManager_1.default.sendDataToClient(socket, cmdIndex, { err: err });
                    LogUtil_1.default.closeAllLogs();
                }
                return [2 /*return*/];
            });
        });
    };
    ClientToServerEventHandler.quitGameRoomHandler = function (socket, cmdIndex, roomId) {
        LogUtil_1.default.writeServerLog("server", "socket id ".concat(socket.id, " perform action: quitGameRoom"));
        try {
            var userVo = GameManagerVo_1.default.userManager.findUserUsingSocketId(socket.id);
            if (!userVo)
                throw new Error("user is does not exist!");
            var roomVo = GameManagerVo_1.default.roomManager.removeUserToRoom(roomId, userVo.roleId);
            if (!roomVo)
                throw new Error("removeUserToRoom error: roomVo null");
            LogUtil_1.default.writeServerLog("room ".concat(roomVo.roomId), "".concat(userVo.roleId, " quit room"));
            socket.leave(roomId);
            NetManager_1.default.sendDataToRoomOthersClient(socket, roomVo.roomId, { roomVo: roomVo }, "roomStatus");
        }
        catch (err) {
            LogUtil_1.default.writeServerLog("error", err);
            NetManager_1.default.sendDataToClient(socket, cmdIndex, { err: err });
            LogUtil_1.default.closeAllLogs();
        }
    };
    ClientToServerEventHandler.switchGameRoomHandler = function (socket, cmdIndex, roomId) {
        LogUtil_1.default.writeServerLog("server", "socket id ".concat(socket.id, " perform action: switchGameRoomHandler"));
        ClientToServerEventHandler.quitGameRoomHandler(socket, cmdIndex, roomId);
        ClientToServerEventHandler.findFreeRoomHandler(socket, cmdIndex, roomId);
    };
    ClientToServerEventHandler.prepareGameHandler = function (socket, cmdIndex, roomId) {
        LogUtil_1.default.writeServerLog("server", "socket id ".concat(socket.id, " perform action: prepareGame"));
        try {
            var roomVo = GameManagerVo_1.default.roomManager.findRoomOfId(roomId);
            if (!roomVo)
                throw new Error("room is not exist.");
            var userVo = GameManagerVo_1.default.userManager.findUserUsingSocketId(socket.id);
            if (!userVo)
                throw new Error("user is does not exist!");
            roomVo.addUserIdToPrepareQueue(userVo.roleId);
            LogUtil_1.default.writeServerLog("room ".concat(roomVo.roomId), "have a user prepare, id: ".concat(userVo.roleId));
            if (roomVo.isAllPrepare()) {
                roomVo.changeRoomStatus(RoomStatusType_1.ROOM_STATUS.setBookmaker);
                var referee_1 = roomVo.initReferee(FightingLandlordCardsVo_1.default);
                if (!referee_1)
                    return;
                referee_1.deal();
                referee_1.setCountdownStartTime(Date.now());
                // TODO: 测试
                referee_1.setCountdownTime(10);
                // 添加计时器
                var waitTime = (referee_1.getCountdownTime() + 0.5) * 1000;
                TimeUtil_1.default.delayedExecution("".concat(roomId), function () {
                    ClientToServerEventInternal_1.default.updateCalledLandlordStatusToFalse(referee_1, roomId);
                }, waitTime);
                LogUtil_1.default.writeServerLog("room ".concat(roomVo.roomId), "all prepare, and room add referee");
                LogUtil_1.default.writeServerLog("room ".concat(roomVo.roomId), "first turn user is ".concat(referee_1.getTurnId()));
            }
            NetManager_1.default.sendDataToClient(socket, cmdIndex, { roomVo: roomVo });
            NetManager_1.default.sendDataToRoomOthersClient(socket, roomId, { roomVo: roomVo }, "roomStatus");
        }
        catch (err) {
            LogUtil_1.default.writeServerLog("error", err);
            NetManager_1.default.sendDataToClient(socket, cmdIndex, err);
            TimeUtil_1.default.removeAllDelayedExecution();
            LogUtil_1.default.closeAllLogs();
        }
    };
    ClientToServerEventHandler.cancelPrepareGameHandler = function (socket, cmdIndex, roomId) {
        LogUtil_1.default.writeServerLog("server", "socket id ".concat(socket.id, " perform action: cancelPrepareGame"));
        try {
            var roomVo = GameManagerVo_1.default.roomManager.findRoomOfId(roomId);
            if (!roomVo)
                throw new Error("room is not exist.");
            var userVo = GameManagerVo_1.default.userManager.findUserUsingSocketId(socket.id);
            if (!userVo)
                throw new Error("user is does not exist!");
            roomVo.removeUserIdToPrepareQueue(userVo.roleId);
            LogUtil_1.default.writeServerLog("room ".concat(roomVo.roomId), "have a user no prepare, id: ".concat(userVo.roleId));
            NetManager_1.default.sendDataToClient(socket, cmdIndex, { roomVo: roomVo });
            NetManager_1.default.sendDataToRoomOthersClient(socket, roomId, { roomVo: roomVo }, "roomStatus");
        }
        catch (err) {
            LogUtil_1.default.writeServerLog("error", err);
            NetManager_1.default.sendDataToClient(socket, cmdIndex, { err: err });
            LogUtil_1.default.closeAllLogs();
        }
    };
    ClientToServerEventHandler.callLandlord = function (socket, cmdIndex, resp) {
        LogUtil_1.default.writeServerLog("server", "socket id ".concat(socket.id, " perform action: callLandlord"));
        try {
            var roomId = resp.roomId;
            var isCall = resp.isCall;
            TimeUtil_1.default.stopDelayedExecution("".concat(roomId));
            var userVo = GameManagerVo_1.default.userManager.findUserUsingSocketId(socket.id);
            if (!userVo)
                throw new Error("user is does not exist!");
            var roomVo = ClientToServerEventInternal_1.default.refreshCallLandlordRoomData(userVo.roleId, roomId, isCall);
            NetManager_1.default.sendDataToClient(socket, cmdIndex, { roomVo: roomVo });
            NetManager_1.default.sendDataToRoomOthersClient(socket, roomId, { roomVo: roomVo }, "roomStatus");
            TimeUtil_1.default.runDelayedExecution("".concat(roomId));
        }
        catch (err) {
            LogUtil_1.default.writeServerLog("error", err);
            NetManager_1.default.sendDataToClient(socket, cmdIndex, { err: err });
            TimeUtil_1.default.removeAllDelayedExecution();
            LogUtil_1.default.closeAllLogs();
        }
    };
    ClientToServerEventHandler.playCards = function (socket, cmdIndex, resp) {
        LogUtil_1.default.writeServerLog("server", "socket id ".concat(socket.id, " perform action: playCards"));
        try {
            var roomId = resp.roomId;
            var cards = resp.cards;
            TimeUtil_1.default.stopDelayedExecution("".concat(roomId));
            var userVo = GameManagerVo_1.default.userManager.findUserUsingSocketId(socket.id);
            if (!userVo)
                throw new Error("user is does not exist!");
            var sendData = ClientToServerEventInternal_1.default.refreshPlayCardsRoomData(userVo.roleId, roomId, cards);
            NetManager_1.default.sendDataToClient(socket, cmdIndex, sendData);
            NetManager_1.default.sendDataToRoomOthersClient(socket, roomId, sendData, "roomStatus");
            var roomVo = GameManagerVo_1.default.roomManager.findRoomOfId(roomId);
            if (roomVo == null)
                throw new Error("room is not exist.");
            if (roomVo.status == RoomStatusType_1.ROOM_STATUS.GameOver)
                GameManagerVo_1.default.roomManager.setInitializationState(roomId);
            else
                TimeUtil_1.default.runDelayedExecution("".concat(roomId));
        }
        catch (err) {
            LogUtil_1.default.writeServerLog("error", err);
            NetManager_1.default.sendDataToClient(socket, cmdIndex, err);
            TimeUtil_1.default.removeAllDelayedExecution();
            LogUtil_1.default.closeAllLogs();
        }
    };
    return ClientToServerEventHandler;
}());
exports.default = ClientToServerEventHandler;
