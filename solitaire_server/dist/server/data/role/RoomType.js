"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ROOM_TYPE = void 0;
var ROOM_TYPE;
(function (ROOM_TYPE) {
    ROOM_TYPE["FightingLandlord"] = "FightingLandlord";
    ROOM_TYPE["TexasHoldEmPoker"] = "TexasHoldEmPoker";
})(ROOM_TYPE = exports.ROOM_TYPE || (exports.ROOM_TYPE = {}));
