import GameManagerVo from "../data/GameManagerVo";
import GameStaticDataManager from "../data/GameStaticDataManager";
import FightingLandlordCardsVo from "../data/role/FightingLandlordCardsVo";
import { ROOM_STATUS } from "../data/role/RoomStatusType";
import RoomVo from "../data/role/RoomVo";
import NetManager from "../net/NetManager";
import LogUtil from "../util/LogUtil";
import TimeUtil from "../util/TimeUtil";

export default class ClientToServerEventInternal {
    
    static refreshCallLandlordRoomData(roleId: number, roomId: number, isCall: boolean): RoomVo {
        const roomVo = GameManagerVo.roomManager.findRoomOfId(roomId);
        if (!roomVo || roomVo.status != ROOM_STATUS.setBookmaker) throw new Error(`room is not exist or status is error`);
        let referee: FightingLandlordCardsVo | null = roomVo.getReferee();
        if (!referee) throw new Error(`referee is not defined.`);
        referee.callLandlord(roleId, isCall);
        LogUtil.writeServerLog(`room ${roomVo.roomId}`, `have a user call landlord[${isCall}], id: ${roleId}`);
        const bookmakerId = referee.dealerBookmaker();
        if (bookmakerId) {
            referee.setBookmakerUserId(bookmakerId);
            roomVo.changeRoomStatus(ROOM_STATUS.inGame);
            TimeUtil.changeDelayedExecution(`${roomId}`, () => { 
                this.updatePlayCardsStatusToFalse(referee!, roomId); 
            });
            TimeUtil.runDelayedExecution(`${roomId}`);
            LogUtil.writeServerLog(`room ${roomVo.roomId}`, `bookmaker is user ${bookmakerId}`);
            LogUtil.writeServerLog(`room ${roomVo.roomId}`, `room ${roomId} start join game status`);
        }
        referee.refreshWhoTurned(roomVo.status);
        referee.setCountdownStartTime(Date.now());
        LogUtil.writeServerLog(`room ${roomVo.roomId}`, `cur turn user is ${referee.getTurnId()}`);
        return roomVo;
    }

    static updateCalledLandlordStatusToFalse(referee: FightingLandlordCardsVo, roomId: number): void {
        const roleId = referee.getTurnId();
        if (roleId === null) throw new Error(`turn id is null`);
        const roomVo = ClientToServerEventInternal.refreshCallLandlordRoomData(roleId, roomId, false);
        NetManager.sendDataToRoomAllClient(roomId, {roomVo}, "roomStatus");
    }

    static refreshPlayCardsRoomData(roleId: number, roomId: number, cards: Array<number>): any {
        const roomVo = GameManagerVo.roomManager.findRoomOfId(roomId);
        if (!roomVo || roomVo.status != ROOM_STATUS.inGame) throw new Error(`room is not exist or status is error`);
        let referee: FightingLandlordCardsVo | null = roomVo.getReferee();
        if (!referee) throw new Error(`referee is not defined.`);
        LogUtil.writeServerLog(`room ${roomVo.roomId}`, `user ${roleId}, play cards: ${GameStaticDataManager.getPokerName(cards)}`);
        if (referee.isMustPlayCards(roleId) && cards.length == 0) throw new Error("is Can't not play cards");
        if (!referee.isComplyRules(roleId, cards)) throw new Error("is not comply rules");
        referee.autoIncrementNumberOfRounds();
        referee.play(roleId, cards);
        referee.increaseMagnificationOfCards(cards);
        LogUtil.writeServerLog(`room ${roomVo.roomId}`, `user ${roleId}, remaining cards: ${referee.getUserHandCards(roleId)}`);
        const winner = referee.isGameOver();
        if (winner) {
            TimeUtil.removeDelayedExecution(`${roomId}`);
            roomVo.changeRoomStatus(ROOM_STATUS.GameOver);
            const scoringVo = referee.calculateReward(winner);
            GameManagerVo.userManager.gameScoring(scoringVo);
            LogUtil.writeServerLog(`room ${roomVo.roomId}`, `winner is ${winner}`);
            LogUtil.writeServerLog(`room ${roomVo.roomId}`, `scoring: ${scoringVo.scoring}`);
            referee.refreshWhoTurned(roomVo.status);
            referee.setCountdownStartTime(Date.now());
            return { roomVo, scoringVo, winner };
        }
        referee.refreshWhoTurned(roomVo.status);
        referee.setCountdownStartTime(Date.now());
        return { roomVo };
    }

    static updatePlayCardsStatusToFalse(referee: FightingLandlordCardsVo, roomId: number): void {
        const roleId = referee.getTurnId();
        if (roleId === null) throw new Error(`roleId is null`);
        let cards: Array<number> = [];
        if (referee.isMustPlayCards(roleId)) {
            const cardId = referee.findMinCards(roleId);
            if (cardId == null) throw new Error(`hand no cards`);
            cards.push(cardId);
        }
        const sendData = ClientToServerEventInternal.refreshPlayCardsRoomData(roleId, roomId, cards);
        NetManager.sendDataToRoomAllClient(roomId, sendData, "roomStatus");
        const roomVo = GameManagerVo.roomManager.findRoomOfId(roomId);
        if (roomVo == null) throw new Error(`room is not exist.`);
        if (roomVo.status == ROOM_STATUS.GameOver) {
            GameManagerVo.roomManager.setInitializationState(roomId);
        }
    }
}