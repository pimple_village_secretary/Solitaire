export default class NumericalValueUtil {

    /**
     * 得到一个两数之间的随机整数
     * 
     * 返回了一个在指定值之间的随机整数。这个值不小于 min （如果 min 不是整数，则不小于 min 的向上取整数），且小于（等于）max。
     * @param min 
     * @param max 
     * @returns 
     */
    static getRandomInt(min: number, max: number): number {
        min = Math.ceil(min);
        max = Math.floor(max);
        const result = Math.floor(Math.random() * (max - min + 1)) + min;
        return result;
    }

}