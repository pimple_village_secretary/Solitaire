export default class UserVo {
    
    roleId!: number;

    roleName!: string;

    roleAvatar!: string;

    money!: number;

    initJsonInfo(resp: any): void {
        this.roleId = resp.roleId;
        this.roleName = resp.roleName;
        this.roleAvatar = resp.roleAvatar;
        this.money = resp.money;
    }
    
}