import GameRoleDataManager from "../../../data/GameRoleDataManager";
import FightingLandlordCardsVo from "../../../data/roledata/FightingLandlordCardsVo";
import GameScoringVo from "../../../data/roledata/GameScoringVo";
import { ROOM_STATUS } from "../../../data/roledata/RoomStatusType";
import RoomVo from "../../../data/roledata/RoomVo";
import UserVo from "../../../data/roledata/UserVo";
import NetManager from "../../../net/NetManager";
import SendData from "../../../net/SendData";
import Poker from "../../poker/Poker";
import MySelfSlot from "./MySelfSlot";
import OtherSlot from "./OtherSlot";
import SettlementScore from "./SettlementScore";

const {ccclass, property} = cc._decorator;

@ccclass
export default class FightingLandlordRoomLayer extends cc.Component {

    @property(cc.Label)
    private title: cc.Label = null;

    @property(cc.Label)
    private magnification: cc.Label = null

    @property(cc.Node)
    private bookmarkCards: cc.Node = null;

    @property(MySelfSlot)
    private myself: MySelfSlot = null;

    @property(OtherSlot)
    private otherSlots: Array<OtherSlot> = [];

    @property(cc.Node)
    private preparationStage: cc.Node = null;

    @property(cc.Button)
    private prepareBtn: cc.Button = null;

    @property(cc.Label)
    private prepareLabel: cc.Label = null;

    @property(cc.Label)
    private progress: cc.Label = null;

    @property(cc.Prefab)
    private pokerPrefab: cc.Prefab = null;

    @property(cc.Prefab)
    private settlementScorePrefab: cc.Prefab = null;

    roomVo: RoomVo = null;

    private userVo: UserVo = null;

    /**
     * 是否再结算界面
     */
    isInCheckoutInterface: boolean = false;

    init(roomVo: RoomVo, userVo: UserVo) {
        this.roomVo = roomVo;
        this.userVo = userVo;
        this.title.string = `房间号: ${this.roomVo.roomId}`;
        this.initSlots();
        this.refresh();
    }

    /**
     * 改变房间到等待状态的样式
     */
    changeRoomToWaitStatus(): void {
        this.bookmarkCards.removeAllChildren();
        this.magnification.node.active = false;
        this.myself.changeSlotToWaitStatus();
        this.otherSlots.forEach(slots => {
            slots.changeSlotToWaitStatus();
        });
    }

    refresh() {
        const status = this.roomVo.status;
        if (status === ROOM_STATUS.waiting) {
            this.preparationStage.active = true;
            this.refreshPreparationStage();
            this.refreshSlots();
        } else if (status === ROOM_STATUS.setBookmaker) {
            this.preparationStage.active = false;
            this.magnification.node.active = true;
            this.refreshMagnification();
            this.refreshBookmarkCards(false);
            this.refreshSlots();
        } else if (status === ROOM_STATUS.inGame) {
            this.preparationStage.active = false;
            this.refreshMagnification();
            this.refreshBookmarkCards(true);
            this.refreshSlots();
        } else if (status === ROOM_STATUS.GameOver) {
            this.refreshSlots();
        }
    }

    refreshMagnification() {
        const referee = this.roomVo.getReferee<FightingLandlordCardsVo>();
        const magnification = referee.magnification[this.userVo.roleId];
        this.magnification.string = `倍率: ${magnification}`;
    }

    refreshBookmarkCards(isFront: boolean) {
        const referee = this.roomVo.getReferee<FightingLandlordCardsVo>();
        const cards = referee.bookmakerCards;
        if (this.bookmarkCards.children.length == 0) {
            cards.forEach(cardId => {
                let node: cc.Node = cc.instantiate(this.pokerPrefab);
                let poker = node.getComponent(Poker);
                poker.init(cardId, isFront);
                this.bookmarkCards.addChild(node);
            });
        }
        if (this.bookmarkCards.children.length != cards.length) return;
        for (let index = 0; index < cards.length; index++) {
            const cardId = cards[index];
            const node = this.bookmarkCards.children[index];
            let poker = node.getComponent(Poker);
            poker.init(cardId, isFront);
        }
    }

    initSlots() {
        this.myself.node.off("refreshRoom");
        this.myself.node.on("refreshRoom", (resp: any) => {
            const roomVo: RoomVo = new RoomVo();
            roomVo.initJsonInfo(resp.roomVo);
            this.roomVo = roomVo;
            if (roomVo.status === ROOM_STATUS.GameOver) {
                this.isInCheckoutInterface = true;
                this.refresh();
                // 挂上结算计分预制体
                this.mountSettlementScoreNode(resp);
                this.roomVo.setInitializationState();
            } else this.refresh();
        });
        this.refreshSlots();
    }

    refreshSlots() {
        const myself = this.roomVo.users[0];
        const referee = this.roomVo.getReferee<FightingLandlordCardsVo>();
        const status = this.roomVo.status
        this.myself.init(myself, referee, this.roomVo);
        for (let index = 0; index < this.otherSlots.length; index++) {
            const vo = this.roomVo.users[index + 1];
            if (!vo) {
                this.otherSlots[index].node.active = false;
                continue;
            }
            this.otherSlots[index].node.active = true;
            this.otherSlots[index].init(vo, referee, status);
        }
    }

    refreshPreparationStage() {
        this.prepareBtn.interactable = false;
        if (this.roomVo.isRoomFull()) {
            this.prepareBtn.interactable = true;
        }
        this.progress.node.active = false;
        if (this.roomVo.prepareQueue.length > 0) {
            this.progress.node.active = true;
            this.progress.string = `${this.roomVo.prepareQueue.length}/${this.roomVo.roomCanHaveMaximumPeople}`;
        }
        if (this.roomVo.isUserPrepare()) this.prepareLabel.string = `取消准备`;
        else this.prepareLabel.string = `准备`;
    }

    /**
     * 挂载结算界面
     * @param resp 
     */
    mountSettlementScoreNode(resp: any) {
        const node = cc.instantiate(this.settlementScorePrefab);
        const settlementScore = node.getComponent(SettlementScore);
        const scoringVo = new GameScoringVo();
        scoringVo.initJsonInfo(resp.scoringVo);
        const referee = this.roomVo.getReferee<FightingLandlordCardsVo>();
        const isBookmakerWin = referee.bookmakerUserId == resp.winner;
        settlementScore.init(this.userVo, scoringVo, isBookmakerWin);
        this.node.addChild(node);
        node.on("clickContinue", () => {
            this.changeRoomToWaitStatus();
            this.refresh();
            this.isInCheckoutInterface = false;
            node.destroy();
        });
        node.on("clickQuit", () => {
            this.clickQuitRoom();
        });
    }

    async clickQuitRoom() {
        const sendData = new SendData(this.roomVo.roomId, "quitGameRoom");
        NetManager.socket.sendData(sendData);
        this.roomVo = null;
        this.node.emit("close");
    }

    async clickSwitchRoom() {
        const sendData = new SendData(this.roomVo.roomId, "switchGameRoom");
        const resp = await NetManager.socket.sendDataSync(sendData);
        const roomVo: RoomVo = new RoomVo();
        roomVo.initJsonInfo(resp.roomVo);
        this.init(roomVo, this.userVo);
    }

    async clickPrepareGameBtn() {
        let method: string;
        if (this.roomVo.isUserPrepare()) method = "cancelPrepareGame";
        else method = "prepareGame";
        const sendData = new SendData(this.roomVo.roomId, method);
        const resp = await NetManager.socket.sendDataSync(sendData);
        let roomVo: RoomVo = new RoomVo();
        roomVo.initJsonInfo(resp.roomVo);
        this.init(roomVo, this.userVo);
    }
}
