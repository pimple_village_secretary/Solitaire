"use strict";
var __read = (this && this.__read) || function (o, n) {
    var m = typeof Symbol === "function" && o[Symbol.iterator];
    if (!m) return o;
    var i = m.call(o), r, ar = [], e;
    try {
        while ((n === void 0 || n-- > 0) && !(r = i.next()).done) ar.push(r.value);
    }
    catch (error) { e = { error: error }; }
    finally {
        try {
            if (r && !r.done && (m = i["return"])) m.call(i);
        }
        finally { if (e) throw e.error; }
    }
    return ar;
};
var __spreadArray = (this && this.__spreadArray) || function (to, from, pack) {
    if (pack || arguments.length === 2) for (var i = 0, l = from.length, ar; i < l; i++) {
        if (ar || !(i in from)) {
            if (!ar) ar = Array.prototype.slice.call(from, 0, i);
            ar[i] = from[i];
        }
    }
    return to.concat(ar || Array.prototype.slice.call(from));
};
Object.defineProperty(exports, "__esModule", { value: true });
var ObjUtil = /** @class */ (function () {
    function ObjUtil() {
    }
    /**
     * 判断对象是否为空，如果全等于 null、undefined、'' 则返回 true
     * @param obj
     */
    ObjUtil.isEmpty = function (obj) {
        return obj === null || obj === undefined || obj === '';
    };
    /**
     * 执行函数，若为空则不执行
     * @param func 要执行的函数对象
     * @param args 传给该函数的参数
     */
    ObjUtil.execFunc = function (func) {
        var args = [];
        for (var _i = 1; _i < arguments.length; _i++) {
            args[_i - 1] = arguments[_i];
        }
        if (this.isEmpty(func)) {
            return;
        }
        return func.apply(void 0, __spreadArray([], __read(args), false));
    };
    return ObjUtil;
}());
exports.default = ObjUtil;
