import FightingLandlordCardsVo from "../../../data/roledata/FightingLandlordCardsVo";
import GameScoringVo from "../../../data/roledata/GameScoringVo";
import UserVo from "../../../data/roledata/UserVo";
import SettlementScoreItem from "./SettlementScoreItem";

const {ccclass, property} = cc._decorator;

@ccclass
export default class SettlementScore extends cc.Component {

    @property(cc.Label)
    title: cc.Label = null;

    @property(cc.Sprite)
    icon: cc.Sprite = null;

    @property(cc.Node)
    content: cc.Node = null;

    @property(cc.Prefab)
    itemPrefab: cc.Prefab = null;

    init(userVo: UserVo, scoringVo: GameScoringVo, isBookmakerWin: boolean): void {
        let iconSpriteName: string;
        if (isBookmakerWin) iconSpriteName = "landlord";
        else iconSpriteName = "farmer";
        cc.loader.loadRes(`texture/room/${iconSpriteName}`, cc.SpriteFrame, (err, texture) => {
            this.icon.spriteFrame = texture;
        });
        if (scoringVo.scoring[userVo.roleId] >= 0) {
            this.title.string = `胜   利`;
            this.title.node.color = cc.color().fromHEX("#EFA91A");
        } else {
            this.title.string = `失   败`;
            this.title.node.color = cc.color().fromHEX("#CF3333");
        }
        Object.keys(scoringVo.scoring).forEach(key => {
            const node = cc.instantiate(this.itemPrefab);
            let settlementScoreItem = node.getComponent(SettlementScoreItem);
            let isWin: boolean;
            if (scoringVo.scoring[key] >= 0) isWin = true;
            else isWin = false;
            settlementScoreItem.init(Number(key), scoringVo.scoring[key], isWin);
            this.content.addChild(node);
        });
    }

    clickContinueBtn(): void {
        this.node.emit("clickContinue");
    }

    clickQuitBtn(): void {
        this.node.emit("clickQuit");
    }

}
