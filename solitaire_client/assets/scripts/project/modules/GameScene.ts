import GameRoleDataManager from "../data/GameRoleDataManager";
import GameStaticDataManager from "../data/GameStaticDataManager";
import UserVo from "../data/roledata/UserVo";
import NetManager from "../net/NetManager";
import SendData from "../net/SendData";
import GameContext from "./main/GameContext";


const {ccclass, property} = cc._decorator;
// import { io } from '../lib/socket.io';

@ccclass
export default class GameScene extends cc.Component {

    private userVo: UserVo = null;

    start () {
        GameRoleDataManager.init();
        let sendData = new SendData(GameRoleDataManager.userVo, "login");
        NetManager.socket.sendDataSync(sendData);
        GameContext.initListener();
        let test = GameStaticDataManager.pokerJsonInfoMap;
    }

}
