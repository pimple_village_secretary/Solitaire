import LogUtil from "../util/LogUtil";
import { ROOM_STATUS } from "./role/RoomStatusType";
import { ROOM_TYPE } from "./role/RoomType";
import RoomVo from "./role/RoomVo";
import UserVo from "./role/UserVo";

export default class RoomManagerVo {

    private roomVosNoFull: Map<number, RoomVo> = new Map<number, RoomVo>();

    private roomVosNoFullCount: number = 0;

    private roomVosFull: Map<number, RoomVo> = new Map<number, RoomVo>();

    private roomIdCounter: number = 0;

    private roomCanHaveMaximumPeople!: number;

    private roomType!: ROOM_TYPE;

    get isHaveFreeRoom(): boolean { return this.roomVosNoFullCount > 0; };
    
    init(maximumNumberRooms: number, roomType: ROOM_TYPE): void {
        this.roomCanHaveMaximumPeople = maximumNumberRooms;
        this.roomType = roomType
    }

    /**
     * 创建一个房间
     * @returns 返回一个RoomVo类型的数据
     */
    createRoom(): RoomVo {
        const vo = new RoomVo();
        vo.init(this.roomIdCounter, this.roomCanHaveMaximumPeople, this.roomType);
        this.roomVosNoFull.set(vo.roomId, vo);
        this.roomIdCounter += 1;
        this.roomVosNoFullCount += 1;
        return vo;
    }

    /**
     * 删除房间
     * @param roomId 
     */
    deleteRoom(roomId: number): void {
        if (this.findRoomOfId(roomId) != null) {
            this.roomVosNoFull.delete(roomId);
            this.roomVosNoFullCount -= 1;
            return;
        }
        this.roomVosFull.delete(roomId);
    }

    /**
     * 找到对应房间
     * @param roomId 房间号
     * @returns 
     */
    findRoomOfId(roomId: number): RoomVo | null {
        let vo = this.roomVosNoFull.get(roomId);
        if (vo) return vo;
        vo = this.roomVosFull.get(roomId);
        if (vo) return vo;
        return null;
    }

    /**
     * 用户加入房间
     * @param roomId 房间号
     * @param userVo 用户Vo
     * @returns 
     */
    addUserToRoom(roomId: number, userVo: UserVo): RoomVo | null {
        const vo = this.roomVosNoFull.get(roomId);
        if (!vo || vo.status == ROOM_STATUS.inGame) return null;
        vo.addToUser(userVo);

        if (!vo.isRoomFull()) return vo;
        this.roomVosFull.set(roomId, vo);
        this.roomVosNoFull.delete(roomId);
        this.roomVosNoFullCount -= 1;
        return vo;
    }

    /**
     * 用户退出房间 (如果用户准备了, 则自动移除准备队列)
     * @param roomId 
     * @param userId 
     */
    removeUserToRoom(roomId: number, userId: number): RoomVo | null {
        let vo = this.roomVosNoFull.get(roomId);
        if (vo) {
            vo.removeUser(userId);
            return vo;
        }
        vo = this.roomVosFull.get(roomId);
        if (!vo) return null;
        this.roomVosNoFull.set(roomId, vo);
        this.roomVosFull.delete(roomId);
        vo.removeUser(userId);
        vo.removeAllToPrepareQueue();       // 应该移除准备队列中的所有用户
        this.roomVosNoFullCount += 1;
        return vo;
    }

    /**
     * 返回一个空闲的房间号
     * @param curRoomId 当前房间号
     * @returns 
     */
    findFreeRoom(curRoomId: number = 0): RoomVo | null {
        if (!this.isHaveFreeRoom) return null;
        if (curRoomId == null) {
            for (const value of this.roomVosNoFull.values()) {
                if (!value.isRoomFull()) return value;
            }
            return null;
        }

        for (let i = curRoomId; i < this.roomIdCounter - 1; i++) {
            const result = this.roomVosNoFull.get(i + 1);
            if (result && !result.isRoomFull()) return result;
        }
        for (let i = 0; i <= curRoomId; i++) {
            const result = this.roomVosNoFull.get(i);
            if (result && !result.isRoomFull()) return result;
        }
        return null;
    }

    /**
     * 设置房间为初始状态（不移除房间的人）
     * @param roomId 房间号
     * @returns 
     */
    setInitializationState(roomId: number): void { 
        const vo = this.findRoomOfId(roomId);
        if (!vo) return;
        vo.changeRoomStatus(ROOM_STATUS.waiting);
        vo.removeReferee();
        vo.removeAllToPrepareQueue();
    }

}