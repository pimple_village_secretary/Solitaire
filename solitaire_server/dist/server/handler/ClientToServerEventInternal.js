"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var GameManagerVo_1 = __importDefault(require("../data/GameManagerVo"));
var GameStaticDataManager_1 = __importDefault(require("../data/GameStaticDataManager"));
var RoomStatusType_1 = require("../data/role/RoomStatusType");
var NetManager_1 = __importDefault(require("../net/NetManager"));
var LogUtil_1 = __importDefault(require("../util/LogUtil"));
var TimeUtil_1 = __importDefault(require("../util/TimeUtil"));
var ClientToServerEventInternal = /** @class */ (function () {
    function ClientToServerEventInternal() {
    }
    ClientToServerEventInternal.refreshCallLandlordRoomData = function (roleId, roomId, isCall) {
        var _this = this;
        var roomVo = GameManagerVo_1.default.roomManager.findRoomOfId(roomId);
        if (!roomVo || roomVo.status != RoomStatusType_1.ROOM_STATUS.setBookmaker)
            throw new Error("room is not exist or status is error");
        var referee = roomVo.getReferee();
        if (!referee)
            throw new Error("referee is not defined.");
        referee.callLandlord(roleId, isCall);
        LogUtil_1.default.writeServerLog("room ".concat(roomVo.roomId), "have a user call landlord[".concat(isCall, "], id: ").concat(roleId));
        var bookmakerId = referee.dealerBookmaker();
        if (bookmakerId) {
            referee.setBookmakerUserId(bookmakerId);
            roomVo.changeRoomStatus(RoomStatusType_1.ROOM_STATUS.inGame);
            TimeUtil_1.default.changeDelayedExecution("".concat(roomId), function () {
                _this.updatePlayCardsStatusToFalse(referee, roomId);
            });
            TimeUtil_1.default.runDelayedExecution("".concat(roomId));
            LogUtil_1.default.writeServerLog("room ".concat(roomVo.roomId), "bookmaker is user ".concat(bookmakerId));
            LogUtil_1.default.writeServerLog("room ".concat(roomVo.roomId), "room ".concat(roomId, " start join game status"));
        }
        referee.refreshWhoTurned(roomVo.status);
        referee.setCountdownStartTime(Date.now());
        LogUtil_1.default.writeServerLog("room ".concat(roomVo.roomId), "cur turn user is ".concat(referee.getTurnId()));
        return roomVo;
    };
    ClientToServerEventInternal.updateCalledLandlordStatusToFalse = function (referee, roomId) {
        var roleId = referee.getTurnId();
        if (roleId === null)
            throw new Error("turn id is null");
        var roomVo = ClientToServerEventInternal.refreshCallLandlordRoomData(roleId, roomId, false);
        NetManager_1.default.sendDataToRoomAllClient(roomId, { roomVo: roomVo }, "roomStatus");
    };
    ClientToServerEventInternal.refreshPlayCardsRoomData = function (roleId, roomId, cards) {
        var roomVo = GameManagerVo_1.default.roomManager.findRoomOfId(roomId);
        if (!roomVo || roomVo.status != RoomStatusType_1.ROOM_STATUS.inGame)
            throw new Error("room is not exist or status is error");
        var referee = roomVo.getReferee();
        if (!referee)
            throw new Error("referee is not defined.");
        LogUtil_1.default.writeServerLog("room ".concat(roomVo.roomId), "user ".concat(roleId, ", play cards: ").concat(GameStaticDataManager_1.default.getPokerName(cards)));
        if (referee.isMustPlayCards(roleId) && cards.length == 0)
            throw new Error("is Can't not play cards");
        if (!referee.isComplyRules(roleId, cards))
            throw new Error("is not comply rules");
        referee.autoIncrementNumberOfRounds();
        referee.play(roleId, cards);
        referee.increaseMagnificationOfCards(cards);
        LogUtil_1.default.writeServerLog("room ".concat(roomVo.roomId), "user ".concat(roleId, ", remaining cards: ").concat(referee.getUserHandCards(roleId)));
        var winner = referee.isGameOver();
        if (winner) {
            TimeUtil_1.default.removeDelayedExecution("".concat(roomId));
            roomVo.changeRoomStatus(RoomStatusType_1.ROOM_STATUS.GameOver);
            var scoringVo = referee.calculateReward(winner);
            GameManagerVo_1.default.userManager.gameScoring(scoringVo);
            LogUtil_1.default.writeServerLog("room ".concat(roomVo.roomId), "winner is ".concat(winner));
            LogUtil_1.default.writeServerLog("room ".concat(roomVo.roomId), "scoring: ".concat(scoringVo.scoring));
            referee.refreshWhoTurned(roomVo.status);
            referee.setCountdownStartTime(Date.now());
            return { roomVo: roomVo, scoringVo: scoringVo, winner: winner };
        }
        referee.refreshWhoTurned(roomVo.status);
        referee.setCountdownStartTime(Date.now());
        return { roomVo: roomVo };
    };
    ClientToServerEventInternal.updatePlayCardsStatusToFalse = function (referee, roomId) {
        var roleId = referee.getTurnId();
        if (roleId === null)
            throw new Error("roleId is null");
        var cards = [];
        if (referee.isMustPlayCards(roleId)) {
            var cardId = referee.findMinCards(roleId);
            if (cardId == null)
                throw new Error("hand no cards");
            cards.push(cardId);
        }
        var sendData = ClientToServerEventInternal.refreshPlayCardsRoomData(roleId, roomId, cards);
        NetManager_1.default.sendDataToRoomAllClient(roomId, sendData, "roomStatus");
        var roomVo = GameManagerVo_1.default.roomManager.findRoomOfId(roomId);
        if (roomVo == null)
            throw new Error("room is not exist.");
        if (roomVo.status == RoomStatusType_1.ROOM_STATUS.GameOver) {
            GameManagerVo_1.default.roomManager.setInitializationState(roomId);
        }
    };
    return ClientToServerEventInternal;
}());
exports.default = ClientToServerEventInternal;
