export enum ROOM_STATUS {
    waiting,            // 等待中
    setBookmaker,       // 设置庄家中
    inGame,             // 游戏中
    GameOver,           // 游戏结束
}