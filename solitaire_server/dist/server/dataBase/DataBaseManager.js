"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var mysql_1 = __importDefault(require("mysql"));
var DataBaseConfig_1 = __importDefault(require("./DataBaseConfig"));
var DataBaseManager = /** @class */ (function () {
    function DataBaseManager() {
    }
    DataBaseManager.query = function (sql) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.pool.getConnection(function (err, connection) {
                try {
                    if (err)
                        throw new Error(err.message);
                    connection.query(sql, function (error, results) {
                        if (error)
                            throw new Error(error.message);
                        else
                            resolve(JSON.stringify(results));
                        connection.release(); // 释放该链接，把该链接放回池里供其他人使用;
                    });
                }
                catch (e) {
                    console.error(e);
                    connection.release(); // 释放该链接，把该链接放回池里供其他人使用;
                    return null;
                }
            });
        });
    };
    DataBaseManager.getStaticData = function (tableName) {
        var results = this.query("SELECT * FROM ".concat(tableName));
        if (results)
            return results;
        console.log("load ".concat(tableName, " failed"));
        return null;
    };
    DataBaseManager.pool = mysql_1.default.createPool(DataBaseConfig_1.default);
    return DataBaseManager;
}());
exports.default = DataBaseManager;
