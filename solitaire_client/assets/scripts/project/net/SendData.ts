export default class SendData {

    message: any;
    
    method: string;

    cmdIndex: number

    private static cmdIndexCount = 1;

    constructor(message: any, method: string) {
        this.message = message;
        this.method = method;
        this.cmdIndex = SendData.cmdIndexCount;
        SendData.cmdIndexCount += 1;
    }

}