import LogUtil from "./LogUtil";

interface TimeOutHandler {
    callback: (() => void);
    timeout: number;
}

export default class TimeUtil {

    private static eventMapOnce: Map<string, NodeJS.Timeout> = new Map<string, NodeJS.Timeout>();

    private static eventMap: Map<string, NodeJS.Timer> = new Map<string, NodeJS.Timer>();

    private static handlerMap: Map<string, TimeOutHandler> = new Map<string, TimeOutHandler>();

    private static isExist(evenName: string): boolean {
        return this.eventMapOnce.has(evenName) || this.eventMap.has(evenName);
    }

    private static closeTimeout(evenName: string): NodeJS.Timeout | null {
        let id = this.eventMapOnce.get(evenName);
        if (!id) return null;
        clearTimeout(id);
        return id;
    }

    private static closeInterval(evenName: string): NodeJS.Timer | null {
        let id = this.eventMap.get(evenName);
        if (!id) return null;
        clearInterval(id);
        return id;
    }

    private static runTimeout(evenName: string, callback: () => void, timeout: number): NodeJS.Timeout {
        const id = setTimeout(() => {
            LogUtil.writeServerLog(`TimeUtil`, `delayed execution, event name: ${evenName}`);
            callback();
            this.removeDelayedExecution(evenName);
        }, timeout);
        this.eventMapOnce.set(evenName, id);
        this.handlerMap.set(evenName, { callback, timeout });
        return id;
    }

    private static runInterval(evenName: string, callback: () => void, timeout: number): NodeJS.Timer {
        const id = setInterval(() => {
            LogUtil.writeServerLog(`TimeUtil`, `delayed execution, event name: ${evenName}`);
            callback();
        }, timeout);
        this.eventMap.set(evenName, id);
        this.handlerMap.set(evenName, { callback, timeout });
        return id;
    }

    /**
     * 执行一次延时执行
     * @param evenName 事件名
     * @param callback 回调事件
     * @param timeout 超时时间
     * @returns 
     */
    static delayedExecutionOnce(evenName: string, callback: () => void, timeout: number): void {
        if (this.isExist(evenName)) return;
        const timeoutId = this.runTimeout(evenName, callback, timeout);
        LogUtil.writeServerLog(`TimeUtil`, `add delayed execution, event name: ${evenName}, timeout: ${timeoutId}`);
    }

    /**
     * 循环执行延时执行
     * @param evenName 事件名
     * @param callback 回调事件
     * @param timeout 超时时间
     * @returns 
     */
    static delayedExecution(evenName: string, callback: () => void, timeout: number): void {
        if (this.isExist(evenName)) return;
        const intervalId = this.runInterval(evenName, callback, timeout);
        LogUtil.writeServerLog(`TimeUtil`, `add delayed execution, event name: ${evenName}, interval: ${intervalId}`);
    }

    /**
     * 移除延时执行
     * @param evenName 事件名
     * @returns 
     */
    static removeDelayedExecution(evenName: string): void {
        if (!this.isExist(evenName)) return;
        const intervalId = this.closeInterval(evenName);
        const timeoutId = this.closeTimeout(evenName);
        this.handlerMap.delete(evenName);
        this.eventMap.delete(evenName);
        this.eventMapOnce.delete(evenName);
        if (intervalId != null) LogUtil.writeServerLog(`TimeUtil`, `remove timeout, event name: ${evenName}, interval: ${intervalId}`);
        if (timeoutId != null) LogUtil.writeServerLog(`TimeUtil`, `remove timeout, event name: ${evenName}, timeout: ${timeoutId}`);
    }

    /**
     * 移除所有延时执行
     */
    static removeAllDelayedExecution(): void {
        const keys = this.handlerMap.keys();
        for (let key of keys) this.removeDelayedExecution(key);
    }

    /**
     * 停止某个延时执行
     * @param evenName 事件名
     * @returns 
     */
    static stopDelayedExecution(evenName: string): void {
        if (!this.isExist(evenName)) return;
        const intervalId = this.closeInterval(evenName);
        const timeoutId = this.closeTimeout(evenName);
        if (intervalId != null) LogUtil.writeServerLog(`TimeUtil`, `stop timeout, event name: ${evenName}, interval: ${intervalId}`);
        if (timeoutId != null) LogUtil.writeServerLog(`TimeUtil`, `stop timeout, event name: ${evenName}, timeout: ${timeoutId}`);
    }

    /**
     * 开始执行某个延时执行
     * @param evenName 事件名
     * @returns 
     */
    static runDelayedExecution(evenName: string): void {
        if (!this.isExist(evenName)) return;
        const value = this.handlerMap.get(evenName);
        if (!value) return;
        if (this.eventMap.has(evenName)) {
            this.closeInterval(evenName);
            const intervalId = this.runInterval(evenName, value.callback, value.timeout);
            LogUtil.writeServerLog(`TimeUtil`, `run timeout, event name: ${evenName}, interval: ${intervalId}`);
        } else if (this.eventMapOnce.has(evenName)) {
            this.closeTimeout(evenName);
            const timeoutId = this.runTimeout(evenName, value.callback, value.timeout);
            LogUtil.writeServerLog(`TimeUtil`, `run timeout, event name: ${evenName}, timeout: ${timeoutId}`);
        }
    }

    /**
     * 改变某个延时执行的回调函数
     * 
     * 会停止这个延时执行，需要手动再开启一下
     * @param evenName 事件名
     * @param callback 回调事件
     * @returns 
     */
    static changeDelayedExecution(evenName: string, callback: () => void) {
        if (!this.isExist(evenName)) return;
        LogUtil.writeServerLog(`TimeUtil`, `change timeout, event name: ${evenName}`);
        this.stopDelayedExecution(evenName);
        const timeout = this.handlerMap.get(evenName)?.timeout;
        if (!timeout) return;
        this.handlerMap.set(evenName, { callback, timeout });
        // this.runDelayedExecution(evenName);
    }


}