import { Socket } from "socket.io";
import LogUtil from "../util/LogUtil";
import GameScoringVo from "./role/GameScoringVo";
import UserVo from "./role/UserVo";

export default class UserManagerVo {

    /**
     * 套接字id索引用户Id
     */
    socketIndexUser: Map<string, number> = new Map<string, number>();

    /**
     * 用户id索引套接字和用户vo
     */
    userIndexSocketUsers: Map<number, { socket: Socket, user: UserVo }> = new Map<number, { socket: Socket, user: UserVo}>();

    init(): void {

    }

    /**
     * 添加套接字和用户
     * @param socket 
     * @param user 
     * @returns void
     */
    addToUser(socket: Socket, user: UserVo): void {
        if (this.socketIndexUser.has(socket.id) || this.userIndexSocketUsers.has(user.roleId)) return;
        this.socketIndexUser.set(socket.id, user.roleId);
        this.userIndexSocketUsers.set(user.roleId, { socket, user });
    }

    /**
     * 根据socket Id删除用户和对应的套接字
     * @param socketId 要被删除的socket id
     * @returns void
     */
    removeUser(socketId: string): void {
        if (!this.socketIndexUser.has(socketId)) return;
        const roleId = this.socketIndexUser.get(socketId);
        if (!roleId) return;
        // 关闭删除套接字和用户
        this.socketIndexUser.delete(socketId);
        this.userIndexSocketUsers.delete(roleId);
    }

    /**
     * 使用socket id找到对应的用户
     * @param id socket id
     * @returns 
     */
    findUserUsingSocketId(id: string): UserVo | null {
        const userId = this.socketIndexUser.get(id);
        if (!userId) return null;
        const result = this.userIndexSocketUsers.get(userId);
        if (!result) return null;
        return result.user;
    }

    /**
     * 使用user id找到对应用户的socket
     * @param id 用户id
     * @returns 
     */
    findSocketUsingUserId(id: number): Socket | null {
        const result = this.userIndexSocketUsers.get(id);
        if (!result) return null;
        return result.socket;
    }

    /**
     * 扣钱
     * @param roleId 用户ID
     * @param num 对应扣除的数量
     * @returns 返回现有的数量
     */
    minusMoney(roleId: number, num: number): number | null {
        let userVo = this.userIndexSocketUsers.get(roleId)?.user;
        if (!userVo) return null;
        userVo.money -= num;
        return userVo.money;
    }

    /**
     * 加钱
     * @param roleId 用户ID
     * @param num 对应增加的数量
     * @returns 返回现有的数量
     */
    addMoney(roleId: number, num: number): number | null {
        let userVo = this.userIndexSocketUsers.get(roleId)?.user;
        if (!userVo) return null;
        userVo.money += num;
        return userVo.money;
    }

    /**
     * 判断是否有足够的钱
     * @param roleId 用户ID
     * @param num 游戏货币数量
     */
    isEnoughMoney(roleId: number, num: number): boolean | null {
        let userVo = this.userIndexSocketUsers.get(roleId)?.user;
        if (!userVo) return null;
        if (userVo.money >= num) return true;
        return false;
    }

    /**
     * 获取用户的钱
     * @param roleId 用户ID
     * @returns 
     */
    getUserMony(roleId: number): number | null {
        let userVo = this.userIndexSocketUsers.get(roleId)?.user;
        if (!userVo) return null;
        return userVo.money;
    }

    gameScoring(scoringVo: GameScoringVo): void {
        Object.keys(scoringVo.scoring).forEach(key => {
            this.addMoney(Number(key), scoringVo.scoring[Number(key)])
        });
    }

}