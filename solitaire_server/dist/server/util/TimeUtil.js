"use strict";
var __values = (this && this.__values) || function(o) {
    var s = typeof Symbol === "function" && Symbol.iterator, m = s && o[s], i = 0;
    if (m) return m.call(o);
    if (o && typeof o.length === "number") return {
        next: function () {
            if (o && i >= o.length) o = void 0;
            return { value: o && o[i++], done: !o };
        }
    };
    throw new TypeError(s ? "Object is not iterable." : "Symbol.iterator is not defined.");
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var LogUtil_1 = __importDefault(require("./LogUtil"));
var TimeUtil = /** @class */ (function () {
    function TimeUtil() {
    }
    TimeUtil.isExist = function (evenName) {
        return this.eventMapOnce.has(evenName) || this.eventMap.has(evenName);
    };
    TimeUtil.closeTimeout = function (evenName) {
        var id = this.eventMapOnce.get(evenName);
        if (!id)
            return null;
        clearTimeout(id);
        return id;
    };
    TimeUtil.closeInterval = function (evenName) {
        var id = this.eventMap.get(evenName);
        if (!id)
            return null;
        clearInterval(id);
        return id;
    };
    TimeUtil.runTimeout = function (evenName, callback, timeout) {
        var _this = this;
        var id = setTimeout(function () {
            LogUtil_1.default.writeServerLog("TimeUtil", "delayed execution, event name: ".concat(evenName));
            callback();
            _this.removeDelayedExecution(evenName);
        }, timeout);
        this.eventMapOnce.set(evenName, id);
        this.handlerMap.set(evenName, { callback: callback, timeout: timeout });
        return id;
    };
    TimeUtil.runInterval = function (evenName, callback, timeout) {
        var id = setInterval(function () {
            LogUtil_1.default.writeServerLog("TimeUtil", "delayed execution, event name: ".concat(evenName));
            callback();
        }, timeout);
        this.eventMap.set(evenName, id);
        this.handlerMap.set(evenName, { callback: callback, timeout: timeout });
        return id;
    };
    /**
     * 执行一次延时执行
     * @param evenName 事件名
     * @param callback 回调事件
     * @param timeout 超时时间
     * @returns
     */
    TimeUtil.delayedExecutionOnce = function (evenName, callback, timeout) {
        if (this.isExist(evenName))
            return;
        var timeoutId = this.runTimeout(evenName, callback, timeout);
        LogUtil_1.default.writeServerLog("TimeUtil", "add delayed execution, event name: ".concat(evenName, ", timeout: ").concat(timeoutId));
    };
    /**
     * 循环执行延时执行
     * @param evenName 事件名
     * @param callback 回调事件
     * @param timeout 超时时间
     * @returns
     */
    TimeUtil.delayedExecution = function (evenName, callback, timeout) {
        if (this.isExist(evenName))
            return;
        var intervalId = this.runInterval(evenName, callback, timeout);
        LogUtil_1.default.writeServerLog("TimeUtil", "add delayed execution, event name: ".concat(evenName, ", interval: ").concat(intervalId));
    };
    /**
     * 移除延时执行
     * @param evenName 事件名
     * @returns
     */
    TimeUtil.removeDelayedExecution = function (evenName) {
        if (!this.isExist(evenName))
            return;
        var intervalId = this.closeInterval(evenName);
        var timeoutId = this.closeTimeout(evenName);
        this.handlerMap.delete(evenName);
        this.eventMap.delete(evenName);
        this.eventMapOnce.delete(evenName);
        if (intervalId != null)
            LogUtil_1.default.writeServerLog("TimeUtil", "remove timeout, event name: ".concat(evenName, ", interval: ").concat(intervalId));
        if (timeoutId != null)
            LogUtil_1.default.writeServerLog("TimeUtil", "remove timeout, event name: ".concat(evenName, ", timeout: ").concat(timeoutId));
    };
    /**
     * 移除所有延时执行
     */
    TimeUtil.removeAllDelayedExecution = function () {
        var e_1, _a;
        var keys = this.handlerMap.keys();
        try {
            for (var keys_1 = __values(keys), keys_1_1 = keys_1.next(); !keys_1_1.done; keys_1_1 = keys_1.next()) {
                var key = keys_1_1.value;
                this.removeDelayedExecution(key);
            }
        }
        catch (e_1_1) { e_1 = { error: e_1_1 }; }
        finally {
            try {
                if (keys_1_1 && !keys_1_1.done && (_a = keys_1.return)) _a.call(keys_1);
            }
            finally { if (e_1) throw e_1.error; }
        }
    };
    /**
     * 停止某个延时执行
     * @param evenName 事件名
     * @returns
     */
    TimeUtil.stopDelayedExecution = function (evenName) {
        if (!this.isExist(evenName))
            return;
        var intervalId = this.closeInterval(evenName);
        var timeoutId = this.closeTimeout(evenName);
        if (intervalId != null)
            LogUtil_1.default.writeServerLog("TimeUtil", "stop timeout, event name: ".concat(evenName, ", interval: ").concat(intervalId));
        if (timeoutId != null)
            LogUtil_1.default.writeServerLog("TimeUtil", "stop timeout, event name: ".concat(evenName, ", timeout: ").concat(timeoutId));
    };
    /**
     * 开始执行某个延时执行
     * @param evenName 事件名
     * @returns
     */
    TimeUtil.runDelayedExecution = function (evenName) {
        if (!this.isExist(evenName))
            return;
        var value = this.handlerMap.get(evenName);
        if (!value)
            return;
        if (this.eventMap.has(evenName)) {
            this.closeInterval(evenName);
            var intervalId = this.runInterval(evenName, value.callback, value.timeout);
            LogUtil_1.default.writeServerLog("TimeUtil", "run timeout, event name: ".concat(evenName, ", interval: ").concat(intervalId));
        }
        else if (this.eventMapOnce.has(evenName)) {
            this.closeTimeout(evenName);
            var timeoutId = this.runTimeout(evenName, value.callback, value.timeout);
            LogUtil_1.default.writeServerLog("TimeUtil", "run timeout, event name: ".concat(evenName, ", timeout: ").concat(timeoutId));
        }
    };
    /**
     * 改变某个延时执行的回调函数
     *
     * 会停止这个延时执行，需要手动再开启一下
     * @param evenName 事件名
     * @param callback 回调事件
     * @returns
     */
    TimeUtil.changeDelayedExecution = function (evenName, callback) {
        var _a;
        if (!this.isExist(evenName))
            return;
        LogUtil_1.default.writeServerLog("TimeUtil", "change timeout, event name: ".concat(evenName));
        this.stopDelayedExecution(evenName);
        var timeout = (_a = this.handlerMap.get(evenName)) === null || _a === void 0 ? void 0 : _a.timeout;
        if (!timeout)
            return;
        this.handlerMap.set(evenName, { callback: callback, timeout: timeout });
        // this.runDelayedExecution(evenName);
    };
    TimeUtil.eventMapOnce = new Map();
    TimeUtil.eventMap = new Map();
    TimeUtil.handlerMap = new Map();
    return TimeUtil;
}());
exports.default = TimeUtil;
