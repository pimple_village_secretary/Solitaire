"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.FACE_CARDS = void 0;
var FACE_CARDS;
(function (FACE_CARDS) {
    FACE_CARDS[FACE_CARDS["diamond"] = 0] = "diamond";
    FACE_CARDS[FACE_CARDS["club"] = 1] = "club";
    FACE_CARDS[FACE_CARDS["heart"] = 2] = "heart";
    FACE_CARDS[FACE_CARDS["spade"] = 3] = "spade";
    FACE_CARDS[FACE_CARDS["attendant"] = 4] = "attendant";
    FACE_CARDS[FACE_CARDS["queen"] = 5] = "queen";
    FACE_CARDS[FACE_CARDS["king"] = 6] = "king";
    FACE_CARDS[FACE_CARDS["smallJoker"] = 7] = "smallJoker";
    FACE_CARDS[FACE_CARDS["bigJoker"] = 8] = "bigJoker";
})(FACE_CARDS = exports.FACE_CARDS || (exports.FACE_CARDS = {}));
