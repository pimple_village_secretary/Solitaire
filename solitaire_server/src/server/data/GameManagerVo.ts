import { ROOM_TYPE } from "./role/RoomType";
import RoomManagerVo from "./RoomManagerVo";
import UserManagerVo from "./UserManagerVo";

export default class GameManagerVo {

    static userManager = new UserManagerVo();
    
    static roomManager = new RoomManagerVo();

    static init() {
        GameManagerVo.userManager.init();
        GameManagerVo.roomManager.init(3, ROOM_TYPE.FightingLandlord);
    }

}