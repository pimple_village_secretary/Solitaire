export default class LoadResInfo {

    private needLoadCount = 0;      // 需要加载的

    private loadedCount = 0;        // 已经加载的

    private callback: Function = null;    // loadedCount >= neetLoadCount时的回调函数

    init(callback: Function): void {
        this.callback = callback;
    }

    addNeedLoadCount(num: number): void {
        this.needLoadCount += num;
    }

    addLoadedCount(num: number): void {
        this.loadedCount += num;
        if (this.loadedCount >= this.needLoadCount) {
            this.callback();
        }
    }

}