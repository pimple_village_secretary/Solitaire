"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.PLAY_CARDS_TYPE = void 0;
/**
 * rocket   火箭：大小王在一起的牌型，即双王牌，此牌型最大，什么牌型都可以打。
 *
 * bomb     炸弹：相同点数的四张牌在一起的牌型，比如四条A。除火箭外，它可以打任何牌型，炸弹对炸弹时，要比大小。
 *
 * single   单支（一手牌）：单张牌，如一支3。
 *
 * pair     对子（一手牌）：相同点数的两张牌在一起的牌型，比如55。
 *
 * threeOfKind      三条：相同点数的三张牌在一起的牌型，比如三条4。
 *
 * threeWithSingle    三带一：三条 ＋ 一手牌的牌型，比如AAA+9
 *
 * threeWithPair    三带二：三条 ＋ 一手牌的牌型，比如AAA+77。
 *
 * singleStraight   单顺：五张或更多的连续单支牌组成的牌型，比如45678或345678910JQKA。2和大小王不可以连。
 *
 * doubleStraight   双顺：三对或更多的连续对子组成的牌型，比如334455或445566778899。2和大小王不可以连。
 *
 * triStraight      三顺：二个或更多的连续三条组成的牌型，比如777888或444555666777。2和大小王不可以连。
 *
 * planeWithWings   飞机带翅膀：三顺 ＋ 同数量的一手牌，比如777888+3+6或444555666+33+77+88。
 *
 * fourWitTwo       四带二：四条+两手牌。比如AAAA+7+9或9999+33+55。
 */
var PLAY_CARDS_TYPE;
(function (PLAY_CARDS_TYPE) {
    PLAY_CARDS_TYPE["rocket"] = "rocket";
    PLAY_CARDS_TYPE["bomb"] = "bomb";
    PLAY_CARDS_TYPE["single"] = "single";
    PLAY_CARDS_TYPE["pair"] = "pair";
    PLAY_CARDS_TYPE["threeOfKind"] = "threeOfKind";
    PLAY_CARDS_TYPE["threeWithSingle"] = "threeWithSingle";
    PLAY_CARDS_TYPE["threeWithPair"] = "threeWithPair";
    PLAY_CARDS_TYPE["singleStraight"] = "singleStraight";
    PLAY_CARDS_TYPE["doubleStraight"] = "doubleStraight";
    PLAY_CARDS_TYPE["triStraight"] = "triStraight";
    PLAY_CARDS_TYPE["planeWithWings"] = "planeWithWings";
    PLAY_CARDS_TYPE["fourWitTwo"] = "fourWitTwo";
    PLAY_CARDS_TYPE["none"] = "none";
})(PLAY_CARDS_TYPE = exports.PLAY_CARDS_TYPE || (exports.PLAY_CARDS_TYPE = {}));
