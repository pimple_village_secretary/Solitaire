import NumericalValueUtil from "../../util/NumericalValueUtil";
import TimeUtil from "../../util/TimeUtil";
import CardsPool from "./CardsPool";
import GameScoringVo from "./GameScoringVo";
import { ROOM_STATUS } from "./RoomStatusType";
import UserVo from "./UserVo";

export default abstract class RefereeVo {

    protected bookmakerCardsNum: number = 0;

    protected handCardsNum: number = 0;

    protected bookmakerUserId: number | null = null;                        // 庄家Id

    protected bookmakerCards: Array<number> = [];                           // 庄家牌

    protected handCards: { [key: number]: Array<number | null> } = {};      // 用户手牌

    protected magnification: { [key: number]: number } = {};                // 倍率(分别存储每个人的倍率)

    protected initBet: number = 1;                  // 起始赌注

    private cardsPool: CardsPool | null = null;     // 卡牌池

    protected numberOfRounds: number = 0;           // 回合数

    protected turnId: number | null = null;         // 谁的回合

    private countdownTime: number = 25;             // 倒计时

    private countdownStartTime: number = 0;         // 起始时间(倒计时起始时间)

    protected slots: Array<number> = [];            // 槽位

    /**
     * 初始化一套牌组
     * 
     * 这个函数初始化的同时会自动洗好一套牌组
     */
    init(users: Array<UserVo | null>, initBet: number = 1, bookmakerCardsNum?: number, handCardsNum?: number) {
        this.initBet = initBet;
        this.bookmakerCardsNum = bookmakerCardsNum??this.bookmakerCardsNum;
        this.handCardsNum = handCardsNum??this.bookmakerCardsNum;
        this.cardsPool = new CardsPool();
        this.cardsPool.init();

        users.forEach(element => {
            if (!element) return;
            this.handCards[element.roleId] = new Array<number>();
            this.magnification[element.roleId] = 1;     // 初始化倍率
            this.slots.push(element.roleId);            // 初始化槽位
        });
    }

    /**
     * 洗牌
     */
    shuffle(): void {
        if (!this.cardsPool) return;
        this.cardsPool.init();
    }

    /**
     * 发牌
     */
    deal(sortingAlgorithm: (a: number | null, b: number | null) => number): void {
        if (!this.cardsPool) throw new Error(`cards pool is not init!`);
        if (this.slots.length == 0) throw new Error(`users is null.`);
        this.slots.forEach(roleId => {
            this.draw(roleId, this.handCardsNum);
            this.handCards[roleId].sort(sortingAlgorithm);
        });

        const cards = this.cardsPool.getCardsOfCardsPool(this.bookmakerCardsNum);
        if (cards.length < this.bookmakerCardsNum) throw new Error(`cards pool is not have ${this.bookmakerCardsNum} cards`);
        this.bookmakerCards = cards;
    }

    /**
     * 出牌
     * @param roleId 出牌的用户Id
     * @param cards 牌的槽位
     */
    play(roleId: number, cards: Array<number>): void {
        const nums = this.handCards[roleId];
        const slots = NumericalValueUtil.findArrayElementIndex(nums, cards);
        if (!slots) return;
        slots.forEach(element => {
            this.handCards[roleId][element] = null;
        });
    }

    /**
     * 摸牌
     * @param roleId 摸牌的用户Id
     */
    draw(roleId: number, num: number): void {
        if (!this.cardsPool) throw new Error(`cards pool is not init!`);
        const cards = this.cardsPool.getCardsOfCardsPool(num);
        if (cards.length < num) throw new Error(`cards pool is not have ${num} cards`);
        cards.forEach(element => {
            this.handCards[roleId].push(element);
        });
    }

    /**
     * 设置庄家ID
     * @param roleId 用户ID
     */
    setBookmakerUserId(roleId: number | null): void { this.bookmakerUserId = roleId; }

    /**
     * 回合数自增
     */
    autoIncrementNumberOfRounds(): void { this.numberOfRounds += 1; };

    /**
     * 增加倍率
     * @param magnification 倍率
     */
    increaseMagnification(roleId: number, magnification: number): void {
        this.magnification[roleId] = magnification;     // magnification;
    }

    /**
     * 设置倒计时时间
     * 
     * 默认为25秒
     * @param time 单位为秒
     */
    setCountdownTime(time: number): void { this.countdownTime = time; };

    /**
     * 获取倒计时时间(秒)
     */
    getCountdownTime(): number { return this.countdownTime; };

    /**
     * 设置倒计时时间起始时间
     * @param time 时间戳
     */
    setCountdownStartTime(time: number): void { this.countdownStartTime = time; };

    /**
     * 获取倒计时时间(时间戳)
     */
    getCountdownStartTime(): number { return this.countdownStartTime; };

    /**
     * 谁的回合
     */
    getTurnId(): number | null { return this.turnId; };

    /**
     * 是否符合出牌规则
     */
    abstract isComplyRules(roleId: number, cards: Array<number>): boolean;

    /**
     * 是否游戏结束
     * @returns 如果游戏结束, 则返回赢家Id, 反之返回null
     */
    abstract isGameOver(): number | null;

    /**
     * 轮到谁的回合
     */
    abstract refreshWhoTurned(status: ROOM_STATUS): void;

    /**
     * 根据牌组来增加倍率
     */
    abstract increaseMagnificationOfCards(cards: Array<number>): void;

    /**
     * 比较两幅牌组大小
     * @param cards1 牌组1
     * @param cards2 牌组2
     * @returns cards1大于cards2则返回true， 反之返回false
     */
    abstract compareCards(cards1: Array<number>, cards2: Array<number>): boolean;

    /**
     * 获取对应用户的手牌
     * @param roleId 
     */
    getUserHandCards(roleId: number): Array<number | null> {
        return this.handCards[roleId];
    }

    /**
     * 计算用户的奖励
     * @param winner 赢家id
     * @return 用户对应的增或减游戏货币（正或负数）
     */
    abstract calculateReward(winner: number): GameScoringVo;
}