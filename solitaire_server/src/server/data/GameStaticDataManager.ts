import DataBaseManager from "../dataBase/DataBaseManager";
import PokerJsonInfo from "./static/PokerJsonInfo";

export default class GameStaticDataManager {
    
    static pokerJsonInfoMap: Map<number, PokerJsonInfo> = new Map<number, PokerJsonInfo>();

    static async initAll(): Promise<void> {
        return new Promise<void>(async (resolve, reject) => {

            this.pokerJsonInfoMap = await this.initSingle<PokerJsonInfo>("pokers");

            resolve();
        })
    }

    static initSingle<T>(tableName: string) {
        return new Promise<any>(async (resolve, reject) => {
            const json = await DataBaseManager.getStaticData(tableName);
            if (json == null) {
                resolve({});
                return;
            }
            const data: Array<any> = JSON.parse(json)
            let result: Map<number, T> = new Map<number, T>();
            data.forEach(element => {
                result.set(element.id, element);
            });
            resolve(result);;
        })
    }

    static getPokerJsonInfoMap(cards: Array<number>): Array<PokerJsonInfo> {
        let result: Array<PokerJsonInfo> = [];
        for (let card of cards) {
            const jsonInfo = this.pokerJsonInfoMap.get(card);
            if (!jsonInfo) continue;
            result.push(jsonInfo);
        }
        return result;
    }

    static getPokerName(cards: Array<number>): Array<string> {
        let result: Array<string> = [];
        for (let card of cards) {
            const jsonInfo = this.pokerJsonInfoMap.get(card);
            if (!jsonInfo) continue;
            result.push(jsonInfo.name);
        }
        return result;
    }

}