interface ClientToServerEvents {
    login: () => void;
    findFreeRoom: () => void;
    findRoomOfId: () => void;
    quitGameRoom: () => void;
    startGame: () => void;
}