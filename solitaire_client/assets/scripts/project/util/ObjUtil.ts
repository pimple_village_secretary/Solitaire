export default class ObjUtil {

    private constructor() {}

    /**
     * 判断对象是否为空，如果全等于 null、undefined、'' 则返回 true
     * @param obj
     */
    static isEmpty(obj: any) {
        return obj === null || obj === undefined || obj === '';
    }

    /**
     * 执行函数，若为空则不执行
     * @param func 要执行的函数对象
     * @param args 传给该函数的参数
     */
    static execFunc(func: Function, ...args) {
        if (this.isEmpty(func)) {
            return;
        }
        return func(...args);
    }

}