import ObjUtil from "../util/ObjUtil";
import SendData from "./SendData";
import ServerConfig from "./ServerConfig";

export default class BaseSocket {

    socket: any;

    private cmdIndexCallbackMap: Map<number, Function> = new Map();

    private listenCallbackMap: Map<string, Function> = new Map();

    connect(config: ServerConfig) {
        this.socket = io.connect(`${config.serverIp}:${config.serverPort}`);

        this.socket.on("connect", () => {
            // 发送一个事件, 告诉连接成功
            console.log("connect success.");
        });

        this.socket.on("message", (response: any) => {
            response = JSON.parse(response);
            if (response.cmdIndex === 0) {
                let callFunc = this.listenCallbackMap.get(response.method);
                ObjUtil.execFunc(callFunc, response.message);
                return;
            }
            let callFunc = this.cmdIndexCallbackMap.get(response.cmdIndex);
            this.cmdIndexCallbackMap.delete(response.cmdIndex);
            ObjUtil.execFunc(callFunc, response.message);
        });

        this.socket.on("close", (response: any) => {
            // 发送一个事件, 告诉断开连接
            console.log("connect close.");
        });
    }

    /**
     * 向服务器发送数据, 无需获取数据返回, 但可以通过回调函数拿到返回的数据, 并做处理
     * @param sendData 发送数据
     * @param callback 服务器返回数据后将调用的回调函数
     * @returns void
     */
    sendData(sendData: SendData, callback?: (even) => void): void {
        this.socket.emit(sendData.method, JSON.stringify(sendData));
        if (!callback) return;
        if (this.cmdIndexCallbackMap.has(sendData.cmdIndex)) {
            console.warn("warning: cmdIndex same!");
            return;
        }
        this.cmdIndexCallbackMap.set(sendData.cmdIndex, callback);
    }

    /**
     * 向服务器发送数据, 有数据返回(异步)
     * @param sendData 发送数据
     * @returns 返回Promise类型的数据
     */
    sendDataSync(sendData: SendData) {
        return new Promise<any>((resolve, reject) => {
            this.sendData(sendData, (even) => {
                resolve(even);
            });
        })
    }

    /**
     * 监听事件, 并制定对应的处理函数
     * @param listenEvent 监听事件
     * @param handler 处理函数
     * @param isAutoRemoveEventHandler 是否自动删除此监听事件
     */
    addEventListener(listenEvent: string, handler: (resp) => void, isAutoRemoveEventHandler: boolean): void {
        if (isAutoRemoveEventHandler) {
            let tempHandler = handler;
            handler = (resp) => {
                this.offEventListener(listenEvent);
                tempHandler(resp);
            }
        }
        this.listenCallbackMap.set(listenEvent, handler);
    }

    /**
     * 移除监听事件
     * @param listenEvent 监听事件
     */
    offEventListener(listenEvent: string): void {
        this.listenCallbackMap.delete(listenEvent);
    }

}