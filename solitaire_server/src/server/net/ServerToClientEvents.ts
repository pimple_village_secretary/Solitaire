interface ServerToClientEvents {
    joinGameRoom: () => void;
    quitGameRoom: () => void;
    havePeopleJoinCurRoom: () => void;
    havePeopleQuitCurRoom: () => void;
}




