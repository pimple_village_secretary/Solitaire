import GameStaticDataManager from "../../data/GameStaticDataManager";
import { POKER_POINTS } from "../../data/roledata/PokerPointsType";
import { POKER_SUITS } from "../../data/roledata/PokerSuitsType";
import PokerJsonInfo from "../../data/static/PokerJsonInfo";

const {ccclass, property} = cc._decorator;

@ccclass
export default class Poker extends cc.Component {

    @property(cc.Node)
    private pokerFrontBgNode: cc.Node = null;

    @property(cc.Node)
    private pokerBackBgNode: cc.Node = null;

    @property(cc.Sprite)
    private faceCardSprite: cc.Sprite = null;

    @property(cc.Sprite)
    private pokerSuitSprite_1: cc.Sprite = null;

    @property(cc.Label)
    private pokerPointLabel_1: cc.Label = null;

    @property(cc.Sprite)
    private pokerSuitSprite_2: cc.Sprite = null;

    @property(cc.Label)
    private pokerPointLabel_2: cc.Label = null;

    @property(cc.SpriteFrame)
    private pokerSuitArray: Array<cc.SpriteFrame> = [];

    @property(cc.SpriteFrame)
    private faceCardArray: Array<cc.SpriteFrame> = [];

    @property(cc.Node)
    private touchMask: cc.Node = null;

    private pokerJsonInfo: PokerJsonInfo = null;

    cardId: number = 0;

    private _selected: boolean = false;

    get selectNode(): cc.Node { return this.node };

    get isSelected(): boolean { return this._selected; };

    init(cardId: number, isFront = true) {
        this.cardId = cardId;
        this.pokerJsonInfo = GameStaticDataManager.pokerJsonInfoMap[this.cardId];
        this._selected = false;
        this.initPokerBg(isFront);
        this.initFrontElement();
    }

    private initPokerBg(isFront: boolean) {
        if (isFront) {
            this.pokerFrontBgNode.active = true;
            this.pokerBackBgNode.active = false;
            return;
        }
        this.pokerFrontBgNode.active = false;
        this.pokerBackBgNode.active = true;
    }

    private initFrontElement() {
        this.pokerSuitSprite_1.node.active = true;
        this.pokerSuitSprite_2.node.active = true;
        this.pokerPointLabel_1.node.active = true;
        this.pokerPointLabel_2.node.active = true;
        this.faceCardSprite.node.active = true;
        if (this.pokerJsonInfo.pokerSuit == -1) {
            this.pokerSuitSprite_1.node.active = false;
            this.pokerSuitSprite_2.node.active = false;
        }
        if (this.pokerJsonInfo.pokerPoint as POKER_POINTS == POKER_POINTS.none) {
            this.pokerPointLabel_1.node.active = false;
            this.pokerPointLabel_2.node.active = false;
        }
        if (this.pokerJsonInfo.faceCard == POKER_SUITS.none) {
            this.faceCardSprite.node.active = false;
        }

        this.pokerSuitSprite_1.spriteFrame = this.pokerSuitArray[this.pokerJsonInfo.pokerSuit];
        this.pokerSuitSprite_2.spriteFrame = this.pokerSuitArray[this.pokerJsonInfo.pokerSuit];
        this.faceCardSprite.spriteFrame = this.faceCardArray[this.pokerJsonInfo.faceCard];
        this.pokerPointLabel_1.string = this.pokerPointLabel_2.string = `${this.pokerJsonInfo.pokerPoint}`;
        
        if (this.pokerJsonInfo.pokerSuit == POKER_SUITS.diamond || this.pokerJsonInfo.pokerSuit == POKER_SUITS.heart) {
            this.pokerPointLabel_1.node.color = this.pokerPointLabel_2.node.color = cc.color().fromHEX("#CF3333");
        } else {
            this.pokerPointLabel_1.node.color = this.pokerPointLabel_2.node.color = cc.color().fromHEX("#000000");
        }
    }

    clickSelected() {
        this._selected = !this._selected;
        if (this._selected) cc.tween(this.selectNode).to(0.12, { y: 26 }, { easing: "circOut" }).start();
        else cc.tween(this.selectNode).to(0.12, { y: 0 }, { easing: "circIn" }).start();
    }

    showTouchMask() { this.touchMask.active = true; };

    hideTouchMask() { this.touchMask.active = false; };

}
