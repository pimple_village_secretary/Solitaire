import Poker from "../modules/poker/Poker";
import LoadResInfo from "../util/LoadResInfo";
import UserVo from "./roledata/UserVo";
import PokerJsonInfo from "./static/PokerJsonInfo";

export default class GameStaticDataManager {

    static pokerJsonInfoMap: { [key: number]: PokerJsonInfo };

    static async init(loadResInfo: LoadResInfo) {
        this.pokerJsonInfoMap = await this.initSingle(PokerJsonInfo, "pokers", loadResInfo);
    }

    static initSingle(T, jsonFileName: string, loadResInfo: LoadResInfo) {
        loadResInfo.addNeedLoadCount(1);
        return new Promise<{ [key: number]: any }>((resolve, reject) => {
            const path = `jsondata/${jsonFileName}`;
            cc.resources.load(path, cc.JsonAsset, (error, assets: cc.JsonAsset) => {
                let result = {};
                let keyArray = new Array();
                assets = assets.json;
                for (const key in assets) {
                    keyArray.push(key);
                }
                const length = assets[keyArray[0]].length;
                for (let i = 0; i < length; i++) {
                    let object = new T();
                    keyArray.forEach(key => {
                        object[key] = assets[key][i];
                    });
                    result[object.id] = object;
                }
                console.log(`load resources ${jsonFileName} success.`);
                loadResInfo.addLoadedCount(1);
                resolve(result);
            });
        });
    }

    static getPokerJsonInfoMap(cards: Array<number>): Array<PokerJsonInfo> {
        let result: Array<PokerJsonInfo> = [];
        for (let card of cards) result.push(this.pokerJsonInfoMap[card]);
        return result;
    }
}