import NumericalValueUtil from "../../util/NumericalValueUtil";
import GameStaticDataManager from "../GameStaticDataManager";
import PokerJsonInfo from "../static/PokerJsonInfo";
import { FACE_CARDS } from "./FaceCardsType";
import { PLAY_CARDS_TYPE } from "./PlayCardsType";
import { POKER_POINTS } from "./PokerPointsType";
import RefereeVo from "./RefereeVo";
import UserVo from "./UserVo";

export default class FightingLandlordCardsVo extends RefereeVo{

    lastPlayedCardsRoleId: number | null = null;                            // 最近一次打出的牌组的人

    cardsPlayedByUsers: { [key: number]: Array<number> | null } = {};       // 记录用户打出的牌组

    callLandlordByUsers: { [key: number]: boolean | null } = {};            // 记录用户是否叫地主

    landlordCandidateQueue: Array<number> = [];                   // 判断到谁地主用户队列

    initJsonInfo(resp: any) {
        this.landlordCandidateQueue = resp.landlordCandidateQueue;
        this.lastPlayedCardsRoleId = resp.lastPlayedCardsRoleId;
        this.cardsPlayedByUsers = resp.cardsPlayedByUsers;
        this.callLandlordByUsers = resp.callLandlordByUsers;
        super.initJsonInfo(resp);
    }

    /**
     * 初始化记牌器
     */
    initCardsCounter(): Map<POKER_POINTS, number> {
        let cardsCounter: Map<POKER_POINTS, number> = new Map<POKER_POINTS, number>();
        const keys = Object.keys(GameStaticDataManager.pokerJsonInfoMap);
        for (let key in keys) {
            const jsonInfo = GameStaticDataManager.pokerJsonInfoMap[Number(key)];
            if (jsonInfo.pokerPoint == POKER_POINTS.none || 
                jsonInfo.pokerPoint == POKER_POINTS.point_a || 
                jsonInfo.pokerPoint == POKER_POINTS.point_2) 
                continue;
            cardsCounter.set(jsonInfo.pokerPoint as POKER_POINTS, 0);
        }
        cardsCounter.set(POKER_POINTS.point_a, 0);
        cardsCounter.set(2 as unknown as POKER_POINTS.point_2, 0);
        cardsCounter.set(POKER_POINTS.none, 0);
        return cardsCounter;
    }

    setCardsCounter(cardsCounter: Map<POKER_POINTS, number>, cards: Array<number>) {
        const jsonInfos = GameStaticDataManager.getPokerJsonInfoMap(cards);
        jsonInfos.forEach(element => {
            const value = cardsCounter.get(element.pokerPoint as POKER_POINTS);
            cardsCounter.set(element.pokerPoint as POKER_POINTS, value + 1);
        });
    }

    isComplyRules(roleId: number, cards: Array<number>): boolean {
        const curPlayType = this.determinePlayCardsType(cards);
        if (curPlayType == PLAY_CARDS_TYPE.none) return false;
        if (this.isMustPlayCards(roleId)) return true;
        const lastPlayedCards = this.cardsPlayedByUsers[this.lastPlayedCardsRoleId];
        return this.compareCards(cards, lastPlayedCards);
    };

    compareCards(cards1: Array<number>, cards2: Array<number>): boolean {
        const curPlayType = this.determinePlayCardsType(cards1);
        const lastPlayedType = this.determinePlayCardsType(cards2);
        if (curPlayType == PLAY_CARDS_TYPE.rocket) return true;
        if (lastPlayedType == PLAY_CARDS_TYPE.rocket) return false;
        if (curPlayType == PLAY_CARDS_TYPE.bomb) {
            if (lastPlayedType != PLAY_CARDS_TYPE.bomb) return true;
            if (lastPlayedType == PLAY_CARDS_TYPE.bomb) {
                if (this.comparePoint(cards1[0], cards2[0]) > 0) return true;
                return false;
            }
        }
        if (curPlayType != lastPlayedType) return false;
        if (cards2.length != cards1.length) return false;
        if (curPlayType == PLAY_CARDS_TYPE.single ||
            curPlayType == PLAY_CARDS_TYPE.pair ||
            curPlayType == PLAY_CARDS_TYPE.threeOfKind) {
            if (this.comparePoint(cards1[0], cards2[0]) > 0) return true;
            return false;
        }
        let repeatingNum = 0;
        if (curPlayType == PLAY_CARDS_TYPE.singleStraight) repeatingNum = 1;
        if (curPlayType == PLAY_CARDS_TYPE.doubleStraight) repeatingNum = 2;
        if (curPlayType == PLAY_CARDS_TYPE.threeWithSingle || 
            curPlayType == PLAY_CARDS_TYPE.threeWithPair ||
            curPlayType == PLAY_CARDS_TYPE.triStraight ||
            curPlayType == PLAY_CARDS_TYPE.planeWithWings) {
            repeatingNum = 3;
        }
        if (curPlayType == PLAY_CARDS_TYPE.fourWitTwo) repeatingNum = 4;
        const curMinCard = this.getMinCardsPointConsecutive(cards1, repeatingNum)!;
        const lastMinCard = this.getMinCardsPointConsecutive(cards2, repeatingNum)!;
        if (this.comparePoint(curMinCard, lastMinCard) > 0) return true;
        return false;
    }

    /**
     * 比较点数大小
     * @param cardId1 
     * @param cardId2 
     * @returns 
     */
    comparePoint(cardId1: number, cardId2: number): number {
        const jsonInfo1 = GameStaticDataManager.pokerJsonInfoMap[cardId1];
        const jsonInfo2 = GameStaticDataManager.pokerJsonInfoMap[cardId2];
        if (jsonInfo1.landlordPointWeights == jsonInfo2.landlordPointWeights) return 0;
        if (jsonInfo1.landlordPointWeights > jsonInfo2.landlordPointWeights) return 1;
        return -1;
    }

    /**
     * 确定牌组类型
     * @param cards 卡牌组
     * @returns 确定牌组类型
     */
    determinePlayCardsType(cards: Array<number>): PLAY_CARDS_TYPE {
        let cardsCounter = this.initCardsCounter();
        this.setCardsCounter(cardsCounter, cards);
        const notRepeatingCards = this.getPointOfNotRepeating(cards);
        if (cards.length == 0) return PLAY_CARDS_TYPE.none;
        if (cards.length == 1) return PLAY_CARDS_TYPE.single;
        if (cards.length == 2 && notRepeatingCards.length == 1) {
            const jsonInfo = GameStaticDataManager.pokerJsonInfoMap[cards[0]]; 
            if (jsonInfo.pokerPoint == POKER_POINTS.none) 
                return PLAY_CARDS_TYPE.rocket;
            return PLAY_CARDS_TYPE.pair;
        }
        if (cards.length == 3 && notRepeatingCards.length == 1) {
            return PLAY_CARDS_TYPE.threeOfKind;
        }
        if (cards.length == 4) {
            if (notRepeatingCards.length == 1) return PLAY_CARDS_TYPE.bomb;
            if (notRepeatingCards.length == 2) {
                if (cardsCounter.get(notRepeatingCards[0] as POKER_POINTS) == 3) return PLAY_CARDS_TYPE.threeWithSingle;
                if (cardsCounter.get(notRepeatingCards[1] as POKER_POINTS) == 3) return PLAY_CARDS_TYPE.threeWithSingle;
            }
        }
        if (cards.length == 5 && notRepeatingCards.length == 2) {
            if (cardsCounter.get(notRepeatingCards[0] as POKER_POINTS) == 3) return PLAY_CARDS_TYPE.threeWithPair;
            if (cardsCounter.get(notRepeatingCards[1] as POKER_POINTS) == 3) return PLAY_CARDS_TYPE.threeWithPair;
        }
        if (cards.length >= 5) {
            let consecutiveLength = this.getCardsPointConsecutiveLengthOfNum(cardsCounter, 1);
            if (consecutiveLength >= 5 && cards.length % consecutiveLength == 0) {
                if (cardsCounter[POKER_POINTS.point_2]) return PLAY_CARDS_TYPE.none;
                return PLAY_CARDS_TYPE.singleStraight;
            }
            consecutiveLength = this.getCardsPointConsecutiveLengthOfNum(cardsCounter, 2);
            if (consecutiveLength >= 3 && cards.length % consecutiveLength == 0) {
                if (cardsCounter[POKER_POINTS.point_2]) return PLAY_CARDS_TYPE.none;
                return PLAY_CARDS_TYPE.doubleStraight;
            }
            consecutiveLength = this.getCardsPointConsecutiveLengthOfNum(cardsCounter, 3);
            if (consecutiveLength >= 2) {
                if (cards.length % consecutiveLength == 0) {
                    if (cardsCounter[POKER_POINTS.point_2]) return PLAY_CARDS_TYPE.none;
                    return PLAY_CARDS_TYPE.triStraight;
                }
                // isSameQuantity 判断 牌组A带B这种规则时， 用来判断是否满足A的不一样的点数数量要等于B的不一样的点数数量
                const isSameQuantity = notRepeatingCards.length == (consecutiveLength * 2);
                // isNumberLessThanTwo 判断 牌组A带B这种规则时， 用来限制B的相同点数的数量等于1，或等于2
                const remainingNum = (cards.length - consecutiveLength * 3) / consecutiveLength;
                const isNumberLessThanTwo = remainingNum == 2 || remainingNum == 1;
                if (isSameQuantity && isNumberLessThanTwo) return PLAY_CARDS_TYPE.planeWithWings;
            }
            consecutiveLength = this.getCardsPointConsecutiveLengthOfNum(cardsCounter, 4);
            if (consecutiveLength >= 1) {
                const isSameQuantity = notRepeatingCards.length == (consecutiveLength * 2);
                const remainingNum = (cards.length - consecutiveLength * 3) / consecutiveLength;
                const isNumberLessThanTwo = remainingNum == 2 || remainingNum == 1;
                if (isSameQuantity && isNumberLessThanTwo) return PLAY_CARDS_TYPE.planeWithWings;
            }
        }
        return PLAY_CARDS_TYPE.none;
    }

    /**
     * 获取点数不重复的卡牌组
     * @param cards 卡牌组
     * @returns 点数不重复的卡牌组
     */
    getPointOfNotRepeating(cards: Array<number>): Array<string> {
        let result: Array<string> = []
        let tempBuffer: Set<string> = new Set();
        for (let card of cards) {
            const point = GameStaticDataManager.pokerJsonInfoMap[card].pokerPoint;
            if (tempBuffer.has(point)) continue;
            tempBuffer.add(point);
            result.push(point)
        }
        return result;
    }

    /**
     * 获取顺子的长度
     * @param cardsCounter 卡牌计数器
     * @param num 顺子中对应的每个点数应有num张
     * @returns 
     */
    getCardsPointConsecutiveLengthOfNum(cardsCounter: Map<string, number>, num: number): number {
        let length = 0;
        let isBegin = false;
        let isEnd = false;
        cardsCounter.forEach((value, key) => {
            if (value == num) isBegin = true;
            if (isBegin && value == 0) isEnd = true;
            if (isBegin && !isEnd) if (value == num) length += 1;
        });
        return length;
    }

    /**
     * 获取顺子的最小的点数
     * @param cards 卡牌组
     * @param num 顺子中对应的每个点数应有num张
     * @returns 
     */
    getMinCardsPointConsecutive(cards: Array<number>, num: number) {
        let minCard: number = null;
        let jsonInfos: Array<PokerJsonInfo>= [];
        cards.forEach(element => {
            const jsonInfo = GameStaticDataManager.pokerJsonInfoMap[element];
            jsonInfos.push(jsonInfo);
        });
        jsonInfos.sort((a, b) => { return a.landlordPointWeights - b.landlordPointWeights });
        let cardCounter = new Map<string, number>();
        for (const jsonInfo of jsonInfos) {
            let value = 0;
            if (cardCounter.has(jsonInfo.pokerPoint)) value = cardCounter.get(jsonInfo.pokerPoint);
            if (value + 1 >= num) {
                minCard = jsonInfo.id;
                break;
            };
            cardCounter.set(jsonInfo.pokerPoint, value + 1);
        }
        return minCard;
    }

    /**
     * 有多少用户没有出牌(当前一圈儿)
     */
    numberOfUsersNotPlayCards(): number {
        const keys = Object.keys(this.cardsPlayedByUsers);
        let result = 0;
        for (const key of keys) {
            const cards = this.cardsPlayedByUsers[Number(key)];
            if (cards == null || cards.length == 0) result += 1;
        }
        return result;
    }

    /**
     * 是否轮到自己必须出牌
     * @param roleId 用户id
     * @returns 
     */
    isMustPlayCards(roleId: number): boolean {
        const num = this.numberOfUsersNotPlayCards();
        const haveThreeUserNotPlayCards = (num == 3);
        const IsMePlayedLastCard = (this.turnId == this.lastPlayedCardsRoleId);
        const isMyTurn = (this.turnId == roleId);
        if (haveThreeUserNotPlayCards || (num == 2 && IsMePlayedLastCard && isMyTurn)) return true;
        return false;
    }


}