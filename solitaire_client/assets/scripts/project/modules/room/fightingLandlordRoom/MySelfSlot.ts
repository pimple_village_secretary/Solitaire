import FightingLandlordCardsVo from "../../../data/roledata/FightingLandlordCardsVo";
import { ROOM_STATUS } from "../../../data/roledata/RoomStatusType";
import RoomVo from "../../../data/roledata/RoomVo";
import UserVo from "../../../data/roledata/UserVo";
import NetManager from "../../../net/NetManager";
import SendData from "../../../net/SendData";
import Poker from "../../poker/Poker";
import Role from "./Role";
import SelectPoker from "./SelectPoker";

const {ccclass, property} = cc._decorator;

@ccclass
export default class MySelfSlot extends cc.Component {

    @property(Role)
    private role: Role = null;

    @property(SelectPoker)
    private pokers: SelectPoker = null;

    // @property(cc.Prefab)
    // private pokerPrefab: cc.Prefab = null;

    @property(cc.Node)
    private alarmClock: cc.Node = null;

    @property(cc.Label)
    private countdown: cc.Label = null;

    @property(cc.Node)
    private cancelBtn: cc.Node = null;

    @property(cc.Node)
    private noCallLandlordBtn: cc.Node = null;

    @property(cc.Node)
    private hintBtn: cc.Node = null;

    @property(cc.Node)
    private playBtn: cc.Node = null;

    @property(cc.Node)
    private callLandlordBtn: cc.Node = null;

    @property(cc.Node)
    private showCardsContent: cc.Node = null;

    @property(cc.Label)
    private userStatusLabel: cc.Label = null;

    @property(cc.Prefab)
    private pokerPrefab: cc.Prefab = null;

    private userVo: UserVo = null;
    
    private referee: FightingLandlordCardsVo = null;

    private roomStatus: ROOM_STATUS;

    private roomId: number;

    init(userVo: UserVo, referee: FightingLandlordCardsVo, roomVo: RoomVo): void{
        this.userVo = userVo;
        this.referee = referee;
        this.roomStatus = roomVo.status;
        this.roomId = roomVo.roomId;
        this.role.init(userVo, referee?.bookmakerUserId);
        if (this.roomStatus === ROOM_STATUS.waiting) return;
        this.refreshCards(referee.turnId);
        this.refreshBtn();
        this.refreshCountdown();
        this.refreshPokerStatus();
    }

    /**
     * 改变槽到等待状态的样式
     */
    changeSlotToWaitStatus(): void {
        this.pokers.node.active = false;
        this.alarmClock.active = false;
        this.showCardsContent.active = false;
        this.userStatusLabel.node.active = false;
    }

    refreshCards(turnId: number | null) {
        this.pokers.node.active = true;
        const cards = this.referee.findHandCards(this.userVo.roleId);
        this.pokers.init(cards);
        this.pokers.node.off("touchPokerEnd");
        this.pokers.node.on("touchPokerEnd", (cards: Array<number>) => {
            let isShow = this.referee.isComplyRules(this.userVo.roleId, cards);
            console.log(`temp: ${isShow}`);
            this.playBtn.getComponent(cc.Button).interactable = isShow;
        });
    }

    refreshBtn() {
        this.cancelBtn.active = false;
        this.noCallLandlordBtn.active = false;
        this.hintBtn.active = false;
        this.playBtn.active = false;
        this.callLandlordBtn.active = false;
        this.playBtn.getComponent(cc.Button).interactable = false;
        this.cancelBtn.getComponent(cc.Button).interactable = !this.referee.isMustPlayCards(this.userVo.roleId);
        if (this.referee.turnId != this.userVo.roleId) return;

        if (this.roomStatus === ROOM_STATUS.setBookmaker) {
            this.callLandlordBtn.active = true;
            this.noCallLandlordBtn.active = true;
        } else if (this.roomStatus === ROOM_STATUS.inGame) {
            this.hintBtn.active = true;
            this.playBtn.active = true;
            this.cancelBtn.active = true;
        }
    }

    refreshCountdown() {
        this.unschedule(this.showCountdown);
        this.alarmClock.active = false;
        if (this.referee.turnId != this.userVo.roleId) return;
        this.alarmClock.active = true;
        this.schedule(this.showCountdown, 1);
        this.showCountdown();
    }

    refreshPokerStatus() {
        this.userStatusLabel.node.active = false;
        this.showCardsContent.active = false;
        if (this.roomStatus == ROOM_STATUS.setBookmaker) {
            const isCallLandlord = this.referee.callLandlordByUsers[this.userVo.roleId];
            if (isCallLandlord == null || this.referee.turnId == this.userVo.roleId) return;
            this.userStatusLabel.node.active = true;
            if (isCallLandlord == true) this.userStatusLabel.string = "叫地主";
            else this.userStatusLabel.string = "不叫地主";
        }
        else if (this.roomStatus == ROOM_STATUS.inGame || this.roomStatus == ROOM_STATUS.GameOver) {
            this.userStatusLabel.string = "不要";
            const cards = this.referee.cardsPlayedByUsers[this.userVo.roleId];
            if (cards == null || this.referee.turnId == this.userVo.roleId) return;
            this.refreshPlayCards(cards);
            if (cards.length == 0) this.userStatusLabel.node.active = true;
            else this.showCardsContent.active = true;
        };
    }

    refreshPlayCards(cards: Array<number>): void {
        this.showCardsContent.removeAllChildren();
        cards.forEach(cardId => {
            let node: cc.Node = cc.instantiate(this.pokerPrefab);
            let poker = node.getComponent(Poker);
            poker.init(cardId);
            this.showCardsContent.addChild(node);
        });
    }

    private showCountdown() {
        const countdownTime = this.referee.remainingTime;
        if (countdownTime >= 0) {
            this.countdown.string = `${Math.round(countdownTime / 1000)}`;
            return;
        }
        this.unschedule(this.showCountdown);
    }

    async clickNoCallLandlordBtn() {
        const roomId = this.roomId;
        const isCall = false;
        const sendData = new SendData({ roomId, isCall } , "callLandlord");
        const resp = await NetManager.socket.sendDataSync(sendData);
        this.node.emit("refreshRoom", resp);
    }

    async clickCallLandlordBtn() {
        const roomId = this.roomId;
        const isCall = true;
        const sendData = new SendData({ roomId, isCall } , "callLandlord");
        const resp = await NetManager.socket.sendDataSync(sendData);
        this.node.emit("refreshRoom", resp);
    }

    async clickPlayBtn() {
        const roomId = this.roomId;
        const cards = this.pokers.currentlySelectedCard();
        const sendData = new SendData({ roomId, cards } , "playCards");
        const resp = await NetManager.socket.sendDataSync(sendData);
        this.node.emit("refreshRoom", resp);
    }

    async clickCancelBtn() {
        const roomId = this.roomId;
        const cards = [];
        const sendData = new SendData({ roomId, cards } , "playCards");
        const resp = await NetManager.socket.sendDataSync(sendData);
        this.node.emit("refreshRoom", resp);
    }
    
}
